#include "MemoryMaps.h"

namespace cmlabs {


///////////////////////////////////////////////////////////
//                     TypeMap                           //
///////////////////////////////////////////////////////////

// static 
bool MemoryMaps::CreateNewTypeLevel(uint16 id, const char* name, uint64 time) {
	TypeMapEntry* entry = GenericMemoryMap<TypeMapEntry, uint16, uint8>::CreateEntryAndLockMap(id, name, 0);
	if (time)
		entry->time = time;
	GenericMemoryMap<TypeMapEntry, uint16, uint8>::UnlockMap();
	return (entry != NULL);
}

// static
bool MemoryMaps::GetTypeLevelName(uint16 id, char* name, uint32 maxSize) {
	return GenericMemoryMap<TypeMapEntry, uint16, uint8>::GetEntryName(id, name, maxSize);
}

// static
bool MemoryMaps::GetTypeLevelID(const char* name, uint16 &id) {
	return GenericMemoryMap<TypeMapEntry, uint16, uint8>::GetEntryID(name, id);
}

// static
bool MemoryMaps::DeleteTypeLevel(const char* name) {
	uint16 id;
	if (!GenericMemoryMap<TypeMapEntry, uint16, uint8>::GetEntryID(name, id))
		return false;
	return GenericMemoryMap<TypeMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
bool MemoryMaps::DeleteTypeLevel(uint16 id) {
	return GenericMemoryMap<TypeMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
uint64 MemoryMaps::GetTypeLevelCreateTime(uint16 id) {
	return GenericMemoryMap<TypeMapEntry, uint16, uint8>::GetEntryCreateTime(id);
}



///////////////////////////////////////////////////////////
//                   ContextMap                          //
///////////////////////////////////////////////////////////

// static 
bool MemoryMaps::CreateNewContextLevel(uint16 id, const char* name, uint64 time) {
	ContextMapEntry* entry = GenericMemoryMap<ContextMapEntry, uint16, uint8>::CreateEntryAndLockMap(id, name, 0);
	if (time)
		entry->time = time;
	GenericMemoryMap<ContextMapEntry, uint16, uint8>::UnlockMap();
	return (entry != NULL);
}

// static
bool MemoryMaps::GetContextLevelName(uint16 id, char* name, uint32 maxSize) {
	return GenericMemoryMap<ContextMapEntry, uint16, uint8>::GetEntryName(id, name, maxSize);
}

// static
bool MemoryMaps::GetContextLevelID(const char* name, uint16 &id) {
	return GenericMemoryMap<ContextMapEntry, uint16, uint8>::GetEntryID(name, id);
}

// static
bool MemoryMaps::DeleteContextLevel(const char* name) {
	uint16 id;
	if (!GenericMemoryMap<ContextMapEntry, uint16, uint8>::GetEntryID(name, id))
		return false;
	return GenericMemoryMap<ContextMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
bool MemoryMaps::DeleteContextLevel(uint16 id) {
	return GenericMemoryMap<ContextMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
uint64 MemoryMaps::GetContextLevelCreateTime(uint16 id) {
	return GenericMemoryMap<ContextMapEntry, uint16, uint8>::GetEntryCreateTime(id);
}



///////////////////////////////////////////////////////////
//                     TopicMap                          //
///////////////////////////////////////////////////////////

// static 
bool MemoryMaps::CreateNewTopic(uint8 id, const char* name) {
	TopicMapEntry* entry = GenericMemoryMap<TopicMapEntry, uint32, uint8>::CreateEntryAndLockMap(id, name, 0);
	GenericMemoryMap<TopicMapEntry, uint32, uint8>::UnlockMap();
	return (entry != NULL);
}

// static
bool MemoryMaps::GetTopicName(uint32 id, char* name, uint32 maxSize) {
	return GenericMemoryMap<TopicMapEntry, uint32, uint8>::GetEntryName(id, name, maxSize);
}

// static
bool MemoryMaps::GetTopicID(const char* name, uint32 &id) {
	return GenericMemoryMap<TopicMapEntry, uint32, uint8>::GetEntryID(name, id);
}

// static
bool MemoryMaps::DeleteTopic(const char* name) {
	uint32 id;
	if (!GenericMemoryMap<TopicMapEntry, uint32, uint8>::GetEntryID(name, id))
		return false;
	return GenericMemoryMap<TopicMapEntry, uint32, uint8>::DeleteEntry(id);
}

// static
bool MemoryMaps::DeleteTopic(uint32 id) {
	return GenericMemoryMap<TopicMapEntry, uint32, uint8>::DeleteEntry(id);
}

// static
uint64 MemoryMaps::GetTopicCreateTime(uint32 id) {
	return GenericMemoryMap<TopicMapEntry, uint32, uint8>::GetEntryCreateTime(id);
}






///////////////////////////////////////////////////////////
//                     NodeMap                           //
///////////////////////////////////////////////////////////

//// static 
//bool MemoryMaps::CreateNewNode(uint8 id, const char* name, uint64 address) {
//	NodeMapEntry* entry = GenericMemoryMap<NodeMapEntry, uint16, uint64>::CreateEntryAndLockMap(id, name, address);
//	GenericMemoryMap<NodeMapEntry, uint32, uint64>::UnlockMap();
//	return (entry != NULL);
//}
//
//// static
//uint64 MemoryMaps::GetNodeAddress(uint16 id) {
//	uint64 address = 0;
//	if (!GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryKey(id, address))
//		return false;
//	return address;
//}
//
//// static
//bool MemoryMaps::GetNodeID(uint64 address, uint16 &id) {
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryID(address, id);
//}
//
//// static
//bool MemoryMaps::GetNodeName(uint16 id, char* name, uint32 maxSize) {
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryName(id, name, maxSize);
//}
//
//// static
//bool MemoryMaps::GetNodeID(const char* name, uint16 &id) {
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryID(name, id);
//}
//
//// static
//bool MemoryMaps::DeleteNode(const char* name) {
//	uint16 id;
//	if (!GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryID(name, id))
//		return false;
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::DeleteEntry(id);
//}
//
//// static
//bool MemoryMaps::DeleteNode(uint16 id) {
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::DeleteEntry(id);
//}
//
//// static
//uint64 MemoryMaps::GetNodeCreateTime(uint16 id) {
//	return GenericMemoryMap<NodeMapEntry, uint16, uint64>::GetEntryCreateTime(id);
//}




///////////////////////////////////////////////////////////
//                     CrankMap                          //
///////////////////////////////////////////////////////////

// static 
bool MemoryMaps::CreateNewCrank(uint16 id, const char* name, const char* function, const char* libraryFilename, uint64 time) {
	CrankMapEntry* entry = GenericMemoryMap<CrankMapEntry, uint16, uint8>::CreateEntryAndLockMap(id, name, 0);
	if (time)
		entry->time = time;
	utils::strcpyavail(entry->function, function, MAXKEYNAMELEN, false);
	utils::strcpyavail(entry->libraryFilename, libraryFilename, MAXKEYNAMELEN, false);
	GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
	return (entry != NULL);
}

// static
bool MemoryMaps::GetCrankName(uint16 id, char* name, uint32 maxSize) {
	return GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryName(id, name, maxSize);
}

// static
bool MemoryMaps::GetCrankFunction(uint16 id, char* function, uint32 maxSize) {
	CrankMapEntry* entry = GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryAndLockMap(id);
	if (entry) {
		utils::strcpyavail(function, entry->function, maxSize, false);
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return true;
	}
	else {
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return false;
	}
}

//static
bool MemoryMaps::GetCrankLibraryFilename(uint16 id, char* libraryFilename, uint32 maxSize) {
	CrankMapEntry* entry = GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryAndLockMap(id);
	if (entry) {
		utils::strcpyavail(libraryFilename, entry->libraryFilename, maxSize, false);
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return true;
	}
	else {
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return false;
	}
}

// static
bool MemoryMaps::GetCrankID(const char* name, uint16 &id) {
	return GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryID(name, id);
}

// static
bool MemoryMaps::GetCrankScript(uint16 id, char* script, uint32 maxSize) {
	CrankMapEntry* entry = GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryAndLockMap(id);
	if (entry) {
		utils::strcpyavail(script, entry->script, maxSize, false);
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return true;
	}
	else {
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return false;
	}
}

// static
bool MemoryMaps::SetCrankScript(uint16 id, const char* script) {
	CrankMapEntry* entry = GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryAndLockMap(id);
	if (entry) {
		utils::strcpyavail(entry->script, script, MAXSCRIPTLEN, false);
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return true;
	}
	else {
		GenericMemoryMap<CrankMapEntry, uint16, uint8>::UnlockMap();
		return false;
	}
}

// static
bool MemoryMaps::DeleteCrank(const char* name) {
	uint16 id;
	if (!GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryID(name, id))
		return false;
	return GenericMemoryMap<CrankMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
bool MemoryMaps::DeleteCrank(uint16 id) {
	return GenericMemoryMap<CrankMapEntry, uint16, uint8>::DeleteEntry(id);
}

// static
uint64 MemoryMaps::GetCrankCreateTime(uint16 id) {
	return GenericMemoryMap<CrankMapEntry, uint16, uint8>::GetEntryCreateTime(id);
}




///////////////////////////////////////////////////////////
//                     QueueMap                          //
///////////////////////////////////////////////////////////

//// static 
//bool MemoryMaps::CreateNewQueue(uint32 pageID, const char* name, uint32 &id) {
//	QueueMapEntry* entry = GenericMemoryMap<QueueMapEntry, uint32, uint32>::CreateEntryAndLockMap(name, pageID, id);
//	GenericMemoryMap<QueueMapEntry, uint32, uint8>::UnlockMap();
//	return (entry != NULL);
//}
//
//// static
//bool MemoryMaps::GetQueueName(uint32 id, char* name, uint32 maxSize) {
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryName(id, name, maxSize);
//}
//
//// static
//uint32 MemoryMaps::GetQueuePageID(uint32 id) {
//	uint32 pageID = 0;
//	if (!GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryKey(id, pageID))
//		return 0;
//	return pageID;
//}
//
//// static
//bool MemoryMaps::GetQueueID(const char* name, uint32 &id) {
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryID(name, id);
//}
//
//// static
//bool MemoryMaps::GetQueueID(uint32 pageID, uint32 &id) {
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryID(pageID, id);
//}
//
//// static
//bool MemoryMaps::DeleteQueue(const char* name) {
//	uint32 id;
//	if (!GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryID(name, id))
//		return false;
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::DeleteEntry(id);
//}
//
//// static
//bool MemoryMaps::DeleteQueue(uint32 id) {
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::DeleteEntry(id);
//}
//
//// static
//uint64 MemoryMaps::GetQueueCreateTime(uint32 id) {
//	return GenericMemoryMap<QueueMapEntry, uint32, uint32>::GetEntryCreateTime(id);
//}





///////////////////////////////////////////////////////////
//                     ProcessMap                        //
///////////////////////////////////////////////////////////

//// static 
//bool MemoryMaps::CreateNewProcess(const char* name, uint16 &id) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::CreateEntryAndLockMap(name, 0, id);
//	GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//	return (entry != NULL);
//}
//
//// static
//bool MemoryMaps::GetProcessName(uint16 id, char* name, uint32 maxSize) {
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryName(id, name, maxSize);
//}
//
//// static
//bool MemoryMaps::GetProcessID(const char* name, uint16 &id) {
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryID(name, id);
//}
//
//// static
//bool MemoryMaps::DeleteProcess(const char* name) {
//	uint16 id;
//	if (!GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryID(name, id))
//		return false;
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::DeleteEntry(id);
//}
//
//// static
//bool MemoryMaps::DeleteProcess(uint16 id) {
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::DeleteEntry(id);
//}
//
//// static
//uint64 MemoryMaps::GetProcessCreateTime(uint16 id) {
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryCreateTime(id);
//}
//
//// static
//bool MemoryMaps::GetProcessCommandLine(uint16 id, char* cmdline, uint32 maxSize) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0) && (strlen(entry->commandline) < maxSize-1)) {
//		strcpy(cmdline, entry->commandline);		
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return true;
//	}
//	else {
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return false;
//	}
//}
//
//// static
//uint8 MemoryMaps::GetProcessStatus(uint16 id, uint64& lastseen) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0)) {
//		uint8 status = entry->status;
//		lastseen = entry->lastseen;
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return status;
//	}
//	else {
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		lastseen = 0;
//		return 0;
//	}
//}
//
//// static
//uint16 MemoryMaps::GetProcessIDFromOSID(uint32 osid) {
//	uint16 id = 0;
//	if (GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryID(osid, id))
//		return id;
//	else
//		return 0;
//}
//
//// static
//uint32 MemoryMaps::GetProcessOSID(uint16 id) {
//	uint32 osid = 0;
//	if (GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryKey(id, osid))
//		return osid;
//	else
//		return 0;
//}
//
//// static
//bool MemoryMaps::SetProcessOSID(uint16 id, uint32 osid) {
//	return GenericMemoryMap<ProcessMapEntry, uint16, uint32>::SetEntryKey(id, osid);
//}
//
//// static
//bool MemoryMaps::GetProcessQueueID(uint16 id, uint8 queueType, uint32& qid) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0)) {
//		switch(queueType) {
//			case SIGNALQ_ID:
//				qid = entry->signalQID;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case MSGQ_ID:
//				qid = entry->messageQID;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case REQQ_ID:
//				qid = entry->requestQID;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case CMDQ_ID:
//				qid = entry->commandQID;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			default:
//				break;
//		}
//	}
//	GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//	return false;
//}
//
//// static
//bool MemoryMaps::SetProcessCommandLine(uint16 id, const char* cmdline) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0) && (strlen(cmdline) < MAXCOMMANDLINELEN)) {
//		strcpy(entry->commandline, cmdline);
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return true;
//	}
//	else {
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return false;
//	}
//}
//
//// static
//bool MemoryMaps::SetProcessStatus(uint16 id, uint8 status) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0)) {
//		entry->status = status;
//		entry->lastseen = GetTimeNow();
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return true;
//	}
//	else {
//		GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//		return 0;
//	}
//}
//
//// static
//bool MemoryMaps::SetProcessQueueID(uint16 id, uint8 queueType, uint32 qid) {
//	ProcessMapEntry* entry = GenericMemoryMap<ProcessMapEntry, uint16, uint32>::GetEntryAndLockMap(id);
//	if ((entry != NULL) && (entry->id == id) && (entry->time != 0)) {
//		switch(queueType) {
//			case SIGNALQ_ID:
//				entry->signalQID = qid;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case MSGQ_ID:
//				entry->messageQID = qid;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case REQQ_ID:
//				entry->requestQID = qid;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			case CMDQ_ID:
//				entry->commandQID = qid;
//				GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//				return true;
//			default:
//				break;
//		}
//	}
//	GenericMemoryMap<ProcessMapEntry, uint16, uint32>::UnlockMap();
//	return false;
//}












///////////////////////////////////////////////////////////
//                     RequestMap                        //
///////////////////////////////////////////////////////////

//// static 
//RequestMapEntry* MemoryMaps::CreateAndLockNewRequest(uint32 from, uint32 to, uint32 &id) {
//	uint32 dataSize;
//	char* data = MemoryManager::GetAndLockSystemBlock(ID_REQUESTMAPPAGE, dataSize);
//	if (data == NULL)
//		return NULL;
//	// Get next available ID from list
//	RequestMapHeader* header = (RequestMapHeader*)data;
//	uint32 loc;
//	if (!utils::GetFirstFreeBitLoc((uint32*)((char*)header + sizeof(RequestMapHeader)), header->bitFieldSize, loc)) {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return NULL;
//	}
//	// Reserve it
//	id = loc;
//	utils::SetBit(id, BITOCCUPIED, (uint32*)((char*)header + sizeof(RequestMapHeader)), header->bitFieldSize);
//
//	// Calc offset
//	uint32 offset = sizeof(RequestMapHeader) + header->bitFieldSize + id * sizeof(RequestMapEntry);
//	if (offset > header->size) {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return NULL;
//	}
//	RequestMapEntry* entry = (RequestMapEntry*) (data+offset);
//	if ((entry->id == id) || (entry->time == 0)) {
//		entry->id = id;
//		entry->time = GetTimeNow();
//		entry->from = from;
//		entry->to = to;
//		header->count++;
//		// Don't unlock the block
//		// MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return entry;
//	}
//	else {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return NULL;
//	}
//}
//
//// static
//RequestMapEntry* MemoryMaps::GetAndLockRequestEntry(uint32 id) {
//	uint32 dataSize;
//	char* data = MemoryManager::GetAndLockSystemBlock(ID_REQUESTMAPPAGE, dataSize);
//	if (data == NULL)
//		return NULL;
//	RequestMapHeader* header = (RequestMapHeader*)data;
//	RequestMapEntry* entry;
//
//	// Calc offset
//	uint32 offset = sizeof(RequestMapHeader) + header->bitFieldSize + id * sizeof(RequestMapEntry);
//	if (offset > header->size) {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return NULL;
//	}
//	entry = (RequestMapEntry*) (data+offset);
//	if ((entry->id == id) && (entry->time != 0)) {
//		return entry;
//	}
//	else {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return false;
//	}
//}
//
//// static
//bool MemoryMaps::UnlockRequestMap() {
//	MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//	return true;
//}
//
//// static
//bool MemoryMaps::DeleteRequest(uint32 id) {
//	uint32 dataSize;
//	char* data = MemoryManager::GetAndLockSystemBlock(ID_REQUESTMAPPAGE, dataSize);
//	if (data == NULL)
//		return false;
//	RequestMapHeader* header = (RequestMapHeader*)data;
//	RequestMapEntry* entry;
//
//	// Calc offset
//	uint32 offset = sizeof(RequestMapHeader) + header->bitFieldSize + id * sizeof(RequestMapEntry);
//	if (offset > header->size) {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return false;
//	}
//	entry = (RequestMapEntry*) (data+offset);
//	if ((entry->id != id) || (entry->time == 0)) {
//		MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//		return false;
//	}
//
//	// Free it
//	entry->time = 0;
//	entry->status = 0;
//	header->count--;
//	utils::SetBit(id, BITFREE, (uint32*)((char*)header + sizeof(RequestMapHeader)), header->bitFieldSize);
//
//	MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//	return true;
//}
//
//// static
//bool MemoryMaps::GetRequestCount(uint32& count) {
//	uint32 dataSize;
//	char* data = MemoryManager::GetAndLockSystemBlock(ID_REQUESTMAPPAGE, dataSize);
//	if (data == NULL)
//		return false;
//	RequestMapHeader* header = (RequestMapHeader*)data;
//	count = header->count;
//	MemoryManager::UnlockSystemBlock(ID_REQUESTMAPPAGE);
//	return true;
//}













} // namespace cmlabs
