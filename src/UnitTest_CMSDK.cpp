#include "UnitTest_CMSDK.h"
#include "PsyTime.h"

namespace cmlabs {

bool UnitTest_CMSDK() {

	//utils::UnitTest_Utils();
	//TemporalMemory::UnitTest();
	//return true;

	//struct timespec res;
	//clock_getres(CLOCK_MONOTONIC, &res);
	//printf("Time res: %u.%u\n", res.tv_sec, res.tv_nsec);

	//clock_gettime(CLOCK_MONOTONIC, &res);
	//printf("Time: %u.%u\n", res.tv_sec, res.tv_nsec);

	//clock_gettime(CLOCK_MONOTONIC, &res);
	//printf("Time: %u.%u\n", res.tv_sec, res.tv_nsec);

	//clock_gettime(CLOCK_MONOTONIC, &res);
	//printf("Time: %u.%u\n", res.tv_sec, res.tv_nsec);
	//return true;

	//uint32 loc = 0;
	//uint32 val = 0xFFFFFFFF;
	//if (!utils::GetFirstFreeBitLoc(&val, 4, loc)) {
	//	printf("Auch\n");
	//	return true;
	//}
	//printf("Loc: %u\n", loc);
	//return true;

	utils::UnitTest_Utils();
	PsyTime_UnitTest();
	MessageIndex::UnitTest();
	ComponentMemory::UnitTest();
	MemoryManager::ShmUnitTest();
	MemoryManager::UnitTest();
	ProcessMemory::UnitTest();
	return true;
}

bool PerfTest_CMSDK() {
	return ProcessMemory::PerfTest();
}

} // namespace cmlabs
