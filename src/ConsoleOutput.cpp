#include "ConsoleOutput.h"

namespace	cmlabs{

bool SetDebugLevel(unsigned char level) {
	memset(DebugLevel, level, CONSOLE_COUNT);
	return true;
}

bool SetDebugLevel(unsigned char subject, unsigned char level) {
	if (subject >= CONSOLE_COUNT)
		return false;
	DebugLevel[subject-1] = level;
	return true;
}

bool SetVerboseLevel(unsigned char level) {
	memset(VerboseLevel, level, CONSOLE_COUNT);
	return true;
}

bool SetVerboseLevel(unsigned char subject, unsigned char level) {
	if (subject >= CONSOLE_COUNT)
		return false;
	VerboseLevel[subject-1] = level;
	return true;
}

bool SetErrorLevel(unsigned char level) {
	ErrorLevel = level;
	return true;
}

bool PrintFile(FILE* stream, const char *formatstring, ... ) {

	va_list args;
    va_start(args, formatstring);
	char res = fprintf(stream, formatstring, args);
	va_end(args);
	return (res > 0);
}

bool PrintVerbose(unsigned char subject, unsigned char level, const char *formatstring, ... ) {

	bool res = true;
	if (VerboseLevel[subject] >= level) {
	    va_list args;
	    va_start(args, formatstring);
		res = PrintFile(stdout, formatstring, args);
		va_end(args);
	}
	return res;
}

bool PrintError(unsigned char level, const char *formatstring, ... ) {

	bool res = true;
	if (ErrorLevel >= level) {
	    va_list args;
	    va_start(args, formatstring);
		res = PrintFile(stderr, formatstring, args);
		va_end(args);
	}
	return res;
}

bool PrintDebug(unsigned char subject, unsigned char level, const char *formatstring, ... ) {

	bool res = true;
	if (DebugLevel[subject] >= level) {
	    va_list args;
	    va_start(args, formatstring);
		res = PrintFile(stdout, formatstring, args);
		va_end(args);
	}
	return res;
}


}
