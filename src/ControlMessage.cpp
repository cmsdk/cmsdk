#include "ControlMessage.h"

namespace cmlabs {

ControlMessage::ControlMessage(uint16 command, uint64 from, uint64 to, char* data, uint32 dataSize) {
	this->data = new char[sizeof(ControlMessageHeader) + dataSize];
	ControlMessageHeader* header = (ControlMessageHeader*) this->data;
	header->size = sizeof(ControlMessageHeader) + dataSize;
	header->cid = CONTROLMESSAGEID;
	header->command = command;
	header->time = GetTimeNow();
	header->from = from;
	header->to = to;
	if (dataSize > 0)
		memcpy(this->data+sizeof(ControlMessageHeader), data, dataSize);
}

ControlMessage::ControlMessage(char* data) {
	this->data = data;
}

ControlMessage::~ControlMessage() {
	data = NULL;
}

uint32 ControlMessage::getSize() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->size;
}

uint16 ControlMessage::getCommand() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->command;
}

uint64 ControlMessage::getTime() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->time;
}

uint64 ControlMessage::getFrom() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->from;
}

uint64 ControlMessage::getTo() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->to;
}

uint16 ControlMessage::getStatus() {
	if (!VerObj(data)) return 0;
	return ((struct ControlMessageHeader*)data)->status;
}

bool ControlMessage::setStatus(uint16 status) {
	if (!VerObj(data)) return false;
	((ControlMessageHeader*)data)->status = status;
	return true;
}


} // namespace cmlabs
