/*

	Implementation of POSIX directory browsing functions and types for Win32.

	Kevlin Henney (mailto:kevlin@acm.org), March 1997.

	Copyright Kevlin Henney, 1997. All rights reserved.

	Permission to use, copy, modify, and distribute this software and its
	documentation for any purpose is hereby granted without fee, provided
	that this copyright and permissions notice appear in all copies and
	derivatives, and that no charge may be made for the software and its
	documentation except to cover cost of distribution.
	
	This software is supplied "as is" without express or implied warranty.

	But that said, if there are any problems please get in touch.

*/

#include "direntwin.h"

namespace cmlabs {


#if defined(POCKETPC)
//	#define CONSTCHAR unsigned short
	#define CONSTCHAR WCHAR
#else
	#define CONSTCHAR char
#endif

#if defined(POCKETPC)
	int errno;
#endif

#ifdef CYGWIN
	int errno;
#endif

struct DIR
{
	HANDLE				handle; /* -1 for failed rewind */
	WIN32_FIND_DATA		info;
	struct dirent		result; /* d_name null iff first time */
	CONSTCHAR *name;
};

DIR *opendir(const char *name)
{
	DIR *dir = 0;

	if(name && name[0])
	{
		size_t base_length = strlen(name);
		const char *all = /* the root directory is a special case... */
			strchr("/\\", name[base_length - 1]) ? "*" : "/*";

		if((dir = (DIR *) malloc(sizeof *dir)) != 0 &&
		   (dir->name = (CONSTCHAR*) malloc(base_length + strlen(all) + 1)) != 0)
		{
			utils::strcpyavail((char*)dir->name, name, (uint32)base_length + (uint32)strlen(all) + 1, true);
			strcat_s((char*)dir->name, base_length + (uint32)strlen(all) + 1, all);

			if((dir->handle = FindFirstFile(dir->name, &dir->info)) != INVALID_HANDLE_VALUE)
			{
				dir->result.d_name = 0;
			}
			else /* rollback */
			{
				free((void*)dir->name);
				free(dir);
				dir = 0;
			}
		}
		else /* rollback */
		{
			free(dir);
			dir   = 0;
			errno = 12; // ENOMEM;
		}
	}
	else
	{
		errno = 22; // EINVAL;
	}

	return dir;
}

int closedir(DIR *dir)
{
	int result = -1;

	if(dir)
	{
		if(dir->handle != INVALID_HANDLE_VALUE)
		{
			result = FindClose(dir->handle);
		}

		free((void*)dir->name);
		free(dir);
	}

	if(result == -1) /* map all errors to EBADF */
	{
		errno = 9; // EBADF;
	}

	return result;
}

struct dirent *readdir(DIR *dir)
{
	struct dirent *result = 0;

	if(dir && dir->handle != INVALID_HANDLE_VALUE)
	{
		if((!dir->result.d_name) || (FindNextFile(dir->handle, &dir->info)))
		{
			result         = &dir->result;
			result->d_name = (char*) dir->info.cFileName;
		}
	}
	else
	{
		errno = 9; // EBADF;
	}

	return result;
}

void rewinddir(DIR *dir)
{
	if(dir && dir->handle != INVALID_HANDLE_VALUE)
	{
		FindClose(dir->handle);
		dir->handle = FindFirstFile(dir->name, &dir->info);
		dir->result.d_name = 0;
	}
	else
	{
		errno = 9; // EBADF;
	}
}


} // namespace cmlabs
