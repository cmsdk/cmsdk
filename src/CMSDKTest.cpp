// Replicoder.cpp : Defines the entry point for the console application.
//

#include "CMSDKTest.h"

using namespace cmlabs;

void printUsage(const char* progname) {
	printf("Usage: %s [] []\n\n", progname);
}

int main(int argc, char* argv[]) {

	UnitTest_CMSDK();

	return 0;
}

