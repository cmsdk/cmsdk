#include "MovingAverage.h"

namespace cmlabs {

Stats::Stats(uint32 maxCount) {
	this->maxCount = maxCount;
}

Stats::~Stats() {
	mutex.enter();
	entries.clear();
	mutex.leave();
}

bool Stats::clear() {
	mutex.enter();
	entries.clear();
	mutex.leave();
	return true;
}

bool Stats::add(double val) {
	mutex.enter();
	entries.push_back(val);
	while (entries.size() > maxCount)
		entries.pop_front();
	mutex.leave();
	return true;
}

double Stats::getSum() {
	double result = 0;
	mutex.enter();
	std::list<double>::iterator i, e;
	for (i=entries.begin(), e=entries.end(); i!=e; i++)
		result += *i;
	mutex.leave();
	return result;
}

double Stats::getAverage() {
	double result;
	mutex.enter();
	result = getSum()/entries.size();
	mutex.leave();
	return result;
}

double Stats::getVariance() {
	double variance, average, size;
	mutex.enter();
	if ( (size = (double)entries.size()) <= 1) {
		mutex.leave();
		return 0;
	}
	variance = 0;
	average = getSum()/size;
	std::list<double>::iterator i, e;
	for (i=entries.begin(), e=entries.end(); i!=e; i++)
		variance += (*i-average)*(*i-average);
	variance /= size;
	mutex.leave();
	return variance;
}

double Stats::getMedian() {
	double result;
	uint32 size, half;
	mutex.enter();
	size = (uint32)entries.size();
	if (!size) return 0;
	else if (size == 1) return entries.front();
	else if (size == 2) return (entries.front() + entries.back()) / 2.0;
	
	half = size / 2;
	entries.sort();
	std::list<double>::iterator i = entries.begin(), e = entries.end();
	for (uint32 n = 0; n <= half; n++)
		i++;
	if (size % 2)
		result = *i;
	else {
		result = *i;
		i++;
		result = (result + (*i)) / 2.0;
	}
	mutex.leave();
	return result;
}

double Stats::getStdDev() {
	return sqrt(getVariance());
}

uint32 Stats::getCount() {
	uint32 result;
	mutex.enter();
	result = (uint32)entries.size();
	mutex.leave();
	return result;
}


bool Stats::UnitTest() {
	Stats stats;
	stats.add(1);
	stats.add(3);
	stats.add(10);
	stats.add(-5);
	stats.add(8);
	stats.add(100);
	stats.add(10);
	stats.add(1);
	
	if (stats.getAverage() != 16)
		return false;
	if (stats.getMedian() != 5.5)
		return false;
	if (stats.getVariance() != 1031.5)
		return false;
	if (abs(stats.getStdDev() - 32.11697) > 0.0001)
		return false;
	
	return true;
}





} // namespace cmlabs
