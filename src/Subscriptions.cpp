#include "Subscriptions.h"

namespace cmlabs {


RetrieveSpec* TriggerSpec::getRetrieveSpec(const char* name) {
	if (!name || !strlen(name) || !retrieveCount)
		return NULL;

	RetrieveSpec* spec = (RetrieveSpec*) (((char*)this) +
		sizeof(TriggerSpec) + (filterCount*sizeof(FilterSpec)));

	uint32 n = 0;
	for (n = 0; n < retrieveCount; n++) {
		if (stricmp(name, spec->name) == 0)
			return spec;
		spec++;
	}
	return NULL;
}

QuerySpec* TriggerSpec::getQuerySpec(const char* name) {
	if (!name || !strlen(name) || !queryCount)
		return NULL;

	QuerySpec* spec = (QuerySpec*) (((char*)this) +
		sizeof(TriggerSpec) + (filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)));

	uint32 n = 0;
	for (n = 0; n < queryCount; n++) {
		if (stricmp(name, spec->name) == 0)
			return spec;
		spec++;
	}
	return NULL;
}

std::list<PostSpec*>* TriggerSpec::getPostSpecs(const char* name) {
	std::list<PostSpec*>* postSpecs = new std::list<PostSpec*>;
	uint32 n = 0;
	PostSpec* spec = (PostSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)));

	for (n = 0; n < postCount; n++) {
		if (!name || !strlen(name) || stricmp(name, spec->name) == 0)
			postSpecs->push_back(spec);
		spec++;
	}
	return postSpecs;
}

std::list<PostSpec*>* TriggerSpec::getPostSpecs(PsyType type) {
	std::list<PostSpec*>* postSpecs = new std::list<PostSpec*>;
	if (!type.isValid())
		return postSpecs;
	uint32 n = 0;
	PostSpec* spec = (PostSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)));

	for (n = 0; n < postCount; n++) {
		if (type.matches(spec->type))
			postSpecs->push_back(spec);
		spec++;
	}
	return postSpecs;
}
	
PostSpec* TriggerSpec::getPostSpec(const char* name) {
	uint32 n = 0;
	PostSpec* spec = (PostSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)));

	for (n = 0; n < postCount; n++) {
		if (stricmp(name, spec->name) == 0)
			return spec;
		spec++;
	}
	return NULL;
}


PostSpec* TriggerSpec::getPostSpec(PsyType type) {

	if (!type.isValid())
		return NULL;

	uint32 n = 0;
	PostSpec* spec = (PostSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)));

	for (n = 0; n < postCount; n++) {
		if (type.matches(spec->type))
			return spec;
		spec++;
	}
	return NULL;
}

SignalSpec* TriggerSpec::getSignalSpec(const char* name) {
	uint32 n = 0;
	SignalSpec* spec = (SignalSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)) +
		(postCount*sizeof(PostSpec)));

	for (n = 0; n < signalCount; n++) {
		if (stricmp(name, spec->name) == 0)
			return spec;
		spec++;
	}
	return NULL;
}


SignalSpec* TriggerSpec::getSignalSpec(PsyType type) {

	if (!type.isValid())
		return NULL;

	uint32 n = 0;
	SignalSpec* spec = (SignalSpec*) (((char*)this) +
		sizeof(TriggerSpec) +
		(filterCount*sizeof(FilterSpec)) +
		(retrieveCount*sizeof(RetrieveSpec)) +
		(queryCount*sizeof(QuerySpec)) +
		(postCount*sizeof(PostSpec)));

	for (n = 0; n < signalCount; n++) {
		if (type.matches(spec->type))
			return spec;
		spec++;
	}
	return NULL;
}



std::string TriggerSpec::toXML() {
	uint32 n;
	std::string str = utils::StringFormat("<trigger name=\"%s\" type=\"%s\" compid=\"%u\" interval=\"%u\" delay=\"%u\" from=\"%u\" to=\"%u\" tag=\"%u\" filtercount=\"%u\" signalcount=\"%u\" postcount=\"%u\" context=\"%s\" triggercontext=\"%s\"",
		name, type.toString().c_str(), componentID, interval, delay, from, to, tag, filterCount, signalCount, postCount, context.toString().c_str(), triggerContext.toString().c_str());

	if (!retrieveCount && !queryCount && !filterCount && !signalCount && !postCount) {
		str += " />\n";
		return str;
	}
	else
		str += ">\n";

	for (n=0; n<retrieveCount; n++) {
		RetrieveSpec* rSpec = getRetrieveSpec(n);
		if (rSpec)
			str += rSpec->toXML();
	}

	for (n=0; n<signalCount; n++) {
		SignalSpec* sSpec = getSignalSpec(n);
		if (sSpec)
			str += sSpec->toXML();
	}

	for (n=0; n<queryCount; n++) {
		QuerySpec* qSpec = getQuerySpec(n);
		if (qSpec)
			str += qSpec->toXML();
	}

	for (n=0; n<postCount; n++) {
		PostSpec* pSpec = getPostSpec(n);
		if (pSpec)
			str += pSpec->toXML();
	}

	str += "</trigger>\n";
	return str;

}

} // namespace cmlabs
