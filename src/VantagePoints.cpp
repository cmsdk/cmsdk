#include "VantagePoints.h"

namespace cmlabs {

VantagePoints::VantagePoints() {
	count = 0;
	inUseMap = NULL;
	state = IDLE;
	intrinsicTimescaleMS = 60000;
	timeoutMS = 60000;
	motionStartedTime = 0;
}

VantagePoints::~VantagePoints() {
}

bool VantagePoints::init(const char* xml) {
	if (!xml)
		return false;

	//<vantagepoints>
	//	<point id="1" label="A" data="2,2,45,6" overlap="0.1,0.3,0.5" />
	//	<point id="2" label="B" data="2,2,45,6" overlap="0.1,0.3,0.5" />
	//	<point id="3" label="C" data="2,2,45,6" overlap="0.1,0.3,0.5" />
	//</vantagepoints>

	XMLResults xmlResults;
	XMLNode node = XMLNode::parseString(xml, "vantagepoints", &xmlResults);

	// display error message (if any)
	if (xmlResults.error != eXMLErrorNone)
		return false;

	return init(node);
}
bool VantagePoints::init(XMLNode node) {
	XMLNode subNode;
	const char* str = node.getAttribute("timescale"); // in secs
	if (str)
		intrinsicTimescaleMS = utils::Ascii2Uint32(str) * 1000;

	str = node.getAttribute("timeout"); // in secs
	if (str)
		timeoutMS = utils::Ascii2Uint32(str) * 1000;

	count = (uint32)node.nChildNode("point");
	if (!count) {
		LogPrint(0, 0, 1, "No points found");
		return false;
	}

	inUseMap = new uint32[count+1]; // don't use index 0
	memset(inUseMap, 0, (count+1) * sizeof(uint32));

	std::vector<std::string> overlapsStr;
	std::vector<std::string>::iterator i, e;
	PerchPoint point;
	const char* id, *label, *data, *overlap;
	int n = 0;
	while (!(subNode = node.getChildNode("point", n++)).isEmpty()) {
		if ((id = subNode.getAttribute("id")) &&
			(label = subNode.getAttribute("label")) &&
			(data = subNode.getAttribute("data")) &&
			(overlap = subNode.getAttribute("overlap"))) {
			point.id = utils::Ascii2Uint32(id);
			if (point.id > count) {
				LogPrint(0, 0, 1, "Point %s ID is too high: %u > %u", id, point.id, count);
				return false;
			}
			if (points.find(point.id) != points.end())
				return false;
			point.entityID = 0;
			point.state = FREE;
			point.lastVisit = GetTimeNow();
			point.label = label;
			point.data = data;
			overlapsStr = utils::TextListSplit(overlap, ",");
			if (overlapsStr.size() == 0) {
				// put equal overlap values in
				for (uint32 n = 0; n <= count; n++) // index 1 based, so add and ignore index 0
					point.overlaps.push_back(1);
			}
			else {
				if (overlapsStr.size() != count) {
					LogPrint(0, 0, 1, "Not enough overlaps for point %s, need %u, only found %u", id, count, overlapsStr.size());
					return false;
				}
				point.overlaps.clear();
				point.overlaps.push_back(0); // index 1 based, so add and ignore index 0
				i = overlapsStr.begin();
				e = overlapsStr.end();
				while (i != e) {
					point.overlaps.push_back(utils::Ascii2Float64((*i).c_str()));
					i++;
				}
			}
			// ensure no benefit to move to itself :-)
			point.overlaps[point.id] = 1; // 100% overlap
			points[point.id] = point;
		}
		else {
			if (!id || !label)
				LogPrint(0, 0, 1, "Point id or name not specified");
			else if (!overlap)
				LogPrint(0, 0, 1, "Point %s ID has no overlap specified", point.id);
			else if (!data)
				LogPrint(0, 0, 1, "Point %s ID has no data specified", point.id);
			return false;
		}
	}
	return true;
}


const char* VantagePoints::getPointData(uint32 id) {
	if (!count || !id || (id > count))
		return NULL;
	PerchPoint* perch = &(points[id]);
	return perch->data.c_str();
}

const char* VantagePoints::getPointLabel(uint32 id) {
	if (!count || !id || (id > count))
		return NULL;
	PerchPoint* perch = &(points[id]);
	return perch->label.c_str();
}


uint32 VantagePoints::reserveNextPoint(uint32 entityID) {
	if (!count || (motionStartedTime && (GetTimeAgeMS(motionStartedTime) < timeoutMS)))
		return 0;

	uint32 n;
	double bestVal, val;
	PerchPoint* perch = NULL;

	uint32 ownPerch = 0, bestPerch = 0, occPerch = 0, eID;
	if (state == IDLE)
		bestPerch = 1;
	else {
		// find own current perch and any occupied perch
		for (n = 1; n <= count; n++) {
			if (!(eID = inUseMap[n]))
				continue;
			perch = &(points[n]);
			if ((perch->state == RESERVED) && (GetTimeAgeMS(perch->lastVisit) > timeoutMS)) {
				perch->state = FREE;
				perch->entityID = 0;
				inUseMap[perch->id] = 0;
				continue;
			}
			occPerch = n;
			if (!ownPerch && (eID == entityID)) {
				ownPerch = n;
				continue;
			}
		}
		if (!occPerch)
			bestPerch = 1;
		else {
			// go find best unoccupied perch from there
			if (ownPerch)
				perch = &(points[ownPerch]);
			else
				perch = &(points[occPerch]);
			bestVal = 0;
			for (n = 1; n <= count; n++) {
				if (!inUseMap[n]) {
					val = 1 - perch->overlaps[n];
					if (intrinsicTimescaleMS)
						val *= ((double)GetTimeAgeMS((&(points[n]))->lastVisit) / intrinsicTimescaleMS);
					if (val > bestVal) {
						bestVal = val;
						bestPerch = n;
					}
				}
			}
		}
	}
	
	if (!bestPerch)
		return 0;

	uint64 now = GetTimeNow();

	if (ownPerch) {
		perch = &(points[ownPerch]);
		perch->state = FREE;
		perch->entityID = 0;
		perch->lastVisit = now;
		inUseMap[perch->id] = 0;
	}

	perch = &(points[bestPerch]);
	perch->state = RESERVED;
	perch->entityID = entityID;
	perch->lastVisit = now;
	inUseMap[perch->id] = entityID;
	state = INMOTION;
	motionStartedTime = now;
	return perch->id;
}

bool VantagePoints::occupyPoint(uint32 id, uint32 entityID) {
	if (!count || !id || (id > count) || !entityID)
		return false;

	PerchPoint* perch = &(points[id]);
	if (perch->state != RESERVED)
		return false;
	if (perch->entityID != entityID)
		return false;

	perch->state = OCCUPIED;
	state = SETTLED;
	motionStartedTime = 0;
	return true;
}

bool VantagePoints::freePoint(uint32 id, uint32 entityID) {
	if (!count || !id || (id > count) || !entityID)
		return false;

	PerchPoint* perch = &(points[id]);
	if (perch->state != RESERVED)
		return false;
	if (perch->entityID != entityID)
		return false;

	inUseMap[id] = 0;
	perch->state = FREE;
	state = SETTLED;
	motionStartedTime = 0;
	return true;
}



} // namespace cmlabs
