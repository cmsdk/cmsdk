#include "MemoryCommander.h"

namespace cmlabs {

MemoryCommander::MemoryCommander() {
	data = NULL;
}

MemoryCommander::~MemoryCommander() {
	delete(data);
}

} // namespace cmlabs
