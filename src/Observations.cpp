#include "Observations.h"

namespace cmlabs {


bool ObservationTest() {
	GenericObservation<ObsLocation> loc;
	GenericObservation<ObsDimension> dim;
	return true;
}


uint32 GetObsTypeFromRawData(const char* data, uint32 size) {
	if ((size <= sizeof(ObsHeader)) || (((ObsHeader*)data)->cid != OBSERVATIONID))
		return 0;
	return ((ObsHeader*)data)->type;
}


int64 ExtractIntFromMessage(const char* name, DataMessage* msg, std::map<std::string, std::string>& mappings, bool &ok) {
	uint32 contentType;
	ok = true;
	std::map<std::string, std::string>::iterator i = mappings.find(name);
	if (i != mappings.end()) {
		if ((contentType = msg->getContentType(i->second.c_str())) == INTID)
			return msg->getInt(i->second.c_str(), ok);
		else if (contentType)
			return msg->getAsInt(i->second.c_str());
	}
	else {
		if ((contentType = msg->getContentType(name)) == INTID)
			return msg->getInt(name);
		else if (contentType)
			return msg->getAsInt(name);
	}
	ok = false;
	return 0;
}

float64 ExtractFloatFromMessage(const char* name, DataMessage* msg, std::map<std::string, std::string>& mappings, bool &ok) {
	uint32 contentType;
	ok = true;
	std::map<std::string, std::string>::iterator i = mappings.find(name);
	if (i != mappings.end()) {
		if ((contentType = msg->getContentType(i->second.c_str())) == DOUBLEID)
			return msg->getFloat(i->second.c_str(), ok);
		else if (contentType)
			return msg->getAsFloat(i->second.c_str());
	}
	else {
		if ((contentType = msg->getContentType(name)) == DOUBLEID)
			return msg->getFloat(name);
		else if (contentType)
			return msg->getAsFloat(name);
	}
	ok = false;
	return 0;
}

uint64 ExtractTimeFromMessage(const char* name, DataMessage* msg, std::map<std::string, std::string>& mappings, bool &ok) {
	uint32 contentType;
	ok = true;
	std::map<std::string, std::string>::iterator i = mappings.find(name);
	if (i != mappings.end()) {
		if ((contentType = msg->getContentType(i->second.c_str())) == TIMEID)
			return msg->getTime(i->second.c_str(), ok);
		//else if (contentType)
		//	return msg->getAsTime(i->second.c_str());
	}
	else {
		if ((contentType = msg->getContentType(name)) == TIMEID)
			return msg->getInt(name);
		//else if (contentType)
		//	return msg->getAsTime(name);
	}
	ok = false;
	return 0;
}

std::string ExtractStringFromMessage(const char* name, DataMessage* msg, std::map<std::string, std::string>& mappings, bool &ok) {
	uint32 contentType;
	ok = true;
	std::map<std::string, std::string>::iterator i = mappings.find(name);
	if (i != mappings.end()) {
		if ((contentType = msg->getContentType(i->second.c_str())) == CONSTCHARID)
			return msg->getString(i->second.c_str(), ok);
		else if (contentType)
			return msg->getAsString(i->second.c_str());
	}
	else {
		if ((contentType = msg->getContentType(name)) == CONSTCHARID)
			return msg->getString(name);
		else if (contentType)
			return msg->getAsString(name);
	}
	ok = false;
	return "";
}


} // namespace cmlabs
