# CMSDK summary {#summary}

The CMSDK core library is a C++ base library which contains much of the low-level functionality required by Psyclone. It is released under a BSD open source licence and is free for anyone to use for any purpose, except for those limited by the CADIA clause. For more information about licensing and the CADIA Clause please visit: https://cmlabs.com/licensing.

The CMSDK library abstracts the platform and operating system and provides an identical set of objects and utility functions for a very large number of functional areas such as threading and synchronisation, process handling, networking, shared memory handling, working with DLLs, timers, wait queues, binary message containers, HTML request handling incl. a fully functional HTTP server, maths and stats classes, 64-bit integer time functions with microsecond resolution, a request client/server/gateway system, a system-wide verbose and logging system and a very large library of utility functions for working with text and files.

Specifically, it provides the fundamental objects required for working with Psyclone and creating components to be loaded into Psyclone via DLLs or shared objects. The main two are PsyAPI and PsySpace, however, these underneath provide the foundation for the whole Psyclone system and multi-process/multi-computer/multi-platform infrastructure. for more information about the Psyclone platform please visit: https://cmlabs.com/psyclone

To learn more about how to compile and link with the CMSDK library please [go to the building page](@ref building_cmsdk).

To see how to use CMSDK in your project [go to the using page](@ref using_cmsdk)

The CMSDK library can be used as an SDK for the Psyclone platform. The main objects to learn about are the PsyAPI and PsySpace objects, but the DataMessage object is important to understand too.

In addition, the CMSDK contains the following objects and areas of function of interest:

- The [Utils library](@ref utils)
- The built-in [HTTP Server](@ref cmlabs::NetworkManager)
- The Request [client](@ref cmlabs::RequestClient)/[server](@ref cmlabs::RequestExecutor)/[gateway](@ref cmlabs::RequestGateway)
- [Threading](@ref cmlabs::ThreadManager)
- Microsecond [Time](@ref PsyTime) handling


For an updated version of this documentation and more information please visit: https://cmsdk.cmlabs.com/

To download the latest version of the CMSDK library please visit: https://cmlabs.com/download
