# Compiling and linking with the CMSDK library {#building_cmsdk}

CMSDK is the core library providing OS dependent functionality and lots and lots of the base code.

The Python2 and Python3 projects are used to port important parts of CMSDK to Python 2.x and 3.x using SWIG. These libraries can then be used for accessing CMSDK functionality from within a Python program

You need to have Python 2.7 and/or 3.5 installed for either or both 32-bit and 64-bit versions to compile these.

## Using Visual Studio 2015 (Windows)

In the Open Source distribution of CMSDK find the Visual Studio solutions file in the top directory called CMSDK.vs2015.sln. Open this in Visual Studio 2015 and compile the whole project either in Debug or Release mode. To build the SSL version of CMSDK select either Release SSL or Debug SSL. For this to work the following path must exist and contain the OpenSSL include files:
   ..\..\_libs\OpenSSL\openssl-1.0.2g-vs2015\include		for 32 bit
   ..\..\_libs\OpenSSL\openssl-1.0.2g-vs2015\include64		for 64 bit
and the library files
   ..\..\_libs\OpenSSL\openssl-1.0.2g-vs2015\lib			for 32 bit
   ..\..\_libs\OpenSSL\openssl-1.0.2g-vs2015\lib64		for 64 bit

To use the CMSDK library from a Python program you will need to build either the CMSDKPY2 and/or the CMSDKPY3 DLLs. You do this by selecting them in the Solution Explorer, right click and select Build or Rebuild. For this to work you will need Python 2 and/or 3 installed in either 32 and/or 64-bit versions in the following directories:
   C:\Python27		64-bit version of Python 2.7
   C:\Python27-32	32-bit version of Python 2.7
   C:\Python35		64-bit version of Python 3.5
   C:\Python35-32	31-bit version of Python 3.5

Other versions and locations can be used by editing the Visual Studio CMSDKPY2 and CMSDKPY3 project files.

## Using Make (Linux)

In the Open Source distribution of CMSDK find the main Makefile in the top directory called Makefile. On the command from this directory line issue the command
   make		to build everything for release
   make debug	to build everything for debug

To build the SSL version of Psyclone run either
   make ssl		to build everything for release including SSL support
   make ssldebug	to build everything for debug including SSL support

For this to work the SSL development libraries must be installed on the computer (used via lssl and lcrypto).

To use modules written in the Python language you will need to build either the Python2Link and/or the Python3Link DLLs. You do this by running
   make python2		to build all files needed for Python 2 integration in release
   make python2 debug	to build all files needed for Python 2 integration in debug
   make python3		to build all files needed for Python 3 integration in release
   make python3 debug	to build all files needed for Python 3 integration in debug

For this to work you will need Python 2 and/or 3 packages installed in the following directories:
   PYTHON2INCLUDE=-I/usr/include/python2.7
   PYTHON3INCLUDE=-I/usr/include/python3.4m
   PYTHON2LIB=-lpython2.7
   PYTHON3LIB=-lpython3.4m

Other versions and locations can be used by editing the Makefile.sys file entries for these.

# Linking with the CMSDK library

To link a third-party source project with the CMSDK library one needs to add the CMSDK binary library files to your project and include the appropriate header files needed. Depending on which additional libraries you may need (such as SSL) you may need to link to these as well.

## Using Visual Studio 2015 (Windows)

The easiest way to set up a project to use CMSDK is to look at the Psyclone project as it does this already.

The project needs to link with the appropriate binary CMSDK library file. They are called CMSDK(Debug)<vsversion>.lib, such as CMSDKDebug2015.lib and are located in CMSDK/lib/<platform> such as CMSDK/lib/Win32. The SSL builds files have SSL in their names such as CMSDKSSLDebug2015.lib.

The include header files are located in CMSDK/include.

Lastly, the project needs to be compiled with the code generation flag / runtime library Multi-threaded DLL.

## Using Make (Linux)

The easiest way to set up a project to use CMSDK is to look at the Psyclone project as it does this already.

The project needs to link with the appropriate binary CMSDK library file. They are called CMSDK(Debug).a, such as CMSDKDebug.a and are located in CMSDK/lib/<platform> such as CMSDK/lib/32. The SSL builds files have SSL in their names such as CMSDKSSLDebug.a.

The include header files are located in CMSDK/include.

