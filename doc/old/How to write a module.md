How to write a module {#moduletutorial}
===========================================
## note this is just copied from the old manual and has to be updated and summarized
text taken from <a href="psycloneManual2.html">here</a> There is possibly more useful information in there
There is a section in functions called Creating modules that use streaming media that could be integrated here

To write an external module in C++, you will need the CppAIRPlug. (If CppAIRPlug was not included with this distribution you can download the plug by going to http://www.cmlabs.com/psyclone/download/, and selecting the platform that you are developing your external module on).

External modules are designed to run outside Psyclone in separate executables. They are very similar to internal modules and they can be written in other languages, such as Java.

Any module has two distinct functionalities, namely message handling and message processing. Like internal modules, external modules only have to worry about the message processing (or crank, as we call it), because all the message handling (receiving, organizing, posting, etc.) is done automatically.

To create an external module in C++ one normally would use an AIRPlug. This is a C++ object which can be instantiated inside any C++ program and the plug is used for all communication with Psyclone.

This is an example of a simple C++ program and how to use the plug:

    int main(int argc, char* argv[]) {
       CppAIRPlug plug = new CppAIRPlug("MyFirstModule", "localhost", 10000);
       if (!plug->init()) {
          printf("Could not connect to Psyclone\n\n");
          return -1;
          }
       Message* triggerMessage;
       Message* msg;
       while (true) {
          if ((triggerMessage = plug->waitForNewMessage(3000)) == NULL) {
          // ... processing ... //
          msg = new Message("", "", "");
          plug->addOutputMessage(msg);
          plug->sendOutputMessages();
          }
       else {
          // ... do something else for a while ... //
          }
       }
    return 0;
    }

This program assumes that Psyclone is running on the local computer on the standard Psyclone port 10000.

For Psyclone to use the module, you will need to tell Psyclone about it. You do this via an XML definition like this:

    <module name="MyFirstModule" type="external" >
        <executable/>
        <description></description>
        <context name="Psyclone.System.Ready">
            <phase name="Default">
                <triggers>
                    <trigger from="AIRCentral" type="My.First.Type" />
                </triggers>
                <posts>
                    <post to="AIRCentral" type="My.First.Output" />
                </posts>
            </phase>
        </context>
    </module>

This specification can be put into the central psySpec configuration file and read in by Psyclone at startup. Alternatively it can be sent to Psyclone at runtime, in the conten of a message; the message type must be Psyclone.System.Create.Module. (Destroy modules by sending a message type Psyclone.System.Destroy.Module, with the name of the module as content, encapsulated in the <module> tag but without further content.)

Unless you specify a command that will automatically start up your module (using the <executable> tag) you will need to start up your external module manually after you start up Psyclone.

Note that you need to use the <executable /> tag to tell Psyclone that this is an external module. Otherwise, Psyclone will create it as an internal module. In a later tutorial you can see how to fill in the <executable> tag to automatically start up your module.

Now Psyclone knows that when any message with type My.First.Type is posted to the Whiteboard AIRCentral, your module with the name MyFirstModule should be woken up with this message.

In most cases you probably want to do a bit more in your module. The Plug object is a communication device, which you can use to get the triggering message and any other messages which may have been delivered if you asked for it in the psySpec using the <retrieve> tag (see below). If you post messages using the Plug the messages are automatically sent to the correct destinations.

When this empty message is posted, the :from and the :to fields are automatically filled in and a copy is sent to every destination Whiteboard defined in the psySpec, each with the correct type filled in. For example, if the psySpec had defined two post destinations

    <posts>
        <post to="AIRCentral" type="My.Second.Type" />
        <post to="AIRCentral" type="My.Third.Type" />
    </posts>

the AirCentral whiteboard would receive two messages, one with type My.Second.Type and one with My.Third.Type.

If, however, the function had created a message like this

    msg = new Message("", "", "My.Second.Type");

only one message would be sent out, as only the first post matches the type. Likewise, had the message been created with

    msg = new Message("", "", "My.Other.Type");

no message would be sent out.

If you wish to bypass the post section manually, you can always do

    msg = new Message("", "MyOtherWB", "My.Other.Type");

in which case only one message would be sent to the MyOtherWB Whiteboard.

When you specify in the psySpec which message types will trigger your module you may wish to retrieve additional messages from the same or other Whiteboards to be delivered along side the triggering message:

    ...
    <triggers>
        <trigger from="AIRCentral" type="My.First.Type" />
    </triggers>
    <retrieves>
        <retrieve from="BBX" type="Input.Sens.UniM.Hear"/>
        <retrieve from="BBX" type="Input.Person.Found.True">
           <latest>10</latest>
           <lastmsec>200</lastmsec>
        </retrieve>
    </retrieves>
    ...

In your code, you can read these additional messages, if any, by asking the Messenger object:

    // Get the current number of retrieved messages
    int getRetrievedMessageCount();
    // Get a specific retrieved message
    Message* getRetrievedMessage(int pos);
    // Get the full collection of the retrieved messages
    ObjectCollection* getAllRetrievedMessages();

Should you want to manually retrieve messages from the crank without specifying this in the psySpec, you can ask the Messenger object:

    // Retrieve additional messages from a source
    ObjectCollection* retrieveMessages(const JString& retrieveFrom, const JString& retrieveSpecXML);

where retrieveSpecXML uses the same format as you would have used in the psySpec including the surrounding <retrieves>… </retrieves>.

    You do not have to register cranks for external modules, but if you do it is a good way of telling your module what to do with a message. If we add a crank in the psySpec like this:

    <module name="MyFirstModule" type="external">
        <description></description>
        <context name="Psyclone.System.Ready">
            <phase name="Default">
                <triggers>
                    <trigger from="AIRCentral" type="My.First.Type" />
                </triggers>
                <crank name="MyFirstModuleCrank" />
                <posts>
                    <post to="WB1" type="My.First.Output" />
                    <post to="WB2" type="My.Second.Output" />
                </posts>
            </phase>
        </context>
    </module>

we can now slightly extend our code to ask for the name of the crank after being woken up by the trigger message:

    while (true) {
        if ((triggerMessage = plug->waitForNewMessage(3000)) == NULL) {
            // ... processing ... //
            if (plug->getCrankName().equals("MyFirstModuleCrank")) {
                  // ... processing ... //
                  msg = new Message("", "", "My.First.Output");
                  plug->addOutputMessage(msg);
                }
            else if (plug->getCrankName().equals("MySecondModuleCrank")) {
                  // ... processing ... //
                  msg = new Message("", "", "My.Second.Output");
                  plug->addOutputMessage(msg);
                 }
            plug->sendOutputMessages();
            }
        else {
            // ... do something else for a while ... //
        }
    }

Note that the module will now post its output to Whiteboard WB1 if the crank is MyFirstModuleCrank, and to whiteboard WB2 if the crank is equal to MySecondModuleCrank. 

Internal modules are designed to run inside Psyclone for maximum performance and efficiency. They are similar to external modules, but simpler to design, implement and maintain. (However, they do have limitations related to dynamically loaded libraries.)

Internal C++ modules are developed using the Psyclone SDK, which is provided with PsyclonePro distributions.

Any module has two distinct functionalities, namely message reception/handling and subsequent processing — also called a crank. Internal modules only have to worry about the crank, because all the message handling (receiving, organizing, posting, etc.) is done automatically.  

To create an internal module one has to define a crank which will process incoming messages and generate output messages.  

This is an example of the crank function for an internal module, which just prints Hello World to the screen:

    int MyFirstModuleCrank(Messenger* messenger) {
       printf("Hello World");
       return 0;
    }

All you need to do is put this into the cranks file in the Psyclone SDK (cranks.cpp and cranks.h in the default library called cm), recompile and now you have an internal module.

For Psyclone to use the module, you will need to tell Psyclone about it. You do this via an XML definition like this:

   <module name="MyFirstModule" type="internal" >
      <description></description>
     <context name="Psyclone.System.Ready">
            <phase name="Default">
               <triggers>
                  <trigger from="WB1" type="My.First.Type" />
               </triggers>
               <crank name="cm::MyFirstModuleCrank" />
               <posts>
                  <post to="WB1" type="My.Second.Type" />
               </posts>
            </phase>
     </context>
   </module>

This specification can be put into the central psySpec configuration file and read in by Psyclone at startup. Alternatively it can be sent to Psyclone at runtime, in the conten of a message; the message type must be Psyclone.System.Create.Module. (Destroy modules by sending a message type Psyclone.System.Destroy.Module, with the name of the module as content, encapsulated in the <module> tag but without further content.)

Now Psyclone knows that when any message with type My.First.Type is posted to the Whiteboard WB1, your module with the name MyFirstModule should be woken up with this message. This will in turn automatically call the MyFirstModuleCrank function in the library cm you created above and hence print "Hello World" to the console.

In most cases you probably want to do a bit more in your module. The Messenger object which is passed to the function is a communication device, which you can use to get the triggering message and any other messages which may have been delivered if you asked for it in the psySpec using the <retrieve> tag (see below). If you post messages using the Messenger the messages are automatically sent to the correct destinations.

Here is an example of a slightly more advanced version of the crank function above:

   int MyFirstModuleCrank (Messenger* messenger) {
 
   if (messenger == NULL)
      return -1;
 
   Message* triggerMessage;
   Message* msg;
   while (messenger->shouldContinueRunning()) {
      if (messenger->waitForNewMessage(50)) {
         if ( (triggerMessage = messenger->getTriggerMessage()) != NULL) {
         //   Do something with the trigger message...
            msg = new Message("", "", "");
            messenger->addOutputMessage(msg);
            messenger->sendOutputMessages();
            }
         }
      }
   return 0;
   }

Now the module sends out one message each time it is woken up. You may notice that instead of returning, the module chooses to stay inside the function until the Messenger says that it should not continue running any longer.

When this empty message is posted, the :from and the :to fields are automatically filled in and a copy is sent to every destination Whiteboard defined in the psySpec, each with the correct type filled in. For example, if the psySpec had defined two post destinations

               <posts>
                  <post to="WB1" type="My.Second.Type" />
                  <post to="WB1" type="My.Third.Type" />
               </posts>

the WB1 Whiteboard would receive two messages, one with type My.Second.Type and one with My.Third.Type.

If, however, the function had created a message like this

       msg = new Message("", "", "My.Second.Type");

only one message would be sent out, as only the first post matches the type. Likewise, had the message been created with

       msg = new Message("", "", "My.Other.Type");

no message would be sent out.

If you wish to bypass the post section manually, you can always do

       msg = new Message("", "MyOtherWB", "My.Other.Type");

in which case only one message would be sent to the MyOtherWB Whiteboard. 

When you in the psySpec specify which message types will trigger your module you may wish to retrieve additional messages from the same or other Whiteboards to be delivered along side the triggering message:

       ...
       <triggers>
          <trigger from="WB1" type="My.First.Type" />
       </triggers>
       <retrieves>
          <retrieve from="BBX" type="Input.Sens.UniM.Hear"/>
          <retrieve from="BBX" type="Input.Person.Found.True">
             <latest>10</latest>
             <lastmsec>200</lastmsec>
          </retrieve>
       </retrieves>
       <crank name="cm::MyFirstModuleCrank" />
       ...

In your crank code, you can read these additional messages, if any, by asking the Messenger object:

       // Get the current number of retrieved messages
       int getRetrievedMessageCount();
       // Get a specific retrieved message
       Message* getRetrievedMessage(int pos);
       // Get the full collection of the retrieved messages
       ObjectCollection* getAllRetrievedMessages();

Should you want to manually retrieve messages from the crank without specifying this in the psySpec, you can ask the Messenger object:

   // Retrieve additional messages from a source
   ObjectCollection* retrieveMessages(const JString& retrieveFrom,
    const JString& retrieveSpecXML);

where retrieveSpecXML uses the same format as you would have used in the psySpec. 

Note: The nature of dynamically loaded libraries where they are loaded and unloaded periodically means that static and global variables loose their meaning. When creating internal crank functions do not use global or static variables in your code. Furthermore, on some UNIX systems such as Linux you will get errors when trying to load a library with static or global variables, usually complaining about an undefined symbol: __dso_handle. If you see this, go through your code and remove all global and static variables.