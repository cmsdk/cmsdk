# Using psyProbe {#psyprobe}

### DUMMY PAGE psyprobe

psyProbe is the web-based monitoring interface for Psyclone. With it you can view the internals of Psyclone at run-time. Before you can use psyProbe you have to run Psyclone.
Use psyProbe by typing the following into the URL box on your browser: 

  <http://[machine]:[psyclonePort]>

 


where [psyclonePort] is the port that Psyclone was started up with, or assigned to psyProbe in the psySpec (the default is 10000 if nothing is specified), and [machine] is either the IP address for the machine that Psyclone is running on, or its name, or the word "localhost".

**For example**, if your machine's name is *"matrix"*, and Psyclone is running on port 10000, you would type http://matrix:10000 into your browser. You can also type http://localhost:10000

**Notice**: You may have to use *http://* in the path since most browsers do not default to this protocol by themselves. More specifically, when you use psyProbe you are not using port 80 on the computer (the default port for http transfer); the browser may not know to default to the http:// protocol for any other port than port 80.

#### Posting Messages
Go to the Post Message page by clicking on the post_message_page_:: link on the main psyProbe page (if you have Psyclone currently running you can go there now by clicking here). 

To post a message to a Psyclone Whiteboard you need to specify three things, at a minimum: Who the message is from, which Whiteboard(s) will receive the message, and what type the message is. You specify the sender in the first panel (the one called from). The to on the page contains a list of the Whiteboards — check off one or more Whiteboards. The third panel on the page, post_message_::, allows you to manually key in the message type, and the message content, if you have any. When this is done, you can post your message by pressing the 'post' button at the bottom.

In addition to this, you can make any module receive a 'cc' on your message. This is done in the from panel. A 'cc'd module will receive (i.e. it will be woken up by) the message no matter whether it registered for its type or not.

#### Whiteboards
Whiteboards can carry discrete messages and continuous streams. The two differ in that messages are like email: You receive a message and that it is that, unless you write back that is. Streams are like a phone call: You get a ring and you pick up, and there is something more coming. The whole thing continues until you hang up. During the phone call you know that what comes through is audio (as opposed to video or a fax).

Therefore, when you use psyProbe to view the content of the whiteboards, the stream samples do not show up in the same way as messages do, as they reside inside whichever streams they belong to Whiteboard. PsyProbe lists messages and streams separately, and you need to look at the streams to see your samples, whether they are video, audio, or something else.

Samples in streams normally come in rapid succession (10s or 100s each second). Usually one module post to a stream and one or more will be subscribers. As a result, each datasample doesn't have a named type, they all automatically live inside a stream, which lives in a whiteboard. Receivers of the streams can perform time-based searches inside the stream and don't only have to receive all the samples in turn.

#### Using Whiteboard Filters
The Whiteboard page allows you to filter the messages shown on the messages page. If you are using the recommended dot-delimited message name format (e.g. My.Example.Message.Type, Input.hearing.voice), you can construct filters using a star (*) as a wildcard.

For example, let's say that the Whiteboard contains the following 5 message types:

  *Namespace.Input.Message.One* 
  *Namespace.Output.Message.Alpha* 
  *Namespace2.Output.Message.Omega* 
  *Namespace3.My.Input.Message.One* 
  *Namespace3.My.Output.Info.Two*

The filter pattern * Namespace.* will result in only the first and second messages types showing on the page. The pattern Namespace3.My.Input.* will block all messages except the fourth in the list. The pattern * .Message.* will, in this example, show all messages except the last one.

This mechanism is very useful for making sure that messages that should be posted have been posted. The filtering is not limited to dot-delimitation; you can specify for example * M* and all messagetypes above above will be allowed; * essag* to exclude the last one.

The content filter allows you to filter on any content of the messages. If you use this feature only messages which contain a particular string in their content will be listed in the list. (Content refers to the payload of messages.) The input field in the content filter allows you to input any ASCII string for this purpose. For example, * clone* will filter out all messages except those which contain the string "clone" somewhere in the content.

The amount of content shown in the table can also be set with the radio buttons none, tiny, full. Selecting "none" will hide the content of the messages; selecting tiny will show the first line of the content (or a summary of the content, if Psyclone knows how to parse it); "full" will show the full content of all messages below each message in the table.

#### Using psyProbe With Satellites

If you want to use PsyProbe to see WBs running in the Satellite, you must put a copy of the full HTML tree (from the Psyclone distribution) next to the Psyclone Satellite Server in the directory where you run the Psyclone -satelliteserver command.

## *taken from psyprobe 2.0 requirements, technical implementation*
### Technical implementation

#### Existing Prototype
An existing prototype has already been created which uses JQuery to allow a static HTML page to dynamically load a single XML file containing hierarchical information about the full running system. Sections are automatically added to and removed from each of the main sections (system, nodes, catalogs, modules) and tabs are automatically setup and their content loaded from individual and separate HTML template files and filled in. JQuery was selected due to the vast number of standard features available including graphs, maps, etc. See Appendix C: Look of the PsyProbe 2.0 prototype for a screenshot of the current prototype and Appendix B: Look of the static PsyProbe page in Psyclone 2.0 for a screenshot of the current static information page in Psyclone 2.0.

#### Dynamic data and updates
Information is pulled from Psyclone as a given interval and used to dynamically update the information displayed on the page. Two types of interfaces to a running system are available.

#### Master Psyclone update
A single and all-encompassing XML file can be requested from Psyclone which can be used to layout and update all the information discussed above. If required for practical or performance reasons, individual parts of this can be made available using equally standard requests.

#### Additional Psyclone API
Psyclone also contains a vast set of web-based APIs that will allow an external entity to via the HTTP protocol to request or input information and even participate as a component in the running system. For examples, this API will be used when the user chooses to manually activate a subscription trigger or retrieve named component private data entries.

#### Segmented parts
Currently the prototype is based around small sections of dynamically loadable HTML template files. The section headlines (System, Nodes, etc.) are part of the main HTML file, but every other part are already loaded dynamically and duplicated for each part of the system by the client, not the server. This should make it much easier to create different entry points (i.e. main HTML files) which only includes the part that is required for this particular view, whether this is a whole section, a whole component or a tab inside a specific component.

#### Tabs
Tabs are loaded from small HTML template files and contain a few fixed name JavaScript functions which are dynamically edited to make them unique to the component before the HTML is added to the main page.

#### Custom Tabs
In essence, all the tabs are currently implemented as custom tabs and the only difference is that the main webpage knows about the fixed ones already. The custom tabs are listed in the master update XML file downloaded from Psyclone and this entry specifies the path of the template file to load. Custom files are created by the user and should follow the exact same design template that the standard tabs do.

#### Pinned Tab bar
The tab bars are either docked, floating or in a separate window. They all behave exactly the same in that the bar itself can be resized, allowing the individual tabs inside to rearrange themselves according to the horizontal and vertical settings of the bar – or allow the size of all the tabs to be resized and again causing the tabs to rearrange themselves based on the same settings. Each tab is simply a template HTML file loaded and filled in and the HTML inside each should handle the resizing and rearranging automatically. 

#### Feed views (text, images, video, audio)
The technical implementation of the feed viewing tabs depends on the data types. There will be standard and separate tab types available for viewing text, binary, XML, images and audio and video clips. The feed information in the main update XML will specify the available types that can be requested via the web API with a separate call. This will retrieve the most recent feed element by type and the local JavaScript will be used to display this on the tab page. If the data type is streaming audio or video a media playing component is required (such as Quicktime) and this can be pointed to a specific URL in Psyclone which will stream the data from the feed out over an HTTP socket.

#### Look and feel
The appearance scheme of the full system should be a named set of Cascading Style Sheets entries and it should be possible to switch easily between different named schemes on a systems settings downdown. The appearance scheme will contain information about colours, fonts, icons, text boxes, dropdown menus, check boxes, passive and active buttons, collapse/expand indicators and advanced components such as graphs and media players.

#### HTML and JavaScript
The prototype primarily uses the JQuery library to create dynamic visual effects, create new sections by filling in dynamically loaded template files, remove outdated sections and update all variable information across all elements of the page. The solution (for desktop browsers) must work equally well in all major browsers and the solution for mobile browsers should work equally well on all major phone browsers. This can include a growing set of HTML5 features.
