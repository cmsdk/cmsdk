Example modules {#examplemodules}
===========================================
This beta release comes with three examples.
The Ping/Pong example above which demonstrates simple messaging, subscriptions and shutting down the system. The PsySpec for this is located in Examples/pingpong.x. The full Examples header file looks like this:
~~~~~~~~~~~~~~~{.cpp}
#if !defined(_EXAMPLES_H_) 
#define _EXAMPLES_H_
#include "PsyAPI.h" 
namespace cmlabs { 
extern "C" { 
DllExport int8 Simple(PsyAPI* api); DllExport int8 Ping(PsyAPI* api); DllExport int8 Shutdown(PsyAPI* api); DllExport int8 Print(PsyAPI* api); DllExport int8 Signal(PsyAPI* api);
} 
} // namespace cmlabs #endif //_EXAMPLES_H_
~~~~~~~~~~~~~~~
It includes the PsyAPI.h header file and declares extern the crank functions inside. A crank function returns an unsigned 8-bit integer, normally 0, when it completes. In the future we may add significance to the return value, but for now this is just ignored.
The PsyAPI object is the C++ object that the crank function uses to communicate with the rest of the system, such as waiting for new triggers, posting output messages, working with parameters, etc. This is very closely related to the old Messenger object in Psyclone 1.x. See the next section for more details.
The Examples source file includes the Examples.h file, declares the cmlabs namespace and then implements each of the crank functions.
A crank is the function that is run when a module is triggered. The simplest crank function is the default crank that posts all the module's output messages and then exits:
~~~~~~~~~~~~~~~{.cpp}
int8 Simple(PsyAPI* api) {
DataMessage* inMsg; const char* triggerName; 
if (api->shouldContinue()) {
if ( inMsg = api->waitForNewMessage(0, triggerName)) 
api->postOutputMessage(); }
return 0;
}
~~~~~~~~~~~~~~~
When waiting for the input message you have to provide a const char pointer that will point to the name of the trigger given in the PsySpec. The time to wait is given in milliseconds. If you don’t provide a post name for the posting you will post all your posts at the same time. If you do provide a name, all posts with this name will be posted. You can provide a message too, if you don’t a default empty one will be used.
The crank doesn’t have to exit and can continue running until it no longer should. The Ping crank does this:
~~~~~~~~~~~~~~~{.cpp}
int8 Ping(PsyAPI* api) { char* myName = new char[256]; api->getModuleName(myName, 256); api->logPrint(0,"[%s] started running...", myName); DataMessage* inMsg, *outMsg; int64 counter = 0, start, end; const char* triggerName; bool print = false; while (api->shouldContinue()) { if ( inMsg = api->waitForNewMessage(100, triggerName)) { outMsg = new DataMessage(); if (_stricmp(triggerName, "Ready") == 0) { api->logPrint(0,"[%s] got start message type '%s'...", myName, api->typeToText(inMsg->getType()).c_str()); start = GetTimeNow(); print = true; } else if (_stricmp(triggerName, "Ball") == 0) { if (!counter++) start = GetTimeNow(); else if (counter % 10000 == 0) { end = GetTimeNow(); if (print) api->logPrint(0,"[%s] got msg %llu, time per msg: %.3fus...", myName, counter, (double)(end-start)/(2*10000.0)); start = GetTimeNow(); } if (counter % 100000 == 0) { api->postOutputMessage("Done", outMsg); outMsg = NULL; } } else { api->logPrint(0,"[%s] got other message type '%s'...", myName, api->typeToText(inMsg->getType()).c_str()); delete(outMsg); outMsg = NULL; } if (outMsg) api->postOutputMessage("Ball", outMsg); } } delete [] myName; return 0; }
~~~~~~~~~~~~~~~
We start by reading our own name and use LogPrint to print to the main console. The format is similar to printf and the log values are for debug level filtering.
Here we actually look at the trigger name and post to a named post with a message that we create. Messages are now DataMessage objects and they have much the same capabilities as the old Messages, but are far more efficient.
The second example works with Signals:
~~~~~~~~~~~~~~~{.xml}
<psySpec> <library name="Examples" library="Examples" /> <!-- auto path and name per OS (xx.dll or libxx.so) --> <module name="Ping"> <trigger name="Start" type="Psyclone.Ready" /> <crank name="signal" function="Examples::Signal" /> <signal name="Output" type="My.Signal.1" /> <signal name="Input" type="My.Signal.2" /> <post name="Done" type="Psyclone.Shutdown" /> </module> <module name="Pong"> <trigger name="Ready" type="Psyclone.Ready" /> <crank name="signal" function="Examples::Signal" /> <signal name="Input" type="My.Signal.1" /> <signal name="Output" type="My.Signal.2" /> <post name="Done" type="Psyclone.Shutdown" /> </module> </psySpec>
~~~~~~~~~~~~~~~
We still use the Psyclone.Ready message to wake up both modules, but now we communicate exclusively via global signals. If you emit a signal this is broadcast to every space in the whole system. Any module can then wait for the arrival of a signal and they will all be triggered at the same time with the data. No subscription is needed, you merely refer to signals by the name you give it in the PsySpec for the individual module. Context changes are also signals so you can use this to wait for contexts to change.
The third example demonstrates the use of contexts:
~~~~~~~~~~~~~~~{.xml}
<psySpec> <library name="Examples" library="Examples" /> <!-- auto path and name per OS (xx.dll or libxx.so) --> <module name="Startup"> <trigger name="Ready" type="Psyclone.Ready" /> <post name="done" context="My.Context.1" /> </module> <module name="Print1"> <context name="My.Context.1"> <trigger name="Ball1" type="ball.1" interval="100" /> <crank name="print" function="Examples::Print" /> <post name="done" context="My.Context.2" /> </context> </module> <module name="Print2"> <context name="My.Context.2"> <trigger name="Ball2" type="ball.2" interval="100" /> <crank name="print" function="Examples::Print" /> <post name="done" context="My.Context.1" /> </context> </module> </psySpec>
~~~~~~~~~~~~~~~
Here we see that instead of posting types we post contexts that effectively change the system contexts. Contexts still work the same in that we can have multiple root contexts (in this example Psyclone and My) and for each root context we can have one and only one active subcontext (either My.Context.1 or My.Context.2). In this example we also see the use of automatic timed triggers that will trigger the module automatically every 100ms.