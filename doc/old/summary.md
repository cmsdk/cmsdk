Psyclone summary {#summary}
====================
Psyclone is a general-purpose platform for deploying multiple processes with powerful message and stream communications. It employs a simple yet powerful publish-subscribe mechanism that allows you to quickly create and connect modules and make them
interact.
Modules can be regarded as small stand-alone programs that get an input and give an output. For example you could have a face recognition module that takes pictures as input and can classify whether you can see a face or not. In autonomous agents you may want to combine this module with some motor modules that direct the camera to a face or something similar. You can use Psyclone to easily connect these modules even if they are running on different computers and/or if they are written in different programming languages. To be able to do this, modules have the so called PsyAPI object that lets them communicate with Psyclone and with other modules. The modules pass this object to their functions (called cranks) as a parameter.

Crank example:
~~~~~~~~~~~~~~~{.cpp}
int8 Simple(PsyAPI* api) { 
DataMessage* inMsg; 
     const char* triggerName; 
     if (api->shouldContinue()) { 
          if ( inMsg = api->waitForNewMessage(0, triggerName)) 
              api->postOutputMessage(); 
     } 
     return 0; 
}  
~~~~~~~~~~~~~~~
In this simple example the module asks the system through the PsyAPI whether it should continue and, if the answer is yes, it will wait until a new message arrives and will afterwards post it's own message to Psyclone. How does the PsyAPI receive messages and when can it continue and when does it have to wait?

Psyclone is based on the Publish-Subscribe principle. That means that every module decides for themselves which messages it wants to receive. Every message that a module sends out (called post) is copied by the local Psyclone node and then forwarded to all modules that subcribed to that post. This subscription is based on Triggers. Every message has a triggertype and the module can tell the Psyclone node to send them all messages with a particular triggertype. Psyclone also includes Whiteboards that can subscribe to messages or can receive them directly from the modules. ### In contrast to the node, Whiteboards store messages and thus modules can retrieve messages from the past from the Whiteboard if they suddenly become relevant for them.### Why should old messages become relevant for a module? The trigger-system is based on contexts. A context is a state that the whole system is in and can be changed by any module. Modules can have multiple contexts in which they are active. Only if the right context is active will the module be woken up by the triggers it is subscribed to. 
So when the context changes, different data than before could become important and then the module can retrieve older messages from a whiteboard. 

![test](/images/context-crank-combinations.jpg)

*Fig.1 If context A.B is active, the module is woken up by any message with triggertype t1 and t2 and will fire crank 1. The same crank can be fired in a different context by a different trigger. If the context changed to A.C but you still want to continue to call crank 1 it will be fired by triggers from type t3. It could also be one of the triggers from context A.B. The same trigger can also activate two different cranks as for example t2 does it: depending on whether A.B or X.Y is in context, crank 1 or crank 2 get activated. It's also possible that both contexts are active at the same time and thus both cranks get activated. Crank 2 and 3, however, can not be activated at the same time since they are in contexts with the same root, so they cannot be in context at the same time.*

##### Modules
A module can have only one crank per context, but multiple triggers per context. This allows  for all kinds of combinations as you can see in figure 1. Especially since multiple contexts can be active at the same time – as long as they have different roots. So it is for example possible that battery.low and people.present is in context at the same time but not battery.low and battery.full since that would contradict each other.  
So how do you build a module for psyclone? 
It's important to understand the distinction between the PsySpec, the setup specification loaded at startup of Psyclone and the code for the modules, compiled to dlls. The idea behind this separation is modularity: the cranks in the module's dll get input and return output but they don't know where the input came from and where the output goes. This is defined in the PsySpec. Input and output can be anything like subscriptions, signals, retrieves or queries. This makes it easy to switch the source or the type of the input data or the destination of the output in the PsySpec without touching the actual code. The PsycSpec works like a scaffolding that attaches all the different modules, whiteboards and catalogs with each other and knows the paths in the directory to each relevant file.

A __catalog__ is a component that can provide access to large amounts of data and that has a query mechanism built in. If the user has created, for example, a address book catalog, any module can query this catalog for a specific address or a list of addresses and the catalog would return it. There are 3 built-in catalogs: the FileCatalog, which you can query for the content of files when you provide their location in the system, ###the DataCatalog, for permanently storing key = data blob to disk, for retrieval even after system restart, and the MessageDataCatalog for storing data easy to query via the web interface.

__Example for the fileCatalog:__ 

###### In the PsySpec: 
Addding the catalog: 
~~~~~~~~~~~~~~~{.xml}
<catalog name="MyFileCatalog" node="Main" type="FileCatalog" root="./"> 
  <parameter name="ReadOnly" type="String" value="no" />  
</catalog> 
~~~~~~~~~~~~~~~
Adding the query: 
~~~~~~~~~~~~~~~{.xml}
<query name="MyFileQuery" source="MyFileCatalog" subdir="FileLocation" ext="txt" binary="yes" />  
~~~~~~~~~~~~~~~

Adding the crank: 
~~~~~~~~~~~~~~~{.xml}
<crank name="RetrieveTest" function="Examples::RetrieveTest" /> 
~~~~~~~~~~~~~~~
###### In the code of the crank: 
~~~~~~~~~~~~~~~ c++
char* result = NULL; 
    status = api->queryCatalog(&result, datasize, "MyFileQuery", "test", "read"); 
~~~~~~~~~~~~~~~
this call to queryCatalog(), a method of the PsyAPI, asks the FileCatalog to read a file called test and return the content 

### The most common ways to transfer messages
**Direct Path**: modules can send messages directly to other modules, whiteboards or catalogs 
~~~~~~~~~~~~~~~{.xml}
<post name="message" type="exampleTriggerType" maxage="2000" to="Whiteboard.1"/> 
~~~~~~~~~~~~~~~
**Subscriptions:** The module (or catalog or whiteboard) can subscribe to messages with certain triggertypes and thus automatically receive a copy of those messages 
~~~~~~~~~~~~~~~{.xml}
<trigger name="exampleTrigger" type="exampleTriggerType" maxage="2000" after="1000"/> 
 ~~~~~~~~~~~~~~~
**Signals:** 
~~~~~~~~~~~~~~~{.xml}
<signal name="Input" type="My.Signal.2"/> 
<signal name="Output" type="My.Signal.1"/><signal name="Input" type="My.Signal.2"/> 
 ~~~~~~~~~~~~~~~
**Retrieves:**  Modules can actively request messages from a whiteboard. These messages are sorted by an index that can be specified by the modules that originally sent the messages to the whiteboard 
~~~~~~~~~~~~~~~{.xml}
<retrieve name="exampleRetrieve" source="exampleWhiteboard" key="exampleDataEntry" keytype="integer" /> 
~~~~~~~~~~~~~~~
**Queries:** similar to retrieves, only that a catalog is asked to return messages instead of a whiteboard. 
~~~~~~~~~~~~~~~{.xml}
<query name="MyFileQuery" source="MyFileCatalog" subdir="FileLocation" ext="txt" binary="yes" /> 
~~~~~~~~~~~~~~~
-- | --
------------ | -------------
psySpec | A central file that is used to manage a system consisting of various programs from one location via Psyclone. Modules, messages, cranks etc. (see below) are all defined here in a really nice and tidy manner. 
module  | Processing in Psyclone is done by entitities called modules, which can be free-standing executables or libraries loaded into Psyclone. Vision modules process video; display modules display images; planning modules make plans. 
contexts | A system for defining and managing a set of architecture-wide global variables. Modules in Psyclone can be "context-driven": A module can use one or more contexts to decide when it should be active. The hierarchy is like this in a module: 
trigger | Modules in Psyclone use the concept of triggers to get alerted when they have input data to process. A module registers with Psyclone for triggers and it will get woken up when any of those triggers get posted. 
whiteboard |A special type of blackboard or infobus that acts as a central connection point for modules. 
crank | What a module does when it is awoken with a trigger - if it's a vision module it will process video; if it's a display module it will display something. Think "method".  
stream | A data stream is a "named pipeline" which you can use to send bytes from one place to another. 
Message | Messages in Psyclone can travel as XML over TCP/IP connections (in-memory for internal modules). 

