Detailed Tutorials {#detailedtutorials}
===========================================
## note this is just copied from the old manual and has to be updated and summarized
text taken from <a href="psycloneManual3.html">here</a> There is possibly more useful information in there. Also some parts from what is currently in the alphabetical list will be interwoven with the entries here.
This will later be in alphabetical order

## Binary data in messages {#binarydatadt}
Psyclone has built-in support for binary content in messages. You would normally use the object slot in a message for this content, just as you do when sending other non-text information, such as dictionaries or collections. If this object has binary content it will automatically be attached as a binary attachment to the message when transmitted across the network.

In both internal and external C++ and external Java modules you can add raw binary data to a message using the DataSample object. An example for C++ could be:

~~~~~~~~~~~~~~~{.cpp}
int len = 5322;
char* data = new char[len];
memset(data, 0, len);
Message* msg = new Message("", "", "");
DataSample* sample = new DataSample();
sample->giveData(data, len);
msg->setObject(sample);
~~~~~~~~~~~~~~~

In Java, you do almost the same:

~~~~~~~~~~~~~~~{.java}
int len = 5322;
byte[] data = new byte[len];
for (int n=0; n<len; n++)
data[n] = 0;
Message msg = new Message("", "", "");
DataSample sample = new DataSample();
sample.data = data;
msg.object = sample;
~~~~~~~~~~~~~~~
Now you can send your message as normal.

You can create your own objects to carry binary information. In Java you need to inherit from either the DataSample object or from the BaseObject – please see the DataSample object to see what you need. In C++ you need to obtain the version of the CoreLibary source code which matches the version of the AIR Plug or the SDK you are using and then implement a third party object. See the CoreLibrary homepage for more information on how to do this.
# Catalogs {#catalogsdt}



## File Catalog {#filecatalogdt}

The File Catalog allows any module to read and write files from/to a central location without having to worry about which computer the module is running on.  

 

A File Catalog is defined in the PsySpec like this: 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="MyFiles" type="FileCatalog" root="./"> 

<parameter name="ReadOnly" type="String" value="no" /> 

</catalog> 
~~~~~~~~~~~~~~~
 

The optional parameter ReadOnly defaults to 'no', but can be set to 'yes' which will prevent write operations. 

 

Any crank code can access the catalog by defining a named query in the PsySpec like this: 

 
~~~~~~~~~~~~~~~{.xml}
<module name="Test"> 

… 

<query name="MyFiles" source="MyFiles" subdir="test" ext="txt" binary="yes" /> 

… 

</module> 
~~~~~~~~~~~~~~~
 

And the crank code can execute the query this way: 

 
~~~~~~~~~~~~~~~{.cpp}
uint32 datasize = 0; 

char* result = NULL; 

uint8 status = api->queryCatalog(&result, datasize, "MyFiles", "test", "read"); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully retrieved %u bytes from file catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'read' timed out for file catalog..."); 

else 

api->logPrint(1, "Retrieve 'read' failed (%u) for file catalog...", status); 

… use binary data in result … 

delete[] result; 
~~~~~~~~~~~~~~~
 

which will read a file from the root dir ./, sub dir test called test.txt, i.e. ./test/test.txt. 

 

Likewise a file can be written by 

 
~~~~~~~~~~~~~~~{.cpp}
data = utils::StringFormat(datasize, "Hello World"); 

status = api->queryCatalog(&result, datasize, "MyFiles", "test", "write", data, datasize); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully wrote %u bytes to file catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'write' timed out for file catalog..."); 

else 

api->logPrint(1, "Retrieve 'write' failed (%u) for file catalog...", status); 

delete[] data;
~~~~~~~~~~~~~~~ 
## data catalog  <a name="datacatalog"></a>
The Data Catalog allows any module to read and write data from/to a central datastore without having to worry about which computer the module is running on.  

 

A Data Catalog is defined in the PsySpec like this: 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="MyData" type="DataCatalog" interval="2000" root="./mydata.dat" /> 
~~~~~~~~~~~~~~~
 

which puts the central datastore in the file ./mydata.dat and writes from memory to file every 2 seconds. 

 

Any crank code can access the catalog by defining a named query in the PsySpec like this: 

 
~~~~~~~~~~~~~~~{.xml}
<module name="Test"> 

… 

<query name="MyData" source="MyData" /> 

… 

</module> 
~~~~~~~~~~~~~~~
 

And the crank code can execute the query this way: 

 
~~~~~~~~~~~~~~~{.cpp}
uint32 datasize = 0; 

char* result = NULL; 

status = api->queryCatalog(&result, datasize, "MyData", "test", "read"); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully read %u bytes from data catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'read' timed out from data catalog..."); 

else 

api->logPrint(1, "Retrieve 'read' failed (%u) from data catalog, ok if starting with a fresh catalog ...", status); 

delete[] result; 
~~~~~~~~~~~~~~~ 
 

which will read an entry from the datastore. 

 

Likewise data can be written by 

 
~~~~~~~~~~~~~~~{.cpp}
data = utils::StringFormat(datasize, "Hello2 World"); 

status = api->queryCatalog(&result, datasize, "MyData", "test", "write", data, datasize); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully wrote %u bytes to data catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'write' timed out to data catalog..."); 

else 

api->logPrint(1, "Retrieve 'write' failed to data catalog (%u)...", status); 

delete[] data; 

delete[] result;
~~~~~~~~~~~~~~~ 

## replay catalog {#replaycatalogdt}
The Replay Catalog can be used in one Psyclone system to simply record certain message activity to disk - and later in a different Psyclone system it can be used to playback these messages as they happened in the first system. 

 

One example could be to record all perception from a robot over a 2 minute period, such as video, audio, sensors, etc. and potentially even the output of some of the processing of these. On a different computer these recorded messages can be used in a simulated system to get exactly the same messages without actually having to have the robot present and running. 

 

The recording phase can be done by simply defining the catalog in the PsySpec and telling it what and how much to record: 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="Replay1" type="ReplayCatalog" root="./replay1" maxsize="10240000" maxcount="2000"> 

<trigger name="Video" type="robot.sensor.video" /> 

<trigger name="Bumper" type="robot.sensor.bumper" /> 

</catalog> 
~~~~~~~~~~~~~~~
 

This will receive all messages of types 'robot.sensor.video' and 'robot.sensor.bumper' and store them to disk in ./replay1, only keeping the last 2000 messages and only keeping up to 10kb of data, whichever limit is hit first. 

 

Then the files can be copied to another computer and to replay the messages you simply define the same catalog with different parameters: 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="Replay1" type="ReplayCatalog" root="./replay1"> 

<post name="Video" type="robot.video" /> 

<post name="Bumper" type="robot.bumper" /> 

</catalog> 
~~~~~~~~~~~~~~~ 
 

This will play back the messages at the same interval they were recorded, but this time with different message types, just the trigger/post names have to match. 

 

Two additional parameters can be used on playback 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="Replay1" type="ReplayCatalog" root="./replay1" interval="1000" rotate="yes"> 

<post name="Video" type="robot.video" /> 

<post name="Bumper" type="robot.bumper" /> 

</catalog> 
~~~~~~~~~~~~~~~
interval="1000"  -  overwrites the posting interval to 1 second, regardless of the recorded message timing 
rotate="yes"  -  when the last message has been replayed go back and start playing from the beginning 
## messageDataCatalog catalog <a name="MessageDataCatalog"></a>
The MessageDataCatalog Catalog can be used to collect messages from the whole system, keep the most recent versions of them and allow anyone to request parts of or the whole message from a web browser by easy to remember names. This means no custom programming or data in the modules at all. 

 

An example of a MessageDataCatalog Catalog entry in the PsySpec could be: 

 
~~~~~~~~~~~~~~~{.xml}
<catalog name="MessageDataCatalog" type="MessageDataCatalog"> 

<trigger name="VideoFrame" type="video.output.frame" /> 

<trigger name="SampleXML" type="video.output.frame" /> 

<trigger name="SampleText" type="video.output.frame" /> 

<trigger name="SampleHTML" type="video.output.frame" />--> 

<setup> 

<store name="VideoFrame" trigger="VideoFrame" key="VideoDataFrame" keytype="data"  

 datatype="raw" mimetype="image/bmp" maxkeep="1" maxfrequency="1" /> 

<store name="SampleJSON" trigger="VideoFrame" mimetype="json" maxkeep="1" maxfrequency="1" /> 

<store name="SampleXML" trigger="VideoFrame" mimetype="xml" maxkeep="1" maxfrequency="1" /> 

<store name="SampleText" trigger="VideoFrame" mimetype="text" key="content" maxkeep="1" maxfrequency="1" /> 

<store name="SampleNum" trigger="VideoFrame" mimetype="text" key="VideoWidth" maxkeep="1" maxfrequency="1" /> 

<store name="SampleSize" trigger="VideoFrame" mimetype="text" key="size" maxkeep="1" maxfrequency="1" /> 

<store name="SampleType" trigger="VideoFrame" mimetype="text" key="type" maxkeep="1" maxfrequency="1" /> 

<store name="SampleTime" trigger="VideoFrame" mimetype="text" key="time" maxkeep="1" maxfrequency="1" /> 

<store name="SampleTimeText" trigger="VideoFrame" mimetype="text" key="timetext" maxkeep="1" maxfrequency="1" /> 

<!--<store name="SampleHTML" mimetype="html" key="html" maxkeep="1" maxfrequency="1" />--> 

</setup> 

</catalog> 
~~~~~~~~~~~~~~~ 
 

Here we see the catalog subscribing to messages like any other module, each with a trigger name as usual. 

 

The custom setup XML part then defines named entries ('VideoFrame', 'SampleJSON', etc.) which can be queried by anyone like this: 

 
~~~~~~~~~~~~~~~
   GET /api/query?from=MessageDataCatalog&query=VideoFrame 
~~~~~~~~~~~~~~~
 

The return content-type is specifically specified in the setup XML so the one calling the URL doesn't have to know. 

 

Each store entry can have the following parameters: 
 name | The name used in the request | Required
 --|--|--
 trigger | Which trigger message to get the information from | Required 
 key | The name of the user entry inside the message - can be header fields such as 'time', 'type' too. 'time' will return the number of microseconds since year 0 - 'timetext' will return a nicer textual description of the date and time | If not specified the whole message is returned as either JSON, XML, HTML or TEXT, depending on the mimetype
 keytype | The type of key | Not currently used
 datatype | The type of binary data, mainly used for bitmaps | If set to 'raw' the code knows to expect raw pixels and need the message to contain width and height information too - so construct a valid bitmap format to return. In the future we may support other types of data types.
 mimetype | The mime type used to help the browser display the data correctly |Required, mime types are standard browser mime types such as 'application/json', 'text/html', 'text/xml', etc. Can also be short versions, 'json', 'xml', 'text', etc.
 maxkeep |How many messages (trigger history) to keep|Default is 1, later may support returning vectors of keys from several historical messages
maxfrequency |The maximum number of messages to keep per second | If set to 1 it will only keep 1 message every second and ignore new messages for that trigger until another 1 second has passed

## Contexts {#contextsdt}
Contexts are used to manage a collection of modules by simultaneously turning them on and off, and to make them behave differently in different situations. Contexts are very helpful to manage the complexity of a large number of modules that typically are not all relevant for the operation of a system at the same time. Psyclone modules can be context-driven in that each module can be set up to have a specific context in which it can run. If that context is not active, the module will not be woken up with any messages it has subscribed to.

If no context is specified for a module they default to belong to the context Psyclone.System.Ready.

You can get some hands-on experience with contexts by looking at the dominosPsySpec example.

Contexts are hierarchically defined, forming a tree. For example, this is a context tree:

SoB.[Alive, Dead]
SoB.Alive.[Awake, Asleep]

In this context tree, the root is called SoB (short for state of being). The root has two branches, Alive and Dead. The branch Alive has two leafs, Awake and Asleep. (Dead has no branches.) To see how this tree is used at run-time, let's look at a hypothetical system that uses this context tree. In this hypothetical system we would have modules specifically designed to post contexts, which ever one is relevant at any time. When the system starts up the context SoB is posted by some bootstrapping module. When this happens another module (called e.g. BeingAlive.100) posts SoB.Alive if the system is considered "alive", otherwise it posts SoB.Dead (the module would presumably run some tests to see if the system actually qualifies for the "alive" label). If SoB.Alive is posted, then another module (called e.g. BeingAwake.200) posts SoB.Alive.Awake if it considers the system to be "awake" (whatever that means in this hypothetical system).

Now we can look at how modules use these contexts to specify when they should be active and when not. For example, we might have a module called LookAround.150, whose job it is to move the sensors of a hypothetical being in the direction of a sound when it receives one. This module should not be running if the being is dead or if the being is asleep. So when we choose a context for this module, we choose the context in which it should be active, which is in this case SoB.Alive.Awake.

In the psySpec this is indicated by the syntax:

<context name="SoB.Alive.Awake">

Now, if at this point any module in the system posts the context SoB.Dead, the context SoB.Alive.Awake will not be active any more, since the branch Dead replaces the branch Alive in the context tree, and the active context will instead be SoB.Dead. At this point the module LookAround.150 will not be triggered by any message types it has subscribed to, because it needs the context SoB.Alive.Awake to be active.

Multiple roots are allowed. If you post for example the context SoB.Alive.Asleep and then post the context System.Ok, the two context trees have different roots, and will thus co-exist with no interference.

Notice that context trees are designed by you, the user. The only restrictions on them is that they be dot-delimited to designate branches.

Contexts are posted (i.e. announced to the system) by using a special message type called Psyclone.Context. In the posting section of a module in the psySpec (or, alternatively, in the XML as constructed by an external module), the type of the message should be Psyclone.Context:[context tree], where [context tree] is the dot-delimited context tree.

Example:
Psyclone.Context:SoB.Alive.Awake

When a context is posted, the context gets "switched" only if a current branch in the tree gets replaced by a new branch. For example, if the current context is SoB.Alive.Asleep and SoB.Alive.Awake gets posted, the context will be switched. And if SoB.Alive.Asleep.REMsleep gets posted, this context will be switched on for any module which had listed this as their context. But if SoB.Alive gets posted while SoB.Alive.Asleep is active, nothing happens.



RELATED: rocket example running the domionsPsySpec.xml



Contexts are used to make a module behave differently in different situations. For example, a vision module would want to use a different algorithm if the scene is bright from if the scene is dark. Instead of managing this manually inside the code of the module, contexts allow a module to change from one type of operation to another by listening to an external signal.

The way contexts are typically used is that in each Context a module will have a separate set of inputs and outputs. By looking at currently active context and a module's spec, anyone, including the system desginer and other runtime modules, can become aware of what the module is doing.

In the module spec each context will have at least one Phase, even if not directly specified. A context can have more than one Phase, in which case the module will cycle through the phases inside this context, until the relevant system context is changed. When this happens, the module would either change to a new set of Phases inside the new context, or go out of context and become idle, if the module has no configuration for the new context.

The system as a whole will usually have a hierarchy of contexts and many of these may be active at the same time. However, a module will only have one active Phase in one of these contexts, and it should not register for more than one simultaneously active contexts.

In a module registration without context and without phases looks like this:
<module name="MyFirstModule" type="internal">
<trigger from="AIRCentral" type="My.First.Type" />
<crank name="cm::MyFirstModuleCrank">
<post to="AIRCentral" type="My.Second.Type" />
</module>

This module only has one phase which belongs to the default context, Psyclone.System.Ready. This context will never be switched off until the system is shutting down. If this module tries to switch phase, it will end up in the same state as before.

If you define more than one context for a module, the module will be in one and only one (or none) of these contexts at any given time.

<module name="Domino-60" type="external">
<context name="Scene.Bright">
<phase name="Send first message group">
<triggers from="Whiteboard.2">
<trigger after="1000" type="DEMO.Domino.50"/>
</triggers>
<posts to="Whiteboard.2">
<post type="DEMO.Domino.60" />
<post type="DEMO.Domino.Flip" phasechange="yes" />
</posts>
</phase>
</context>
<context name="Scene.Dark">
<phase id="Send second message group">
<triggers from="Whiteboard.2">
<trigger after="1000" type="DEMO.Domino.50"/>
</triggers>
<posts to="Whiteboard.2">
<post type="DEMO.Domino.60" phasechange="yes" />
<post type="DEMO.Domino.Flop" />
</posts>
</phase>
</context>
</module>

If you specify more than one phase in each context, the module will behave as explained in the phase tutorial.

Internal modules will automatically be using the correct crank in the correct phase in the currently active context. If they go out of context they will stop running and wait to be in context again.

External modules will have to detect whether they are in context at all, and if so, which one. The main loop should be checking this, as shown here:

C++

while (true) {
if (plug->inContext()) {
if ((triggerMessage = plug->waitForNewMessage(3000)) == NULL) {
// read the crank name
crankName = plug->getCurrentCrankName();
// read context name if you like
context = plug->getCurrentContextName();
// read phase name if you like
phase = plug->getCurrentPhaseName();
// ... process trigger ... //
}
}
else {
plug->wait(50);
}
}

Java

while (true) {
if (plug.inContext()) {
if ((triggerMessage = plug.waitForNewMessage(3000)) == NULL) {
// read the crank name
crankName = plug.getCurrentCrankName();
// read context name if you like
context = plug.getCurrentContextName();
// read phase name if you like
phase = plug.getCurrentPhaseName();
// ... process trigger ... //
}
}
else {
Utils.pause(50);
}
}



## External Spaces {#externalspacesdt}
Any Catalog, Whiteboard or Module can be placed in a process separately from the Node process
<catalog space="MySpace" name="MyData" type="DataCatalog" interval="2000" ... />
If the Space doesn’t already exist it will be automatically started up as a separate system process and it is automatically destroyed when the Psyclone Node shuts down. A Node can have an unlimited number of Spaces attached and if one Space crashes because a component inside performs an illegal action only the components in this Space is affected and can be restarted (not currently done automatically).



## Messages {#messagesdt}
Message types should follow the convention to use a dot-delimited string where the first part indicates the root namespace, and the subsequent segments describe the type of content that the message relates to, of increasing specificity from left to right.  
Messages usually contain user data entries that contain information for the recipient of the messages and can contain many entries of small or very large amounts of data (from integers to binary data blobs) 
**Example:** 
In the PsySpec: 
~~~~~~~~~~~~~~~{.xml}
<post name="exampleMsg" type="Robot.Status" /> 
~~~~~~~~~~~~~~~
In the code of the crank: 
~~~~~~~~~~~~~~~{.cpp}
msg = new DataMessage() 
msg->setString("RobotStatus", "Almost ready") 
msg->setInt("BatteryLevel", 78) 
api->postOutputMessage("exampleMsg", msg); 
~~~~~~~~~~~~~~~
Data Entries can be of types String, Int, Float, Time, Binary and Message. 

You can also set entries of type arrays (integer indexes) and maps (string indexes): 
~~~~~~~~~~~~~~~{.cpp}
msg->setInt(1, "myarray", 2); 
msg->setInt(2, "myarray", 2); 
msg->setInt(3, "myarray", 2); 
msg->setInt(4, "myarray", 2); 
msg->setInt(5, "myarray", 2); 

Int val = msg->getInt(3, "myarray"); 
~~~~~~~~~~~~~~~
Or the whole array: 
~~~~~~~~~~~~~~~{.cpp}
get 
std::map<int64, int64> arr = msg->getIntArray("myarray") 
set 
msg->setIntArray("myarray2", arr) 
~~~~~~~~~~~~~~~
This works for all types (int, float, string, time, data, messages) 

Same for maps 
~~~~~~~~~~~~~~~{.cpp}
msg->setInt("in", "mymap", 1); 
msg->setInt("out", "mymap", 2); 
msg->setInt("middle", "mymap", 3); 
msg->setInt("off", "mymap", 4); 

int val = msg->getInt("out", "mymap"); 
~~~~~~~~~~~~~~~
Or the whole array: 
~~~~~~~~~~~~~~~{.cpp}
get 
std::map<std::string, int64> arr = msg->getIntMap("mymap") 
set 
msg->setIntMap("myarray2", arr) 
~~~~~~~~~~~~~~~ 
SLOT NAME |	CONTAINS |	COMMENTS 
--|--|--
Type |	Dot-delimited type  |	Example: Input.Audio.Raw  
ID |	Global unique identification tag |	-  
From |	Name of module that posted the message |	  -
To | 	Name of whiteboard to receive the message 	|  - 
cc 	| Name of module(s) to receive a copy of the message (whether the module registered for its type or not) |	Modules listed will receive the message whether they registered for its type or not 
PostedTime |	Timestamp (in milliseconds) when the message left the module which posted it |	Ex: 2004.02.0001 12:20:41:129 Reflects the moment of the call to post( ) to within a microsecond or less, under normal operation 
ReceivedTime |	Timestamp (ms) when the whiteboard (component listed in the To slot) received the message |	Ex: 2004.02.0001 12:20:40:128  
Content & language  |	The content of the message — what the module whants to convey by posting it. Must be ASCII.The language that the Content is expressed in, e.g. XML, Lisp, Python, HTML, Java Script, or unspecified. |	The Content can be autoparsed by the whiteboard if it recognizes the message type and content format. 
InReplyTo |	A single Reference to the Message to which this Message is a reply, grouped by the Reference tag 	 | -
Stored |	Name of the whiteboard where the message is stored 	  | -
History |	References to past messages relevant to this one. |	This feature not fully implemented in version 0.5 
Comment |	Human-readable comment  |	Can be used optionally as an extension slot 

## Message and Module priorities {#mmpriodt}
There are two priorities in Psyclone, one for messages and one for modules and whiteboards. Each module has a priority setting that determines its relative priority for accessing CPU cycles — i.e. its runtime priority. Messages have a prority that determines their relative order of scheduling, both in the whiteboards and in the modules.

This dual priority mechanism allows the system to dynamically prioritize certain information paths over others, creating a hierarchy of data significance, from the absolutely critical to the supernumerary while also taking into account the computing cost of producing these.

To set this priority in the psySpec, do:

<module priority="3.0">
...
</module>

The message and runtime priority goes from 0.0 (highest) to 6.999... (lowst), following the OpenAIR specification:

0.0 - Highest. Reserved for I/O, network and other vital systems-related processes.
1.0 - Very High. Message scheduling; all whiteboards are at this level.
2.0 - High.
3.0 - Medium.
4.0 - Low.
5.0 - Even lower. Used for rather unimportant processes, e.g. logging.
6.0 - Lowest. Background, non-real-time and immaterial tasks.

When setting the runtime proirity of modules on this scale, Psyclone will translate the selected priority into an equivalently ranked priority of the operating system that the module is running on, maintaining the relative differences between the levels. This means that internal and external modules, as well as whiteboards, are balanced directly against other processes running on the same computer. (Psyclone is designed to make no more use of CPU cycles than necessary; its priority against other processes can be set on UNIX by doing

If no priority is specified for a module it defaults to 2.0; whiteboard priority defaults to 1.0. While the priority of whiteboards can be set, this is only recommended for unique situations. Priority 0.x is used for network activity and similar activities which are vital to normal functioning of Psyclone. (The priority of these cannot be modified).

To set the priority of whiteboards in the psySpec do:

<whiteboard priority="2.2">
...
</whiteboard>

While the priorities of modules and whiteboards can be set to any level, it is not recommended to put anything at the 0 (network) level as this can lead to fairly unpredictable system behaviors.

By default a message inherits the priority of the module that posts it. The module that posts a message can set its priority in the post() method.

In the code you can set the priority of a message by doing

msg.setPriority(3.3)

where 3.3 is a float. In the psySpec you can set the priority of a message by doing:

<post to="WB1" priority="2.2">My.Type.1</post>

## Module parameters {#moduleparametersdt}
All modules, internal and external, can have an unlimited number of parameters, which the module itself can control, but which other modules can interact with as well, both via a direct function call and via (low-level) messages, as explained below. Parameters work the same for both internal as well as external modules in all programming languages and the getParameter() function returns the same value for internal and external modules, local or remote.

Parameters are defined in the psySpec:

<module name="VideoServer1.Camera1" type="internal" >
<description></description>
<parameter name="Interval" type="Integer" value="200" step="50" default="100"/>
<parameter name="BitmapFiles" type="String" value="../../Video/Frame###.bmp" />
<parameter name="Repeat" type="String" value="Yes">
<collection>
<entry>Yes</entry>
<entry>No</entry>
</collection>
</parameter>
<parameter name="Resize" type="double" value="0.5" min="0.125" max="4.0"/>
<parameter name="Printlevel" type="Integer" value="0" min="0"/>
<context name="Psyclone.System.Ready">
<phase name="ReadBitmaps">
<triggers>
<trigger after="100" type="Psyclone.System.Ready"/>
<trigger from="Vision" after="100" type="Command.Camera1*"/>
</triggers>
<crank name="gui::BitmapVideoReader"/>
<outputstream name="Output" source="Input.Video.Camera1" />
</phase>
</context>
</module>

In this example the module VideoServer1.Camera1 defines five named parameters, two of which are strings, two are integers and one is a double (floating point). Each has an initial value and some have a default value which is set when the parameter is being reset.

Some define a step value, which will restrict the parameter from taking on values which are not an integer times the step value away from the initial value. Some specify a minimum and/or a maximum value that the parameter can have. Finally, one parameter specifies the full list of values that that parameter can ever have.

When working with ones own parameters one can use the following API, defined for the Messenger object for internal modules and the Plug object for both C++ and Java external modules.

To check that the module has a parameter by name:

bool hasParameter(String name);

To read one of the modul's own parameters, either as a string, integer or double:

String getParameterString(String param);
int getParameterInteger(String param);
double getParameterDouble(String param);

To se one of the module's own parameters, either as a string, integer or double:

bool setParameterString(String param, String value);
bool setParameterInteger(String param, int value);
bool setParameterDouble(String param, double value);

To reset a parameter back to its default value:

bool resetParameter(String param);

To tweak a parameter up or down one or more notches:

bool increaseParameter(String param, int steps);
bool decreaseParameter(String param, int steps);

To add another item value to an itemized parameter:

bool addParameterItem(String param, String value);
bool removeParameterItem(String param, String value);

When working with other modules' parameters one can use the following API, defined for the Messenger object for internal modules and the Plug object for both C++ and Java external modules.

When wishing to read another module's parameters:

String getParameterString(String module, String param);
int getParameterInteger(String module, String param);
double getParameterDouble(String module, String param);

The string version will always return either the string value of the knob value ("2", "2.54", "text") or if an error occurred the string PARAM_ERROR: <reason>

The two numerical functions in case of errors return

Java: ret = Parameter.PARAM_ERROR
C++:  ret = PARAM_ERROR

For setting another module's parameters:

bool setParameterString(String module, String param, String value, String notify);
bool setParameterInteger(String module, String param, int value, String notify);
bool setParameterDouble(String module, String param, double value, String notify);

notify is the name of the Whiteboard which should receive the notification messages. This is not needed for internal modules, as they already know which Whiteboard to post to, so the Messenger API does not include this last parameter for the functions or any of the functions below.

To tweak a parameter up or down one or more notches:

bool parameterIncrease(String module, String param, int steps, String notify);
bool parameterDecrease(String module, String param, int steps, String notify);

To add or remove an item from an itemized parameter:

bool parameterAddItem(String module, String param, String value, String notify);
bool parameterRemoveItem(String module, String param, String value, String notify);

To reset a parameter to its default value:

bool parameterReset(String module, String param, String notify);

You can also choose to create a new parameter in your own module manually from your code at any time:

public boolean addParameter(String paramname, String xml)

With this a module can add an additional parameter to itself using the same XML as is used in the psySpec, i.e.

<parameter name="Frequency" type="Integer" value="1" min="0" max="100" step="5" />

Note that paramname in the code must match the name="" XML parameter name in the psySpec.

One can also ask for the full list of parameters from another module, including each parameter's specification:

ObjectDictionary getParameterSpecs(String module)
Parameter getParameterSpec(String module, String parameter)

The former returns a dictionary of Parameter Specs and the latter just the one Parameter Spec which matches the name. Both can return null if no relevant information is found.

Armed with this spec one can look at the various spec fields, such as parameter.name, parameter.valueType, parameter.minValue, parameter.maxValue, but also the value of the parameter at the time of the request with parameter.value.

To set a parameter using a message, send the following message to the module (put the module's name in the cc field of the message):

message type = "PARAM_SET"
message content = "myparam=myvalue"

This is a standard Psyclone lowlevel command, using the low-level OpenAIR message API.

## Retrieves {#retrievesdt}

---------------------------------------------------

This is material from the former retrieves-entry in the list of concepts and terms. you may include this in this tutorial. If it is not needed just delete this section

A module can retrieve messages and streamed data from whiteboards. Which data type to retrieve can be specified in the psySpec and also done at runtime.
**Example** XML for such retrieval: 
~~~~~~~~~~~~~~~{.xml}
  <trigger from="WB1" type="Input.Sens.MultiM.Vision.Human.Found.True"/> 
   <retrieve from="WBX" type="Input.Sens.UniM.Hear.Human.Voice"/>  
  <retrieve from="WBX" type="Input.Sens.MultiM.Vision.Human"> 
     <latest>3</latest> 
     <lastmsec>8000</lastmsec> 
   </retrieve>  
~~~~~~~~~~~~~~~
The first retrieve asks to be sent a message of type Input.Sens.UniM.Hear.Human.Voice along with the message type Input.Sens.MultiM.Vision.Human.Found.True to which it has subscribed; the second retrieve asks to be sent a message of type Input.Sens.MultiM.Vision.Human along with that, but with the conditions that if there are more than 3 such messages in the last 8 seconds it will only return the 3 most recent of those. All conditions that are listed like that are ANDed together. 
Retrieve specs can be tied to particular contexts. For **example**,  
~~~~~~~~~~~~~~~{.xml}
<retrieve from="WBX" type="Input.Sens.UniM.Hear.Human.Voice"/> 
   <aftercontext>SoB.Alive.SoS.Awake.DialogOn.I-Have-Turn</aftercontext> 
<retrieve> 
~~~~~~~~~~~~~~~
means that all messages of type Input.Sens.UniM.Hear.Human.Voice will be retrieved if they were posted after the context SoB.Alive.SoS.Awake.InDialog.ItookTurn was posted (using postedtime). Other tags that are available are ` <duringcontext> `and `<beforecontext>`. The latter means any time before the context was posted (was active); the former means anything that is not included in the conjunction of the other two, that is, anything that is not retrieved by calling both `<beforecontext>` and `<aftercontext>` on the same context. During means any time between this context last became active and subsequently became inactive. The syntax can also specify these conditionals as parameters: 
~~~~~~~~~~~~~~~{.xml}
<retrieve from="WBX" type="Input.Sens.UniM.See.Person" beforecontext="SoB.Alive.SoS.Asleep"> 
~~~~~~~~~~~~~~~

-------------------------------------------------------------

Using Retrieves to Retrieve Past Information 	Back to Index 	definition_::

In addition to being triggered by information that a module is subscribed for, a module can also ask to be given old information. Your module can retrieve dynamically (at runtime). The runtime method requires the module itself to decide when to do the retrieval. Alternatively, your module definition in the psySpec can include a retrieve spec.

The XML for the retreiving looks like this:

        <trigger from="WB1" type="Input.Sens.MultiM.Vision.Human.Found.True"/>
      <retrieve from="WBX" type="Input.Sens.UniM.Hear.Human.Voice"/>
      <retrieve from="WBX" type="Input.Sens.MultiM.Vision.Human">
        <latest>3</latest>
        <lastmsec>8000</lastmsec>
      </retrieve>                  

The first retrieve asks to be sent a message of type Input.Sens.UniM.Hear.Human.Voice along with the message type Input.Sens.MultiM.Vision.Human.Found.True to which it has subscribed; the second retrieve asks to be sent a message of type Input.Sens.MultiM.Vision.Human along with that, but with the conditions that if there are more than 3 such messages in the last 8 seconds it will only return the 3 most recent of those. All conditions that are listed like that are ANDed together.

You can also group them using a retrieves tag:

    <retrieves>
      <retrieve from="WB3" type="xxx"/>
      <retrieve from="WB2" type="yyy"/>
    </retrieves>

You can retrieve based on several features:

    message ID
        Example: <retrieve id="[GUID]"/> where GUID is the unique id of a message
    message sender
        Ex: <retrieve sender="HumanTracker1"/>
    message type
        Ex: <retrieve from="WBX" type="Output.Plan"/>
    content
        Ex: <retrieve content="*green tomato*"/> will return any message where the phrase "green tomato" appears anywhere in the content of the message. The stars mean that anything can come before and after the two words.
    contentmatch
        Ex: <retrieve contentmatch="xml" content="*green tomato*"/> will work like the last example, but in addition the contentmatch="xml" will force matching to also be done on the XML itself, i.e. tag names, parameter names, etc. Notice: You cannot use the reserved XML symbols like < > / & " etc.
    Time: aftertime, untiltime, latest, lastmsec

The last constraints, aftertime, untiltime, latest and lastmsec, are done with tags, as the example above has shown for latest and lastmsec. The others are used precisely in the same way.
aftertime means anything that has happened after the particular timestamp used;
untiltime will mean anything that happened before that timestamp.

The <retrieve> spec also allows these three additional constraints which specify retrieval relative to the posting of contexts:

    <retrieve>
       <before>SoB.Alive.SoS.Awake</before>
       <during>SoB.Alive.SoS.Awake</during>
       <after>SoB.Alive.SoS.Awake</after>
    <retrieve>          

before means any time before this context last became active.
during means any time between this context last became active and subsequently
became inactive.
after means any time after this context last became inactive.

Specifying Retrieves in the psySpec

When using retrieves in the psySpec the above example would look like this:

    <module name="HumanTracker1">
      <trigger from="WB1" type="Input.Sens.MultiM.Vision.Human.Found.True"/>
      <retrieve from="WBX" type="Input.Sens.UniM.Hear.Human.Voice"/>
      <retrieve from="WBX" type="Input.Sens.MultiM.Vision.Human">
        <latest>3</latest>
        <lastmsec>8000</lastmsec>
      </retrieve>
      . . .
    </module>                  

Using Retrieves in Code

A module can at any time decide to retrieve old information. To do this from inside the module you can use any one of these:

    ObjectCollection retrieveMessages(String xml)
    ObjectCollection retrieveMessages(RetrieveSpec spec)
    ObjectCollection retrieveMessages(ObjectCollection specs)

where the xml variable contains some retrieve XML specification as shown above.

The two latter methods are alternatives to raw XML. The spec variable is XML code contained in a RetrieveSpec object; the specs variable is a collection of RetrieveSpec objects contained in an ObjectCollection object.

If your module definition in the psySpec includes a retrieve spec, any messages retrieved according to that spec will be included in every wakeup message each time your module gets triggered.

When your module receives the wakeup message, to get at the retrieved messages in your code, use:

    // Get the current number of retrieved messages
    int getRetrievedMessageCount();
    // Get a specific retrieved message
    Message getRetrievedMessage(int pos);
    // Get the full collection of the retrieved messages
    ObjectCollection getAllRetrievedMessages();

## Whiteboards {#whiteboardsdt}

-------------------------------------------------
This is material from the former entry on whiteboards in the list of concept and terms. You may want to include it here. otherwise delete this section

Whiteboards hold messages in memory at run-time for a period of time for any module to retrieve when they need to see the messages. The messages can be retrieved by time and type, but custom indexes can also be used to retrieve messages based on values of user-defined keys.  
Whiteboards are specified in the PsySpec like this: 
  
~~~~~~~~~~~~~~~{.xml}
<whiteboard name="WB1" maxcount="10000"/> 
~~~~~~~~~~~~~~~
and any module can post to them by adding to=”WB1” in their post specification.  
The parameter maxcount limits the number of messages that the whiteboard can store at any one time. The value that this parameter takes is an integer (default value is 10000). When the limit (maxcount=) is reached the whiteboard will start permanently erasing the oldest messages. All messages on a particular whiteboard are guaranteed to be permanently available on that whiteboard until its upper limit is reached.  
 
Whiteboards can also subscribe to messages themselves 
  
  
~~~~~~~~~~~~~~~{.xml}
<whiteboard name="WB2"> 
<trigger name="Ball" type="ball.1"/> 
</whiteboard>  
~~~~~~~~~~~~~~~

which means that modules don’t have to specify the to field and any message with the matching type will be routed to the Whiteboard automatically. 
  
Any message entering a Whiteboard must have a TTL (time to live) higher than 0 and will be removed from the Whiteboard automatically when the TTL expires. If the message has a TTL of 0 it will not be indexed by the Whiteboard. TTL is set in milliseconds on the message post in the PsySpec (and can be overwritten by setting it when creating the output DataMessage, but this value is in microseconds). 
  
The default Whiteboard indexes its messages by time and a module can retrieve messages from it by specifying the retrieve in the PsySpec 
  
~~~~~~~~~~~~~~~{.xml}
<retrieve name="r1" source="WB1" maxcount="10" /> 
~~~~~~~~~~~~~~~

Modules can use the index for retrieving specific messages, for example all messages from time t1 to t2. 
It can use the retrieve API to carry out a named retrieve and add additional parameters 
 
And to execute you call: 
~~~~~~~~~~~~~~~ c++
 std::list<DataMessage*> retrievedMsgs; 
 std::list<DataMessage*>::iterator i, e; 

    int8 status = api->retrieve(retrievedMsgs, "r1"); 
    if (status == QUERY_SUCCESS) 
     api->logPrint(1, "Successfully retrieved %u messages...", retrievedMsgs.size()); 
    else if (status == QUERY_TIMEOUT) 
     api->logPrint(1, "Retrieve 'r1' timed out..."); 
    else 
     api->logPrint(1, "Retrieve 'r1' failed (%u)...", status); 
 
    for (i = retrievedMsgs.begin(), e = retrievedMsgs.end(); i != e; i++) { 
     // do something with this message 
     delete(*i); 
    } 
~~~~~~~~~~~~~~~


**Example**

If you, however, want to retrieve information based on for example people counting the data, time is irrelevant. For that whiteboards can also index their messages by data other than time:   

-------------------------------------------------
To specify a whiteboard in the psySpec (or dynamically), this is the basic syntax:

<whiteboard name="WB1">
<description>This is a basic Whiteboard</description>
</whiteboard>

This creates a whiteboard called WB1. (All whiteboards (and modules) in Psyclone must have unique names.)

To limit the number of messages stored, use the max= parameter:

<whiteboard name="WB1" maxcount="10000">

This will limit the number of messages to 10000; when message 10001 is recieved the oldest message will be permanently destroyed by the whiteboard.

Media streams can bespecified for whiteboards in the psySpec like this:

<whiteboard name="MyWB" maxcount="15000">
<description>My Whiteboard</description>
<streams>
<stream name="Input.Stream.Video.Raw" maxsize="10" />
<stream name="Input.Stream.Video.Proc1" maxsize="10" />
<stream name="Input.Stream.Video.Proc2" maxsize="10" />
<stream name="Input.Stream.Video.Proc3" maxsize="10" />
</streams>
</whiteboard>

The MyWB whiteboard will now contain four circular media streams, each which has a max buffer size of 10 megabytes. After this limit is reached the oldest data samples are deleted. (Notice that the names of the streams have no semantic meaning to the system.)

Whiteboards can be created and destroyed dynamically by sending a message to 'Psyclone' with type

Psyclone.System.Create.Whiteboard

Include the spec for the whiteboard in the content of the message. To destroy a whiteboard use:

Psyclone.System.Destroy.Whiteboard

Streams can be added to a whiteboard by sending the message

Psyclone.System.Create.Stream

to 'Psyclone' with the whiteboard spec:

<whiteboard name="MyWB">
<stream name="Input.Stream.Video.Proc3" maxsize="10" />
</whiteboard>

where MyWB is a whiteboard already in existence. The maxsize= parameter sets the size of the circular stream buffer to 10 megabytes.

Streams can also be created outside whiteboards by simply putting the following in the content:

<stream name="Input.Stream.Video.Raw" maxsize="10" />

The following will create a standalone stream in a satellite called Sat1:

<stream name="Input.Stream.Video.Raw" maxsize="10" satellite="Sat1" /> 



Using Timing With High-Volume Posting
When Modules post messages faster than the receiving Whiteboards can process them, the posting may be slowed down by the Whiteboard itself. The simplest way for a Module to detect that a Whiteboard is getting too busy is to put a simple timer around the posting itself, to measure if the posting at some point starts taking longer than usual.

In C++ you can do this very easily using the JTime object:

JTime start;
// post message to Whiteboard
int dif = start.getMicroAge(); // returns microsecond time

In Java you can do this with millisecond accuracy using

long start = System.currentTimeMillis();
// post message to Whiteboard
int dif = start - System.currentTimeMillis(); // returns microsecond time

You can then notice if the posting starts taking much longer than usual and take the appropriate action.




