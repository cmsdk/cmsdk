#Catalogs <a name="catalogsdetails"></a>

## File Catalog <a name="filecatalog"></a>

The File Catalog allows any module to read and write files from/to a central location without having to worry about which computer the module is running on.  

 

A File Catalog is defined in the PsySpec like this: 

 
``` xml
<catalog name="MyFiles" type="FileCatalog" root="./"> 

<parameter name="ReadOnly" type="String" value="no" /> 

</catalog> 
```
 

The optional parameter ReadOnly defaults to 'no', but can be set to 'yes' which will prevent write operations. 

 

Any crank code can access the catalog by defining a named query in the PsySpec like this: 

 
``` xml
<module name="Test"> 

… 

<query name="MyFiles" source="MyFiles" subdir="test" ext="txt" binary="yes" /> 

… 

</module> 
```
 

And the crank code can execute the query this way: 

 
``` c++
uint32 datasize = 0; 

char* result = NULL; 

uint8 status = api->queryCatalog(&result, datasize, "MyFiles", "test", "read"); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully retrieved %u bytes from file catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'read' timed out for file catalog..."); 

else 

api->logPrint(1, "Retrieve 'read' failed (%u) for file catalog...", status); 

… use binary data in result … 

delete[] result; 
```
 

which will read a file from the root dir ./, sub dir test called test.txt, i.e. ./test/test.txt. 

 

Likewise a file can be written by 

 
``` c++
data = utils::StringFormat(datasize, "Hello World"); 

status = api->queryCatalog(&result, datasize, "MyFiles", "test", "write", data, datasize); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully wrote %u bytes to file catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'write' timed out for file catalog..."); 

else 

api->logPrint(1, "Retrieve 'write' failed (%u) for file catalog...", status); 

delete[] data;
``` 
## data catalog  <a name="datacatalog"></a>
The Data Catalog allows any module to read and write data from/to a central datastore without having to worry about which computer the module is running on.  

 

A Data Catalog is defined in the PsySpec like this: 

 
``` xml
<catalog name="MyData" type="DataCatalog" interval="2000" root="./mydata.dat" /> 
``` xml
 

which puts the central datastore in the file ./mydata.dat and writes from memory to file every 2 seconds. 

 

Any crank code can access the catalog by defining a named query in the PsySpec like this: 

 
``` xml
<module name="Test"> 

… 

<query name="MyData" source="MyData" /> 

… 

</module> 
```
 

And the crank code can execute the query this way: 

 
``` c++
uint32 datasize = 0; 

char* result = NULL; 

status = api->queryCatalog(&result, datasize, "MyData", "test", "read"); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully read %u bytes from data catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'read' timed out from data catalog..."); 

else 

api->logPrint(1, "Retrieve 'read' failed (%u) from data catalog, ok if starting with a fresh catalog ...", status); 

delete[] result; 
``` 
 

which will read an entry from the datastore. 

 

Likewise data can be written by 

 
``` c++
data = utils::StringFormat(datasize, "Hello2 World"); 

status = api->queryCatalog(&result, datasize, "MyData", "test", "write", data, datasize); 

if (status == QUERY_SUCCESS) 

api->logPrint(1, "Successfully wrote %u bytes to data catalog...", datasize); 

else if (status == QUERY_TIMEOUT) 

api->logPrint(1, "Retrieve 'write' timed out to data catalog..."); 

else 

api->logPrint(1, "Retrieve 'write' failed to data catalog (%u)...", status); 

delete[] data; 

delete[] result;
``` 

## replay catalog <a name="replaycatalog"></a>
The Replay Catalog can be used in one Psyclone system to simply record certain message activity to disk - and later in a different Psyclone system it can be used to playback these messages as they happened in the first system. 

 

One example could be to record all perception from a robot over a 2 minute period, such as video, audio, sensors, etc. and potentially even the output of some of the processing of these. On a different computer these recorded messages can be used in a simulated system to get exactly the same messages without actually having to have the robot present and running. 

 

The recording phase can be done by simply defining the catalog in the PsySpec and telling it what and how much to record: 

 
``` xml
<catalog name="Replay1" type="ReplayCatalog" root="./replay1" maxsize="10240000" maxcount="2000"> 

<trigger name="Video" type="robot.sensor.video" /> 

<trigger name="Bumper" type="robot.sensor.bumper" /> 

</catalog> 
```
 

This will receive all messages of types 'robot.sensor.video' and 'robot.sensor.bumper' and store them to disk in ./replay1, only keeping the last 2000 messages and only keeping up to 10kb of data, whichever limit is hit first. 

 

Then the files can be copied to another computer and to replay the messages you simply define the same catalog with different parameters: 

 
``` xml
<catalog name="Replay1" type="ReplayCatalog" root="./replay1"> 

<post name="Video" type="robot.video" /> 

<post name="Bumper" type="robot.bumper" /> 

</catalog> 
``` 
 

This will play back the messages at the same interval they were recorded, but this time with different message types, just the trigger/post names have to match. 

 

Two additional parameters can be used on playback 

 
``` xml
<catalog name="Replay1" type="ReplayCatalog" root="./replay1" interval="1000" rotate="yes"> 

<post name="Video" type="robot.video" /> 

<post name="Bumper" type="robot.bumper" /> 

</catalog> 
```
interval="1000"  -  overwrites the posting interval to 1 second, regardless of the recorded message timing 
rotate="yes"  -  when the last message has been replayed go back and start playing from the beginning 
## MessageDataCatalog catalog <a name="MessageDataCatalog"></a>
The MessageDataCatalog Catalog can be used to collect messages from the whole system, keep the most recent versions of them and allow anyone to request parts of or the whole message from a web browser by easy to remember names. This means no custom programming or data in the modules at all. 

 

An example of a MessageDataCatalog Catalog entry in the PsySpec could be: 

 
``` xml
<catalog name="MessageDataCatalog" type="MessageDataCatalog"> 

<trigger name="VideoFrame" type="video.output.frame" /> 

<trigger name="SampleXML" type="video.output.frame" /> 

<trigger name="SampleText" type="video.output.frame" /> 

<trigger name="SampleHTML" type="video.output.frame" />--> 

<setup> 

<store name="VideoFrame" trigger="VideoFrame" key="VideoDataFrame" keytype="data"  

 datatype="raw" mimetype="image/bmp" maxkeep="1" maxfrequency="1" /> 

<store name="SampleJSON" trigger="VideoFrame" mimetype="json" maxkeep="1" maxfrequency="1" /> 

<store name="SampleXML" trigger="VideoFrame" mimetype="xml" maxkeep="1" maxfrequency="1" /> 

<store name="SampleText" trigger="VideoFrame" mimetype="text" key="content" maxkeep="1" maxfrequency="1" /> 

<store name="SampleNum" trigger="VideoFrame" mimetype="text" key="VideoWidth" maxkeep="1" maxfrequency="1" /> 

<store name="SampleSize" trigger="VideoFrame" mimetype="text" key="size" maxkeep="1" maxfrequency="1" /> 

<store name="SampleType" trigger="VideoFrame" mimetype="text" key="type" maxkeep="1" maxfrequency="1" /> 

<store name="SampleTime" trigger="VideoFrame" mimetype="text" key="time" maxkeep="1" maxfrequency="1" /> 

<store name="SampleTimeText" trigger="VideoFrame" mimetype="text" key="timetext" maxkeep="1" maxfrequency="1" /> 

<!--<store name="SampleHTML" mimetype="html" key="html" maxkeep="1" maxfrequency="1" />--> 

</setup> 

</catalog> 
``` 
 

Here we see the catalog subscribing to messages like any other module, each with a trigger name as usual. 

 

The custom setup XML part then defines named entries ('VideoFrame', 'SampleJSON', etc.) which can be queried by anyone like this: 

 
```
   GET /api/query?from=MessageDataCatalog&query=VideoFrame 
```
 

The return content-type is specifically specified in the setup XML so the one calling the URL doesn't have to know. 

 

Each store entry can have the following parameters: 
 name | The name used in the request | Required
 --|--|--
 trigger | Which trigger message to get the information from | Required 
 key | The name of the user entry inside the message - can be header fields such as 'time', 'type' too. 'time' will return the number of microseconds since year 0 - 'timetext' will return a nicer textual description of the date and time | If not specified the whole message is returned as either JSON, XML, HTML or TEXT, depending on the mimetype
 keytype | The type of key | Not currently used
 datatype | The type of binary data, mainly used for bitmaps | If set to 'raw' the code knows to expect raw pixels and need the message to contain width and height information too - so construct a valid bitmap format to return. In the future we may support other types of data types.
 mimetype | The mime type used to help the browser display the data correctly |Required, mime types are standard browser mime types such as 'application/json', 'text/html', 'text/xml', etc. Can also be short versions, 'json', 'xml', 'text', etc.
 maxkeep |How many messages (trigger history) to keep|Default is 1, later may support returning vectors of keys from several historical messages
maxfrequency |The maximum number of messages to keep per second | If set to 1 it will only keep 1 message every second and ignore new messages for that trigger until another 1 second has passed


 