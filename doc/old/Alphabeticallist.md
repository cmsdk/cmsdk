Alphabetical listing of concepts and terms {#alphabeticallist}
===========================================

#### Blackboard  <a name="blackboard"></a>
A blackboard is a special kind of information and data store, meant to keep global state of an evolving system, often at multiple levels of representation detail. The blackboard also is a way to keep global state for a system. Although used primarily in A.I. for systems trying to understand its inputs in order to respond intelligently, blackboards are can be extremely useful for building systems with complex, evolving states, as well as for systems with distributed processing. Psyclone's whiteboards are a special implementation of a scheduling blackboard.  
**RELATED: [whiteboards](#whiteboards)** 

#### Boostrapping the system  <a name="bootstrappingthesystem"></a>
In an event-driven system there is always the question of which event is the initial one. When Psyclone starts up it creates the Whiteboards first and then it posts a message called Psyclone.System.Ready. You can start your system by using this message type as a trigger for one of your modules. You can also attache timing to the startup of your modules using the __after=__ parameter in the trigger specification: 
~~~~~~~~~~~~~~~{.xml}
   <trigger after="500" type="Psyclone.System.Ready"/> 
 ~~~~~~~~~~~~~~~
This will trigger the module 500 milliseconds after the message Psyclone.System.Ready has been posted. 
__RELATED: [whiteboards](#whiteboards) [triggers](#trigger)__  

#### Catalogs <a name="catalogs"></a>
Catalogs are components that can provide access to large amounts of data and that have a query mechanism built in. Catalogs are a way to abstract access to information. Catalogs follow a simple query-response pattern that is separate from the whiteboard messaging system.  
Any module in the Psyclone system can ask a catalog a question and receive an answer back, without actually having to know the format of the data stored. An example could be a large SQL database with proprietary information, where modules need to extract names and addresses, but cannot know the format of the database. A Catalog could be constructed with a bit of code, which can receive questions from modules, then search the database and provide an answer back. Another hypothetical example is a Google Catalog, where modules can ask a question and get search result URLs back, without having to know how to communicate with Google. A third example is for learning, where some modules add data to a knowledge base and others ask questions about the data.  

For a detailed tutorial how to use catalogs look [here](@ref catalogsdt).

#### Console / command line <a name="consolecommandline"></a>
Psyclone is started from the command line in a console window.  
**Example (Linux):** 
   >./psyclone port=20000 
   
Command line parameters that can be passed into Psyclone: 
PARAMETER |	VALUE |	EXAMPLE | COMMENTS 
------------ | ------------- |-------------|----------
port= |	[portnumber] 	|port=10000  |	This is the only required parameter. Must be a positive integer above 1024 
spec= |	"[path][valid psySpec]" |	spec="../myPsySpec.xml" |The path is optional 
verbose= |	 1 ... 4 |	verbose=3 |	How much run-time info Psyclone prints to the console 
html= |	"[path]" |	html="html" |	Where Psyclone's built-in Web server can find the HTML files. Default is 'html'.  
psycloneport=  |	[portnumber] |	psycloneport=10001 | 	This will override any specification of Psyclone port in the psySpec and the default, which is 10000 

#### Constructionist AI 
Constructionist AI is a general methodology in that facilitates the development of large artificial intelligence systems. An example of a methodology following the philosophy of Constructionist AI is the Constructionist Design Methodology described on the __MINDMAKERS__ site. The main premise of Constructionist AI is to develop tools, principles and mechanisms for helping AI researchers construct enormously large systems with more speed and efficiency. 


#### Contexts <a name="contexts"></a>
Contexts are global states in Psyclone. They can be used to manage the runtime behavior of modules. Contexts are defined as a tree, e.g. *Root.Branch1.Branch2* or *A.B.C.D*; in a typical system specific modules are used to post contexts and manage transitions between them. The syntax of contexts is period-delimited — they look exactly like message types. 
**Example:** If we create a system that records the color of people's garments as they walk by a camera pointed out the window we might have a people-detecting module that detects the presence of people and several color-estimation modules that estimates the color of clothing (each module is a specialist in a particular color). We now create a context called **People.Present**. The people detector will post this context whenever it detects people and the color modules will then be in-context and thus will be able to process the video. When people are not present the people detector will post a different context, **People.Absent**. As soon as **People.Absent** context is posted, Psyclone will list the color modules as **out of context**, they would receive no triggers from that point on, and hence, would not run until **People.Present** was posted again. 
A context is specified in the psySpec using the <context> tag in the <module> spec. All modules have a context in which to be active (having no context specified is like having the global, top-level context). A module can have more than one context specified, however, at runtime a module picks one and only one of its contexts (the last one posted) in which it is active.  
The dot notation in the above examples is always used to build the context tree. To post a context use the Psyclone.Context message type and append :<myContextName> after it, where <myContextName> is e.g. A.B.C.D or Root.Branch1.Branc2. Notice the use of colon for separation, resulting in an actual posted message type of Psyclone.Context:Root.Branch1.Branch2. (Expert point: This posted message type is an actual message posting event, so context postings can also be used as triggers — i.e. any module can be triggered off a context posting.) 
There is no use of wildcards in context notation.  

For a detailed tutorial how to use contexts look [here](@ref contextsdt).

#### CoreLibrary <a name="corelibrary"></a>
Psyclone builds on the CMLabs CoreLibrary for many of its basic functions. CoreLibrary is a multiplatform object library for C++ (Linux, Win32, MacOSX, PocketPC). It provides many of the common objects found in Java (Strings, Threads, Collections, etc.) and has the ability to send objects across the network.  

#### Cranks <a name="cranks"></a>
A crank is a Psyclone term for a method which can be referenced from the psySpec as part of a module's functioning. When a module gets a wakeup its crank gets run. Typically the first thing that the crank should do is check the trigger message or the retrieved message whether it makes sense for it to run the main part of the crank, which typically involves computing what the module is made for computing. 
**RELATED:  [psySpec](#psySpec), [wakeup](#wakeup)** 

#### External modules <a name="externalmodules"></a>
External modules are written to connect to Psyclone but to run in their own process space. External modules can run on the same machine as or on a separate machine from Psyclone. Additionally, external modules can be started manually or they can be started automatically with special configuration for the module in a psySpec. 
Like all modules, external modules register with whiteboards for triggers (message and stream types), and post results of their processing to whiteboards, in the form of messages and/or streams. External modules use a PsyAPI object created with the CMSDK library to connect and talk to Psyclone. Even though the external modules live outside Psyclone, their triggers, cranks, parameters and contexts can still be defined in a psySpec; doing so has several benefits, among others to increase the speed at which the whole system can switch between system-wide states, so-called contexts. And when messages need to go to a module on another computer the Psyclone Node will send the message through the network to the other Psyclone Node (one node per computer).  Messages are but are  transferred as binary packages in a single go

#### FileCatalog <a name="filecatalog"></a>
The FileCatalog allows a module to read a write files from a name location specified in the PsySpec. It uses the generic Psyclone catalog functionality but is built directly into Psyclone and therefore available to be used by any module, internal or external. The FileCatalog has Catalogs can have any number of parameters and is typically created using the SDK and loaded from a dll. 
**RELATED: [catalogs](#catalogs) [storageCatalog](#storageCatalog)**

#### Internal Modules <a name="internalmodules"></a>
Like External Modules, Internal Modules can be configured in the psySpec with triggers, retrieves, posts and cranks. Internal modules are small programs created using the CMSDK, they live inside DLLs and are loaded into memory by Psyclone and live in the same process (i.e. memory) space as Psyclone. The path to the DLLs has to be given to Psyclone in the PsySpec. 

#### MediaConnection <a name="mediaconnection"></a> 		
A MediaConnection object provides access to a media stream. 
**RELATED: [mediastreams](#mediastreams)** 		
   
#### Media streams <a name="mediastreams"></a> 		
A media stream is a type of streaming data that a whiteboard can make available on a subscription basis. Streams is a more efficient mechanism for transporting data between modules in cases where the meta-information about the data doesn't change. It's interface (API) is very similar to the message API.  
**RELATED: [whiteboards](#whiteboards)** 		

#### Message type / stream type ontology <a name="messagetype"></a>
Using an ontology to define message and stream types can grately enhance the future flexibility of a system, making it easier to (a) change, (b) integrate with external, unknown systems, (c) expand and build upon. To make an ontology for message types, each level in a dot-delimited message type graph (TopLevel.Sub1.Sub2.Sub3.Etc.LeastSignifLevel) is carefully defined so that other users can use the levels to describe their own message types.  
The top level should always be a unqiue root. For example, if a company called XYZ creates its ontology, putting XYZ as the top-level makes all subsequent type identifiers unique to that company. Defining rules for how to use the sub-identifiers makes use of the message type ontology more consistent. For example, if XYZ builds speech recognizers, it might have several levels dedicated to the type of pre-processing that the audio goes through. Hypothetical message type in such an ontology could be XYZ.Audio.Raw and XYZ.Audio.Filtered.EchoCancellation.Stage1. 

#### Messages <a name="messages"></a>
Please enter here a brief description what messages are

For a detailed tutorial how to use messages look [here](@ref messagesdt) and [here](@ref mmpriodt).

#### Modules <a name="modules"></a>
Modules in Psyclone are a way to organize function. Think "executable program". A Psyclone module can be (1) a stand-alone executable that executes in a regular manner as any program would, running in a computer's operating system (to do so the executable uses a PsyAPI object); a Psyclone module can also be (2) an internal library that is loaded into Psyclone.  
Modules are essentially conceptual constructs which help organize the creation of large Psyclone systems. Modules register with the local Node for triggers (message and stream types), and post results of their processing to the Node, in the form of messages and/or streams and they can  post and retrieve messages from Whiteboards. Modules can also be activated by Signals to which they are subscribed. Internal modules are specified in the Psyspec; external modules use the CMSDK library to create PsyAPI objects to connect and talk to Psyclone.

#### Network <a name="network"></a>
Psyclone allows you to run your system as multiple executables that communicate via messages or streams. Here are the main topics on networking: 
•	Internal and external modules. 
•	Running modules on multiple computers. 
•	Automatic startup of exeternal modules on a remote computer. 
For standard message passing Psyclone relies on TCP, even though other delivery messages might be added in the future. Each component will autodetect whether the receiver of a message is located in the same physical executable, in which case it will be delivered directly through memory. If not, it automatically opens a TCP connection to the receiver if one is not already open, and sends the message. If a connection fails, it will be automatically reconnected if possible and only after two retries will the message parsing fail with notification to the sender. 
When a module connects to Psyclone through a firewall or some other unidirectional routing device and it suspects that Psyclone cannot connect directly back to it, the module can open a two way connection to the components inside Psyclone. This means it will hold a normal sending connection as well as one additional receiving or callback connection, acting as if the remote component had actually connected back to it in the first place. The remote component will automatically use this latter connection when sending messages if this is available. If this connection is broken, the module will automatically re-establish this if possible or keep retrying until successful. 
If Psyclone loses a connection with an external module and needs to send a message to it, it will retry twice for every message, before giving up. This will of course slow down Psyclone a bit as it can take up to 50 milliseconds to retry a connection, so in the future Psyclone might try for a set number of times and then unregister the module completely, waiting for it to reregister when it again is available. 

#### Persistent Record Storage Catalog <a name="persistentcatalog"></a>
The Persistent Record Storage Catalog (StorageCatalog for short) allows a module to save persistent data that can be reloaded again after a Psyclone shutdown. It is built into Psyclone and therefore available to be used by any module, internal or external. A generic catalog can have any number of parameters and is typically created using the SDK and loaded from a dll.  

**RELATED: [catalogs FileCatalog](#filecatalog)**


**RELATED: [psySpec](#psyspec) [tutorial cranks](# )**

#### port <a name="port"></a> 			
The Internet Protocol (IP) port number at which psyclone is running. This is provided on the command line when Psyclone is started. 



**RELATED: [running Psyclone](#runningPsyclone  )**
  
#### post <a name="post"></a>			
When a module sends out a message it is called a post.  
The temporal model adhered to in Psyclone is that stamping a message with the postedtime is the very last thing a module should do before posting it. The assumption is therefore that at this moment in time, when the postedtime was written, the module gave up control of the content of the message and the transmission is ballistic. Therefore, the semantics are that the content of any message is assumed to be accurate up until the postedtime — after that the message was not under the control of the module any more and may or may not reflect accurately what it refers to. 

**RELATED: [modules](#modules) [messages](#messages) [postedtime](#postedtime)**


#### Priorities of Messages & Modules <a name="priorities"></a>
Priorities of messages and modules can be set in the psySpec as well as at runtime.  


**RELATED: [message](#messages) and [module priorities](#priorities)**

#### psyAPI <a name="psyapi"></a>
The PsyAPI object is the C++ object that the crank function uses to communicate with the rest of the system, such as waiting for new triggers, posting output messages, working with parameters, etc.  
Every module has its own PsyAPI object and gives this to cranks as a parameter. 
**Example:** 
~~~~~~~~~~~~~~~{.cpp}
int8 Simple(PsyAPI* api) { 
DataMessage* inMsg; 
     const char* triggerName; 
     if (api->shouldContinue()) { 
          if ( inMsg = api->waitForNewMessage(0, triggerName)) 
              api->postOutputMessage(); 
     } 
     return 0; 
} 
 ~~~~~~~~~~~~~~~
#### psyProbe <a name="psyProbe"></a>
PsyProbe is the web-based interface for Psyclone.  



**RELATED: [using psyProbe](#psyprobe) [psyProbe glossary](#  )**

#### psySpec <a name="psySpec"></a>
The psySpec holds setup specifications for the system. Loaded at Psyclone startup. The paths that Psyclone will check automatically, relative to the directory that Psyclone was run from (if no path is specified), are: 
~~~~~~~~~~~~~~~
   ./ 
    ../ 
    ../../ 
    PsySpecs/ 
    ../PsySpecs/ 
    ../../PsySpecs/ 
    psySpecs/ 
    ../psySpecs/ 
    ../../psySpecs/ 
    Psyspecs/ 
    ../Psyspecs/ 
    ../../Psyspecs/ 
    psyspecs/ 
    ../psyspecs/ 
    ../../psyspecs/ 
~~~~~~~~~~~~~~~


**RELATED: [running Psyclone](#  )**

#### Registration <a name="registration"></a>
The contexts, triggers, retrieves and cranks specified for a module in the psySpec is called a registration. In addition to up-front specification of this via the psySpec any module can update its registration at any time during runtime.  

#### receive / receivedtime   <a name="receivedtime"></a>
When a module receives a message it will timestamp it with the time of reception - this is the receivedtime. The temporal model in Psyclone assumes that the receivedtime timestamping is the very first thing a module does when it receives a message, to reflect as well as possible the actual time of message arrival. 



**RELATED: [post postedtime](#postedtime)**

#### retrieve / retrieve spec  <a name="retrievespec"></a>
Please write here a brief descriptions of retrieves

For a detailed tutorial how to use retrieves look [here](@ref retrievesdt).


**RELATED: [whiteboards](#whiteboards), [psySpec](#psyspec), [using retrieves](#retrievespec)**



#### Spaces   <a name="spaces"></a>
Any Catalog, Whiteboard or Module can be placed in a process separately from the Node process 
~~~~~~~~~~~~~~~{.xml}  
<catalog space="MySpace" name="MyData" type="DataCatalog" interval="2000" ... /> 
~~~~~~~~~~~~~~~
The main purpose of Spaces is separation. If a module runs inside Psyclone and it crashes because of bad coding by the user, it takes Psyclone down with it. If you put that module into a Space (which is a separate process on the same computer) and the module crashes it only takes down the Space, which can be respawned without loosing any of the Node and the shared memory for the system. 
External modules do the same, so if the external program crashes just restart it and Psyclone will continue to run happily. 
 
If the Space doesn’t already exist it will be automatically started up as a separate system process and it is automatically destroyed when the Psyclone Node shuts down. A Node can have an unlimited number of Spaces attached and if one Space crashes because a component inside performs an illegal action only the components in this Space is affected and can be restarted (not currently done automatically).

For a detailed tutorial how to use spaces look [here](@ref externalspacesdt).

#### Scheduling  <a name="scheduling"></a>
Messages with the same priority level Psyclone will be transmitted in-order; whiteboards use a FIFO as their main scheduling mechanism. However, when using messages, a message at a higher priority level will be sent before a message at a lower priority level.  


**RELATED: [priorities of messages](#priorities) and [streams](#streams)**

#### SDK  <a name="sdk"></a>
The Psyclone SDK fully enables the creation of internal modules (aka crank libraries).  

#### spec=  <a name="spec"></a>
Command-line parameter for Psyclone, spec=<psySpec xml file>, where <psySpec xml file> is the name of the file containing the psySpec xml. The file can be located in the same directory or in a separate directory called psyspecs.  

**RELATED: [starting Psyclone](# ), [psySpec](#psyspec)**

#### Streams  <a name="streams"></a>

See Media Streams.  

#### StorageCatalog  <a name="storagecatalog"></a>

See Persistent Record Storage Catalog.  

#### Throughput / priority  <a name="throughputpriority"></a>

Whiteboards will put messages through as fast as they can. If they are overloaded and messages have priorities, highest-priority messages will be handled first. Streams have all equal priority and thus equal throughput. Streams are higher priority than messages.  

**RELATED: [Whiteboards](#whiteboards)**



#### Trigger  <a name="trigger"></a>

Modules in Psyclone use the concept of triggers to get alerted when they have input data to process. A module registers with Psyclone for triggers and it will get woken up when any of those triggers get posted.  


**Example:** 
 
~~~~~~~~~~~~~~~{.xml}
<trigger  name="exampleTrigger" type="exampleTriggertype" /> 
~~~~~~~~~~~~~~~
The differentiation between type and name exists so that the crank code never has to refer to types directly. 
That means that the user can change types in the spec alone to alter the information flow and the code will still work as it doesn't know about types for input or for output messages 
 
 
~~~~~~~~~~~~~~~{.xml}
<trigger name="Ball" type="ball.1" maxage="2000" after="1000"/> 
~~~~~~~~~~~~~~~



|          |                                                                                                                                                                           |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| name=    | A module-internal, alias specified by the crank of the module. Any crank connected to Triggers with that name will be activated                                           |
| type=    | This is the trigger type associated with the post. The type is the relevant feature that the module subscribes to                                                         |
| maxage=  | In addition to having the correct type, a post's age has to be below this number to wake up the module. This can be used to skip triggers if the system is lagging behind |
| after=   | Determines the time that the module waits after receiving the trigger to activate the crank                                                                               |
| verbose= | Command-line parameter for Psyclone, verbose=n. Controls the amount of basic output from Psyclone at the console                                                          |
Possible values of n:  
0 - System default, minimal output; severe errors only, no runtime printout after startup 
1 - verboseness 0 + normal errors and warnings 
2 - verboseness 1 + notification of each instance of the posting of a message from a module  
3 - verboseness 2 + notification of each triggering of a module by some message  
4 - verboseness 3 + notification of each instance of message retrieval by a module and module context switching 

**RELATED: [spec=console](#spec=console)**

#### Wakeup <a name="wakeup"></a>

A module gets a wakeup message from a Whiteboard when a message type it has registered for gets posted to that Whiteboard. The message, which is of type AIR.Instruct.Wakeup, contains, in its content slot, a specification for how the module should behave as a result of the wakeup. If the module has been specified in the psySpec, this will contain the message types to retrieve, name of the crank to run.  

**RELATED: [contexts](#contexts),[ psySpec](#psySpec)**




#### Whiteboards <a name="whiteboards"></a>

Please write here a brief description of what a whiteboard is.

For a detailed tutorial how to use Whiteboards look [here](@ref whiteboardsdt).


