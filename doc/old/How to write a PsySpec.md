How to write a PsySpec {#psySpec}
===========================================
## note this is just copied from the old manual and has to be updated and summarized
text taken from <a href="psycloneManual3.html">here</a> 

A simple psySpec contains initialization values for Psyclone at startup; an advanced psySpec contains the complete specification for a whole system architecture, and enables its automatic management. It can be used to specify one or more Whiteboards, the external and internal modules, as well as global parameters of the system. The dominosPsySpec.xml is a tutorial example psySpec for demonstrating the use of contexts in Psyclone.

    Components can be created and destroyed at runtime by sending a message of type Psyclone.System.Create.[component-type], where component-type is one of {Module, Whiteboard, Stream}.

The psySpec is read in by doing:

   psyclone port=[port] spec="[psySpecName]"

 Example:
  >./psyclone port=10000 spec="dominosPsySpec.xml"

If no psySpec is given, Psyclone will look for a file called psySpec.xml. If none is found, it will automatically create two Whiteboards by default, named WB1 and WB2. Port defaults to 10000 if not specified.

The psySpec contains the intialization values for Psyclone at startup. It can be used to specify one or more Whiteboards, as well as the modules of the system.

Here is a simple example of a psySpec, specifying some global variables, a Whiteboard called WB1 and one module called Startup:

   <psySpec name="psySpec Example" version="1.2">  

      <whiteboard name="WB1">
         <description>This is a basic Whiteboard</description>
      </whiteboard>

      <module name="Startup" type="internal">
            <description>Bootstrap by posting a root context upon system ready</description>
            <context name="System">
                      <phase name="Look for System Created">
                          <triggers from="any">
                                 <trigger type="System.Ready" after="100" />
                          </triggers>
                          <posts>
                               <post to="WB1" type="Psyclone.Context:SoB.Alive" />
                          </posts>
                     </phase>
            </context>
       </module>

   </psySpec>

(The tags context and phase may be omitted.) This psySpec creates one Whiteboard called WB1, and one internal module called Startup. When the Psyclone Factory posts some initial System messages to all Whiteboards, Startup goes into context.

Then, when the Factory posts message System.Ready, Startup gets triggered and posts the message of type Psyclone.Context:SoB.Alive. This posting is a signal to the context system in Psyclone to make the context SoB.Alive active. (If you are wondering where the crank is for this module: When you don't specify a crank Psyclone will use its default crank that simply posts upon triggering.)

To specify a module that runs external to Psyclone:

    <module ... type="external">

(If type="external" is omitted and an <executable> is defined, it is assumed to be an external module.)

The <command> tag inside <executable> can hold a command to be executed automatically by Psyclone:

    <executable name="myexe">
       <command>command-line command to be executed</command>
    </executable>

For example:

    <executable name="myexe">
       <command osmem="256">./big_java</command>
       <command osmem="64">./small_java</command>
    </executable>

Further details on the use of the psySpec are listed in various sections; see the following sections.

List of psySpec tags

<version> Psyclone tracks the versions of psySpec syntax and provides startup information on backward and forward compatibility of a psySpec.

<global> This is a general section that contains global parameters for psyclone.

<psyprobe> PsyProbe is the web-based interface to Psyclone. It defaults to port 10000, so technically this example is redundant. Here you can change it.

<whiteboard> Here we have specified a whiteboard with a simple description. Whiteboards must have unique names.

<module> We have specified one module called Startup. Modules must have unique names.

<context> This module will be "in context", as it's called — i.e. listening for its triggers — during one context, namely, the System context. Since contexts are posted automatically to all Whiteboards (the message type Psyclone.Context is global), there is no need to specify which Whiteboard the context lives on.

<phase> This module only has one phase, named Look for system created. Having one phase means that the module only will always have the same triggers during the System context.

<trigger> Here we have given the module one trigger, namely, System.Ready. When the message type System.Ready gets posted, the module will be woken up with an AIR.Instruct.Wakeup message. This message will contain the message which caused the triggering, as well as the phaseSpec (the XML within the <phase> tags) for the module. Here, the parameter after states that the wakeup message should be sent 100 milliseconds after the trigger message System.Ready gets posted.

<post> This module is designed to post a context. To do this it uses the message type Psyclone.Context. The module, when woken up, will automatically post a context message with the value SoB.Alive — the actual context that gets posted. Any module that has listed SoB.Alive as their context will then be in context, ready to be woken up by any of its trigger messages, should they get posted.