# Using the CMSDK library {#using_cmsdk}

When the CMSDK library has been linked and the header files location have been set up any program can start including header files and using CMSDK objects and functions. They all live in the namespace called cmsdk which can either be used directly such as

   cmlabs::PsyAPI* api = new cmlabs::PsyAPI();

or the namespace can be specified earlier:

   using namespace cmlabs;
   PsyAPI* api = new PsyAPI();

A lot of the utility functions are in a separate namespace called cmlabs::utils, such as

   cmlabs::utils::StringFormat("(%.3f,%.3f)", x, y);

or

   using namespace cmlabs;
   utils::StringFormat("(%.3f,%.3f)", x, y);


