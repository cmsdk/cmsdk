#!/bin/sh

if [ "$1" = "" ]; then
    echo Find not set
    exit
fi
if [ "$2" = "" ]; then
    echo Replace not set
    exit
fi
if [ "$3" = "" ]; then
    echo Input file not set
    exit
fi
if [ "$4" = "" ]; then
    echo Output file not set
    exit
fi

sed -b -e "s#$1#$2#g" $3 > tmp$$ && mv -f tmp$$ $4
