NetworkManager:
v Implement basic HTTP Server
v Test with browser
v Implement basic HTTP Client
v Test against HTTP Server
v Implement Message send/recv/detect
v Test with simple Message passing


MemoryManager:
k ./accesschk.exe -osv | grep Shmem__
v Shared Memory Manager
v PsyTime & TMC
v Component Map (cid <-> name <-> component data <-> node)
v Types & Topics Maps (ids <-> name)
v Node Map (cid <-> node)
v ThreadManager
v Memory Manager test including EOL
v Component Data Storage
  v Static data
  v Stats
  v Parameters
  v Private data
v Pool Structures
v EOL Structures
v EOL Clean-up incl. thread
v Global text size define
v Memory Map Template
v System Queues
  v MsgQ:  Trigger Messages bound for Modules (queue)
    v wait for with semaphore still missing
  v PostQ: Posted Messages from Modules (queue)
  v SysQ:  System commands queue
  v ReqQ:  Requests btw components with reply handling
           Need Comp -> Proc for MsgQ
v Linux Port
  v Time dif negative
  v Dynamic Memory lock + endless loop?
  v MemoryManager links page pointers

- Verbose/Debug engine
  - Current levels stored in system memory section
  - Filter on every log/debug
  - Log written to memory queue
  - Node spools to file
  - Node can receive requests for logs, read from file
  - Node rotates log file (policy?)

- Startup protocol
	- Config time server
	- Config node tries connect to all req nodes
	- Try connect to non-req nodes
	- Send node map to each
	   - Await node con list back from each
	- Retry failed req con
	   - check if other node repoted connection
	- active wait for mising req nodes
	- if another node report connection
	- Send out network ready

	- Non-Config node awaits conection
	- then awaits node map
	- try connect to each
	   - return node con list when done
	- active wait for mising req nodes

	- If time_server disappears
	  - connect to next lowest ID
	  - keep trying the lowest ID


  - Master
    - create ThreadMaster
    v creates MemoryManager
    v creates MasterMutex and saves it in shared mem
    v initialises shared memory
  - All
    - registers process by name
    - registers all threads
    - registers heartbeat
    - unregisters when shutting down
  - Master
    - monitors heartbeats
    - unregisters dead processes

Control and Data Messages

Thread Pool

PsyAPI object

Optimisations:
- Move select func bodies into headers
