The CMSDK core library is a C++ base library which contains much of the low-level
functionality required by Psyclone. It is released under a BSD open source licence
and is free for anyone to use for any purpose, except for those limited by the
CADIA clause (see below).

It abstracts the platform and operating system and provides an identical set of
objects and utility functions for a large number of functional areas such as
threading, synchronisation, processes, networking, shared memory, DLLs, timers,
messaging, HTTP server, maths and stats classes, microsecond time, request
client/server/gateway, logging and a large library of utility functions for
working with text and files.

Serves as an SDK for the Psyclone platform:

   https://cmlabs.com/psyclone

For documentation and more information please visit:

   http://cmsdk.cmlabs.com/
   
To download the latest version please visit:

   https://cmlabs.com/download



EXTENDED BSD License

Redistribution and use in source and binary forms, with or without
modification, is permitted provided that the following conditions 
are met:

- Redistributions of source code must retain the above copyright 
  and collaboration notice, this list of conditions and the following 
  disclaimer.

- Redistributions in binary form must reproduce the above copyright 
  notice, this list of conditions and the following disclaimer in the 
  documentation and/or other materials provided with the distribution.

- Neither the name of its copyright holders nor the names of its 
  contributors may be used to endorse or promote products derived from 
  this software without specific prior written permission.

- CADIA Clause: The license granted in and to the software under this 
  agreement is a limited-use license. The software may not be used in 
  furtherance of: (i) intentionally causing bodily injury or severe 
  emotional harm to any person; (ii) invading the personal privacy or 
  violating the human rights of any person; or (iii) committing or 
  preparing for any act of war. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
