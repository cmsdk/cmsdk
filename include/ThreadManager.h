#if !defined(_THREADMANAGER_H_)
#define _THREADMANAGER_H_

#include "MemoryManager.h"

namespace cmlabs {

struct ThreadStats {
	uint32 id;				// ID of the thread
	uint64 created;			// Thread creation time
	uint32 status;			// Status of the thread
	uint32 osID;			// OS id of thread
	ThreadHandle hThread;	// Thread handle
	THREAD_FUNCTION func;	// Function to run
	void* args;				// Function argument
	uint64 time[10];		// Last 10 timestamps
	uint64 currentCPUTicks[10];	// Last 10 CPU usage readings
};

struct ThreadDataHeader {
	uint32 size;			// Total size of data in bytes
	uint32 count;			// Number of thread stats
	uint32 activeCount;		// Number of thread stats
	uint32 statColPolicy;	// How statistics are collected
	uint64 time[10];		// Last 10 timestamps
	uint64 currentCPUTicks[10];	// Last 10 CPU usage readings
	uint32 bitFieldSize;	// Number of bytes in the bitfield
	// Bitfield
	// Stats1, Stats2, ...
};


class Runnable {
public:
	Runnable() {threadID = 0; shouldContinue = true; isRunning = false;}
	virtual ~Runnable() {
		stop();
	}
	virtual bool stop(uint32 timeout = 200) {
		shouldContinue = false;
		int32 timeleft = (int32)timeout;
		while (isRunning) {
			utils::Sleep(5);
			if ( (timeleft -= 5) <= 0) {
				LogPrint(0,LOG_SYSTEM,0,"Thread id %u didn't terminate...\n", threadID);
				return false;
			}
		}
		return true;
	}

protected:
	uint32 threadID;
	bool shouldContinue;
	bool isRunning;
};

#define GETTHREADSTATS(data,id) (ThreadStats*)(data + sizeof(ThreadDataHeader) + ((ThreadDataHeader*)data)->bitFieldSize + id*sizeof(ThreadStats))

class ThreadManager : public Runnable {
public:
	static ThreadManager* Singleton;

	// Create the ThreadManager Singleton
	static bool CreateThreadManager();
	// Create a new thread and start it
	static bool CreateThread(THREAD_FUNCTION func, void* args, uint32& newID, uint32 reqID = 0);
	// Pause the thread wherever it is now
	static bool PauseThread(uint32 id);
	// Allow the thread to continue running
	static bool ContinueThread(uint32 id);
	// Terminate the thread and restart it from its base location
	static bool InterruptThread(uint32 id);
	// Terminate and destroy the thread
	static bool TerminateThread(uint32 id);
	// Has thread terminated / exited
	static bool IsThreadRunning(uint32 id);

	// Add thread stats for local thread - needed for pthreads
	static bool AddLocalThreadStats();
	// Add thread stats for local thread - needed for pthreads
	static bool GetLocalThreadID(uint32& id);

	// Get the statistics for local thread
	static ThreadStats GetLocalThreadStats();
	// Get the statistics for a thread
	static ThreadStats GetThreadStats(uint32 id);
	// Get the statistics for all threads
	static ThreadStats* GetAllThreadStats(uint32& count);

	// Shutdown and delete the ThreadManager
	static bool Shutdown();

	ThreadManager();
	~ThreadManager();
	bool init();
	bool shutdown();

	int32 threadMonitoring();

protected:
	// Create a new thread and start it
	bool createThread(THREAD_FUNCTION func, void* args, uint32& newID, uint32 reqID);
	// Pause the thread wherever it is now
	bool pauseThread(uint32 id);
	// Allow the thread to continue running
	bool continueThread(uint32 id);
	// Terminate the thread and restart it from its base location
	bool interruptThread(uint32 id);
	// Terminate and destroy the thread
	bool terminateThread(uint32 id);
	// Has thread terminated / exited
	bool isThreadRunning(uint32 id);

	// Add thread stats for local thread - needed for pthreads
	bool addLocalThreadStats();
	// Get the local thread ID
	bool getLocalThreadID(uint32& id);

	// Get the statistics for a thread
	ThreadStats getThreadStats(uint32 id);
	// Get the statistics for all threads
	ThreadStats* getAllThreadStats(uint32& count);

	// Resize the ThreadStats storage to be able to contain more threads
	bool resizeThreadStorage(uint32 newCount);

	utils::Mutex mutex;
	unsigned char* data;
};

bool TestThreadManager();

static THREAD_RET THREAD_FUNCTION_CALL ThreadMonitoring(void* arg);


} // namespace cmlabs

#endif //_THREADMANAGER_H_

