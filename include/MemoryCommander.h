#if !defined(_MEMORYCOMMANDER_H_)
#define _MEMORYCOMMANDER_H_

#include "MemoryManager.h"

namespace cmlabs {

class MemoryCommander {
public:
	MemoryCommander();
	~MemoryCommander();
	
protected:
	unsigned char* data;
};

bool TestMemoryCommander();

} // namespace cmlabs

#endif //_MEMORYCOMMANDER_H_

