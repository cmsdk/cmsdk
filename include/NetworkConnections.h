#if !defined(_NETWORKCONNECTIONS_H_)
#define _NETWORKCONNECTIONS_H_

#pragma once

// #define UDPCON_PRINT_DEBUG
// #define UDPCON_PRINTBINARY_DEBUG
// #define TCPCON_PRINT_DEBUG
// #define TCPCON_PRINTBINARY_DEBUG

#include "ThreadManager.h"
#include "HTML.h"
//#include "PsyTime.h"

#ifdef _USE_SSL_
	#include "openssl/bio.h"
	#include <openssl/ssl.h>
	#include <openssl/err.h>
#endif // _USE_SSL_

namespace	cmlabs{

#define NOENC			1
#define SSLENC			2
#define AESENC			3

#define TCPCON			1
#define UDPCON			2
#define SSLCON			3
#define AESCON			4

#define INITIALBUFFERSIZE	4096

#define NETWORKERROR_ACCEPT			1
#define NETWORKERROR_RECEIVE		2
#define NETWORKERROR_SEND_ERROR		3
#define NETWORKERROR_SEND_TIMEOUT	4
#define NETWORKERROR_MEMORYFULL		5
#define NETWORKERROR_GREETING_ERROR	6

class TCPListener;
class TCPConnection;
class UDPConnection;
class NetworkConnection;

class NetworkConnectionReceiver {
public:
	NetworkConnectionReceiver() {}
	virtual bool receiveNetworkConnection(NetworkConnection* con) = 0;
	virtual bool registerError(uint16 error, TCPListener* con) = 0;
};

class NetworkDataReceiver {
public:
	NetworkDataReceiver() {}
	virtual bool receiveData(char* data, uint32 size, NetworkConnection* con) = 0;
	virtual bool registerError(uint16 error, NetworkConnection* con) = 0;
};







class TCPListener : public Runnable {
public:
	friend THREAD_RET THREAD_FUNCTION_CALL TCPListenerRun(THREAD_ARG arg);

	TCPListener();
	virtual ~TCPListener();
	bool setSSLCertificate(const char* sslCertPath, const char* sslKeyPath);
	bool init(uint16 port, uint8 encryption, NetworkConnectionReceiver* receiver = NULL, NetworkDataReceiver* dataReceiver = NULL);
	bool disconnect(uint16 error = 0);
	bool isConnected();
	uint64 getLocalAddress();

	NetworkConnection* acceptConnection(uint32 timeout);

private:
	uint8 encryption;
	uint64 localAddress;
	uint64 lastActivity;
	SOCKET socket;
	NetworkConnectionReceiver* receiver;
	NetworkDataReceiver* dataReceiver;
	utils::Mutex mutex;

	std::string sslCertPath;
	std::string sslKeyPath;

	bool run();
};

class NetworkConnection : public Runnable {
public:
	NetworkConnection();
	~NetworkConnection();
	virtual bool disconnect(uint16 error = 0);
	virtual bool reconnect(uint32 timeoutMS) = 0;
	virtual bool didConnect(int timeout = 0);
	virtual bool didConnect(SOCKET s, int timeout);
	virtual bool isConnected(int timeout = 0);
	virtual bool isRemote();
	virtual uint64 getRemoteAddress();
	bool setConnectTimeout(uint32 timeoutMS);

	virtual bool send(const char* data, uint32 size, uint64 receiver = 0) = 0;
//	virtual int32 peekStream();
	virtual bool receive(char* data, uint32 size, uint32 timeout, bool peek = false);
	virtual bool receiveAvailable(char* data, uint32& size, uint32 maxSize, uint32 timeout, bool peek = false);
	virtual bool discard(uint32 size);
	virtual uint32 clearBuffer();

	virtual bool waitForDataToRead(uint32 timeout);
	virtual bool waitForDataToBeWritten(uint32 timeout);

	virtual uint32 getOutputSpeed();
	virtual uint32 getInputSpeed();

	virtual uint8 getConnectionType();

	bool setGreetingData(const char* data, uint32 size);
	char* greetingData;
	uint32 greetingSize;

protected:
	uint8 type;
	uint64 remoteAddress;
	uint64 localAddress;
	uint64 lastActivity;
	uint32 threadID;
	SOCKET socket;
	bool remote;
	uint32 bufferLen;
	uint32 bufferContentLen;
	uint32 bufferContentPos;
	NetworkDataReceiver* receiver;
	utils::Mutex mutex;
	utils::Mutex sendMutex;
	char* buffer;
	uint32 connectTimeoutMS;
	uint32 inputSpeed;
	uint32 outputSpeed;
	uint64 inputBytes;
	uint64 outputBytes;

	virtual int32 readIntoBuffer();
	virtual bool resizeBuffer(uint32 len);
	virtual bool run();
};


class UDPConnection : public NetworkConnection {
public:
	friend THREAD_RET THREAD_FUNCTION_CALL UDPConnectionRun(THREAD_ARG arg);

	UDPConnection();
	~UDPConnection();
	bool initForOutputOnly();
	bool connect(uint16 port, NetworkDataReceiver* receiver = NULL);
	bool send(const char* data, uint32 size, uint64 receiver = 0);
	bool reconnect(uint32 timeoutMS);
	bool setDefaultReceiver(uint64 receiver);
	uint64 getLocalAddress();

protected:
	uint64 defaultReceiver;
};


class TCPConnection : public NetworkConnection {
public:
	friend THREAD_RET THREAD_FUNCTION_CALL TCPConnectionRun(THREAD_ARG arg);

	TCPConnection();
	~TCPConnection();
	bool connect(SOCKET s, uint64 localAddr, NetworkDataReceiver* receiver = NULL);
	bool connect(uint64 addr, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool connect(const char* addr, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool connect(const uint32* addresses, uint16 addressCount, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool delayedConnect(uint64 addr, uint32 timeoutMS, NetworkDataReceiver* receiver);
	bool delayedConnect(const char* addr, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver);
	bool send(const char* data, uint32 size, uint64 receiver = 0);
	bool reconnect(uint32 timeoutMS);

protected:
	bool findRemoteAddress(uint64& addr);
};

THREAD_RET THREAD_FUNCTION_CALL SSLConnectionRun(THREAD_ARG arg);

class SSLConnection : public NetworkConnection {
public:
	friend THREAD_RET THREAD_FUNCTION_CALL SSLConnectionRun(THREAD_ARG arg);

	SSLConnection();
	~SSLConnection();
	bool init();
	bool init(const char *certFile, const char *keyFile);
	bool connect(SOCKET s, uint64 localAddr, NetworkDataReceiver* receiver = NULL);
	bool connect(uint64 addr, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool connect(const char* addr, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool connect(const uint32* addresses, uint16 addressCount, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver = NULL);
	bool delayedConnect(uint64 addr, uint32 timeoutMS, NetworkDataReceiver* receiver);
	bool delayedConnect(const char* addr, uint16 port, uint64& location, uint32 timeoutMS, NetworkDataReceiver* receiver);
	bool send(const char* data, uint32 size, uint64 receiver = 0);
	bool reconnect(uint32 timeoutMS);
	bool isConnected(int timeout = 0);
	bool didConnect(int timeout = 0);
	bool disconnect(uint16 error = 0);

	int32 peekStream();
	int32 readIntoBuffer();
	bool receive(char* data, uint32 size, uint32 timeout, bool peek = false);
	bool receiveAvailable(char* data, uint32& size, uint32 maxSize, uint32 timeout, bool peek = false);

	std::string certinfo;

protected:
	bool findRemoteAddress(uint64& addr);
	#ifdef _USE_SSL_
		SSL_CTX *ctx;
		SSL *ssl;
		BIO *certbio;
		BIO *outbio;
#endif //_USE_SSL_
};

THREAD_RET THREAD_FUNCTION_CALL TCPListenerRun(THREAD_ARG arg);
THREAD_RET THREAD_FUNCTION_CALL TCPConnectionRun(THREAD_ARG arg);
THREAD_RET THREAD_FUNCTION_CALL UDPConnectionRun(THREAD_ARG arg);

bool NetworkTest_TCPServer(uint16 port, uint32 count = 0);
bool NetworkTest_TCPClient(const char* address, uint16 port, uint32 count = 0);
bool NetworkTest_SendReceiveData(TCPConnection* con, char* data, uint32 dataLen, uint32 c, bool receiveFirst = false);

}

#endif // _NETWORKCONNECTIONS_H_
