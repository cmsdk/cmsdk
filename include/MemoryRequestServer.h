#if !defined(_MEMORYREQUESTSERVER_H_)
#define _MEMORYREQUESTSERVER_H_

#include "MemoryRequestConnection.h"

namespace cmlabs {

class MemoryRequestServer {
public:
	static bool UnitTest();

	MemoryRequestServer();
	~MemoryRequestServer();

	bool init(const char* memoryName, uint32 maxConnectionCount, uint32 maxQueueSize, uint32 maxRequestCount);
	bool shutdown();

	DataMessage* waitForRequest(uint32 ms);
	bool setRequestStatus(uint64 id, uint16 status);
	bool replyToRequest(uint64 id, DataMessage* msg);

private:
	RequestServerHeader* serverHeader;

	utils::Mutex* mutex;

	MemoryRequestConnection* requestCon;

	bool maintenance();
};

} // namespace cmlabs

#endif //_MEMORYREQUESTSERVER_H_

