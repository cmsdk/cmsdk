/*

  This is the file where we define all we need for different OSes,
  like the Darwin system, that does not have various normal OS functions.

*/

#if !defined(__standard_h)              // Sentry, use file only if it's not already included.
#define __standard_h

#pragma warning (disable : 4100)

#define __LIBRARY_VERSION_STRING__		"2.0.0 Alpha1"

// Will speed up STL in general and massively in debug
#ifndef _SECURE_SCL
	#define _SECURE_SCL 0
	#define _SCL_SECURE_NO_DEPRECATE
	#define _HAS_ITERATOR_DEBUGGING 0
#endif

#include <list>
#include <map>
#include <queue>
#include "bimap.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

// #pragma warning( disable : 4995 )

#if defined(WIN32_PLATFORM_PSPC)
	// Code for either Windows Mobile 6 Classic
	// or Windows Mobile 6 Professional.
	#define POCKETPC
	#define WINDOWS
	#define NATIVESYSTEMNAME "Win32PPC"
	#define COMPILERVERSION _MSC_VER
//	#if (WIN32_PLATFORM_PSPC = 400)
		// Code for Windows Mobile 2003.
//	#elif (WIN32_PLATFORM_PSPC = 310)
		// Code for Pocket PC 2002.
//	#endif
#elif defined(__CYGWIN__)
	#define NATIVESYSTEMNAME "Win32Cygwin"
	#define COMPILERVERSION GCC_VERSION
	#define   WINDOWS
	#if defined(__x86_64)
		#define   ARCH_64
	#elif defined(__i386)
		#define   ARCH_32
	#endif
#elif defined(_WIN64)
	#define   WINDOWS
	#define   ARCH_64
	#define WIN_VS
	#if _MSC_VER < 1300
		#define WIN_VS6
		#define NATIVESYSTEMNAME "Win32VS6"
	#else
		#define WIN_VSNET
		#define NATIVESYSTEMNAME "Win32VSNET"
	#endif // _MSC_VER < 1300
	#define COMPILERVERSION _MSC_VER
#elif defined(_WIN32)
	#define   WINDOWS
	#define   ARCH_32
	#define WIN_VS
	#if _MSC_VER < 1300
		#define WIN_VS6
		#define NATIVESYSTEMNAME "Win32VS6"
	#else
		#define WIN_VSNET
		#define NATIVESYSTEMNAME "Win32VSNET"
	#endif // _MSC_VER < 1300
	#define COMPILERVERSION _MSC_VER
#elif defined(__GNUC__)

	#if defined(__x86_64)
		#define   ARCH_64
	#elif defined(__i386)
		#define   ARCH_32
	#endif
	#if defined(__linux)
		#define   LINUX
		#define NATIVESYSTEMNAME "Unix"
	#elif defined(__APPLE__)
		#define   OSX
		#define   DARWIN
		#define NATIVESYSTEMNAME "Darwin"
	#endif
	#define COMPILERVERSION GCC_VERSION
#endif

//3.1415926535
#define clPI 3.14159265358979323846264 
#define clmax(a,b) (((a) > (b)) ? (a) : (b))
#define clmin(a,b) (((a) < (b)) ? (a) : (b))
#define clround(x) (int) (((x) - floor(x)) >= 0.5 ? (floor(x) + 1) : (floor(x)))
#define clsqr(x) ((x) * (x))
#define clsign(x) (((x) < 0) ? (-1) : (1))
#define clabs(a) ((a)<0?-(a):(a))

#if defined(WIN_VS)
	#if !defined(POCKETPC)
		#if defined(_DEBUG)
			//#define _CRTDBG_MAP_ALLOC
			//#include <stdlib.h>
			//#include <crtdbg.h>
			//#define MY_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__) 
			//#define new MY_NEW 
			//#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
		#endif
	#endif
#endif

#if defined(CYGWIN)
	#include <winsock2.h>
#endif

#if defined(__GNUC__)

	#ifdef __GNUC_PATCHLEVEL__
		#define GCC_VERSION (__GNUC__ * 10000 \
	                    + __GNUC_MINOR__ * 100 \
	                    + __GNUC_PATCHLEVEL__)
	#else
		#define GCC_VERSION (__GNUC__ * 10000 \
	                    + __GNUC_MINOR__ * 100)
	#endif
	//#if __GNUC__ == 4
	//	#if __GNUC_MINOR__ < 3
	//		#error "GNU C++ 4.3 or later is required to compile this program"
	//	#endif /* __GNUC_MINOR__ */
	//#endif /* __GNUC__ */
	#define _open open
	#define _close close
#endif

#ifdef WINDOWS
	#define longlong signed __int64
	// typedef signed __int64 longlong;
//	#include <windows.h>
//	#include <RpcDce.h>
	#ifndef snprintf
		#define snprintf sprintf_s
	#endif // snprintf
	#define stricmp _stricmp
	//#define snprintf sprintf_s
	#define ssscanf sscanf_s
	#define strnicmp _strnicmp
#else

	typedef long long longlong;
	#ifdef Darwin
	#else
	#endif // Darwin

	#include <unistd.h>
	#include <pthread.h>
	#include <semaphore.h>

	#ifndef __int64
		typedef long long __int64;
	#endif 

	#define HAVE_TIMEZONE_VAR 1
	#define ssscanf sscanf
	#define strnicmp strncasecmp
#endif

#if !defined(POCKETPC)
	#include <signal.h>
#endif

#include <stdlib.h>
#include <ctype.h>

#endif // __standard_h
