#if !defined(_CONSOLEOUTPUT_H_)
#define _CONSOLEOUTPUT_H_

#include <memory.h>
#include <stdio.h>
#include <stdarg.h>

namespace	cmlabs{

#define CONSOLE_SYSTEM		1
#define CONSOLE_NETWORK		2
#define CONSOLE_NODE		3
#define CONSOLE_PROCESS		4
#define CONSOLE_COMPONENT	5
#define CONSOLE_COUNT		5

static const char *VerboseSubjects[] = { "verbose-system", "verbose-network", "verbose-node", "verbose-process", "verbose-component" };
static const char *DebugSubjects[] = { "debug-system", "debug-network", "debug-node", "debug-process", "debug-component" };
static unsigned char VerboseLevel[5] = {1,1,1,1,1};
static unsigned char DebugLevel[5] =   {0,0,0,0,0};
static unsigned char ErrorLevel = 1;

bool SetDebugLevel(unsigned char level);
bool SetDebugLevel(unsigned char subject, unsigned char level);
bool SetVerboseLevel(unsigned char level);
bool SetVerboseLevel(unsigned char subject, unsigned char level);
bool SetErrorLevel(unsigned char level);

bool PrintFile(FILE* stream, const char *formatstring, ... );
bool PrintVerbose(unsigned char subject, unsigned char level, const char *formatstring, ... );
bool PrintError(unsigned char level, const char *formatstring, ... );
bool PrintDebug(unsigned char subject, unsigned char level, const char *formatstring, ... );

}

#endif //_CONSOLEOUTPUT_H_

