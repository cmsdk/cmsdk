#if !defined(_MESSAGEPLAYER_H_)
#define _MESSAGEPLAYER_H_

#include "DataMessage.h"

namespace cmlabs {

struct ReplayIndexEntry {
	uint32 cid;
	uint64 serial;
	uint64 msgTime;
	uint32 size;
	char msgTriggerName[128];
	DataMessage* msg;
#if defined	ARCH_32
	DataMessage* dummy;
#endif
	bool reset() {
		memset(this, 0, sizeof(ReplayIndexEntry));
		cid = DATAMESSAGEINFOID;
		return true;
	}
	bool isValid() {
		return (cid == DATAMESSAGEINFOID);
	}
};


class MessagePlayer {
public:
	MessagePlayer();
	~MessagePlayer();

	bool initRead(const char* root, uint32 interval, bool rotate = false);
	bool initRead(const char* root, bool rotate = false);
	bool initWrite(const char* root, uint32 maxCount, uint64 maxSize);

	bool setSubTypeList(std::map<uint16, std::string>& subtypes);
	bool setSubContextList(std::map<uint16, std::string>& subcontexts);
	bool setComponentNameList(std::map<uint32, std::string>& names);
	bool writeMetadata();
	bool readMetadata();
	bool setSystemIDs(DataMessage* msg);

	bool exportToCSVFile(const char* filename, const char* separator = NULL);
	std::string exportToCSV(const char* separator = NULL);

	bool addMessage(const char* name, DataMessage* msg);
	DataMessage* waitForNextMessage(uint32 ms, const char* &triggerName);
	DataMessage* waitForNextMessage(uint32 ms, uint32 &msToNext);
	DataMessage* waitForNextMessage(uint32 ms, const char* &triggerName, uint32 &msToNext);
	DataMessage* waitForNextMessage(uint32 ms);
	std::string getCurrentTriggerName();

	bool flushToDisk();
	std::string printAllString(const char* format = NULL);

private:
	bool writeBuffer();
	bool readBuffer();

	uint32 interval;
	uint32 maxCount;
	uint64 maxSize;
	uint32 indexSize;
	bool rotate;
	std::string root;
	char* indexData;
	char currentTriggerName[MAXKEYNAMELEN + 1];
	std::string indexFile;
	uint32 indexFileSize;

	std::string metadataFile;
	std::string metadataTextFile;
	std::string csvFile;

	std::map<uint16, std::string> subtypeList;
	std::map<uint16, std::string> subcontextList;
	std::map<uint32, std::string> componentNameList;

	uint32 curStart;
	uint32 curWrite;
	uint32 curIndex;
	uint32 curCount;
	uint64 curSize;
	uint64 lastWriteTime;
	uint64 lastReceiveTime;
	uint64 lastReadTime;
	uint64 serial;
	uint64 nextMsgTime;
};

} // namespace cmlabs

#endif //_MESSAGEPLAYER_H_

