#if !defined(_MEMORYREQUESTQUEUES_H_)
#define _MEMORYREQUESTQUEUES_H_

#include "PsyTime.h"
#include "DataMessage.h"

namespace cmlabs {

#define MEMORYREQUEST_ERROR			0
#define MEMORYREQUEST_IDLE			1
#define MEMORYREQUEST_INIT			2
#define MEMORYREQUEST_SUBMITTED		3
#define MEMORYREQUEST_RECEIVED		4
#define MEMORYREQUEST_STARTED		5
#define MEMORYREQUEST_COMPLETED		6
#define MEMORYREQUEST_REPLIED		7
#define MEMORYREQUEST_REPLYRECEIVED	8
#define MEMORYREQUEST_FINISHED		9

struct RequestQueueHeader {
	uint32 size;
	uint32 count;
	uint64 createdTime;
	uint64 lastUpdateTime;
	uint16 status;
	uint32 startPos;
	uint32 endPos;
	uint32 padding;
	// N x DataMessages from startPos to endPos
};



struct RequestStatusHeader {
	uint32 size;
	uint32 countTotal;
	uint32 countInUse;
	uint64 createdTime;
	uint64 lastUpdateTime;
	uint16 status;
	uint32 bitFieldSize;
	// Bitfield
	// countTotal x Entries
};

struct RequestStatusEntry {
	uint64 id;
	uint64 createdTime;
	uint64 lastUpdateTime;
	uint16 status;
	uint32 from; // id of connection making the request
	uint32 to; // id of connection processing the request
};

class MemoryRequestQueues {
public:
	static bool InitQueue(char* data, uint32 size);
	static bool CheckQueue(char* data, uint32 size);
	static bool AddToQueue(char* data, DataMessage* msg);
	static DataMessage* GetNextQueueMessage(char* data);

	static bool InitRequestMap(char* data, uint32 count);
	static bool CheckRequestMap(char* data, uint32 count);
	static uint64 AddNewRequest(char* data, uint16 status, uint32 from, uint32 to);
	static bool SetRequestStatus(char* data, uint64 id, uint16 status);
	static uint32 GetRequestStatus(char* data, uint64 id, uint64& createdTime, uint64& lastUpdateTime);
	static bool CloseRequest(char* data, uint64 id);
private:
	static RequestStatusEntry* GetRequestEntry(char* data, uint64 id);
};

} // namespace cmlabs

#endif //_MEMORYREQUESTQUEUES_H_

