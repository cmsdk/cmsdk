#if !defined(_HTML_H_)
#define _HTML_H_

#include "Utils.h"

namespace cmlabs {
namespace html {

std::string DecodeHTML(std::string str);
std::string EncodeHTML(std::string str);

unsigned char HTML2Char(const char* str);

bool GetMimeType(char* dest, const char* filename, uint32 maxsize);
bool GetMimeType(char* dest, const char* filename, uint32 maxsize, bool &isBinary);
bool IsMimeBinary(const char* type);

std::string GetUsernameFromURL(std::string url);
std::string GetPasswordFromURL(std::string url);
std::string GetAuthenticationFromURL(std::string url);
std::string GetHostFromURL(std::string url);
uint16 GetPortFromURL(std::string url);
std::string GetProtocolFromURL(std::string url);
std::string GetURIFromURL(std::string url);

} // namespace html
} // namespace cmlabs

#endif //_UTILS_H_

