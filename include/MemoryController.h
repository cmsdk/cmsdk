#if !defined(_MEMORYCONTROLLER_H_)
#define _MEMORYCONTROLLER_H_

#include "Types.h"

namespace cmlabs {

// Interface for all MemoryControllers
class MemoryController {
public:
	MemoryController();
	virtual ~MemoryController();

	virtual bool setID(uint16 id) = 0;
	virtual uint16 getID() = 0;

	virtual bool setDynamicShmemSize(uint64 memorySize) = 0;
	virtual uint64 getDynamicShmemSize() = 0;

	virtual uint32 getDynamicShmemSerial() = 0;
	virtual uint32 incrementDynamicShmemSerial() = 0;
};


} // namespace cmlabs

#endif //_MEMORYCONTROLLER_H_

