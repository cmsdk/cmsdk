#if !defined(_UNITTEST_PSYSDK_H_)
#define _UNITTEST_PSYSDK_H_

#include "MemoryManager.h"
#include "MessageIndex.h"
#include "RESTAPI.h"

namespace cmlabs {

bool UnitTest_CMSDK();
bool PerfTest_CMSDK();

} // namespace cmlabs

#endif // _UNITTEST_PSYSDK_H_

