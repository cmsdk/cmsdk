#if !defined(_CONTROLMESSAGE_H_)
#define _CONTROLMESSAGE_H_

#include "PsyTime.h"
#include "ObjectIDs.h"

namespace cmlabs {


struct ControlMessageHeader {
	uint32	size;	// auto
	uint32	cid;	// auto
	uint64	time;	// auto/api
	uint64	from;
	uint64	to;

	uint16	command;// user
	uint16	status;	// user
};

class ControlMessage {
public:
	ControlMessage(uint16 command, uint64 from, uint64 to, char* data = NULL, uint32 dataSize = 0);
	ControlMessage(char* data);
	~ControlMessage();

	uint32 getSize();
	uint16 getCommand();
	uint64 getTime();
	uint64 getFrom();
	uint64 getTo();

	uint16 getStatus();
	bool setStatus(uint16 status);

	char* data;
};

} // namespace cmlabs

#endif //_CONTROLMESSAGE_H_

