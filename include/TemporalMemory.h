#if !defined(_TEMPORALMEMORY_H_)
#define _TEMPORALMEMORY_H_

#include "MemoryManager.h"
#include "PsyTime.h"

namespace cmlabs {

	struct SlotEntryStruct {
		uint16 serial; // wraps around
		uint64 eol;
		uint32 bin;
		uint64 offset;
		uint64 size; // if 0 not in use
		uint64 usage; // blocks used * blocksize >= size
		uint32 startBlock;
		uint32 blockUsage;
		//uint32 blockSize;
	};

	struct TemporalMemoryStruct {
		uint64 size;
		uint32 cid;
		uint32 slotCount;
		uint32 slotsInUse;
		uint64 usage;
		uint64 growStep;
		uint64 maxSize;
		uint64 createdTime;		// time of creation
		uint64 lastCheckTime;
		uint64 checkTimeInterval;
		uint16 binCount;
		uint32 bitFieldSize; // number of bytes in the bitfield
		// Bitfield(bitFieldSize)
		// slotCount * SlotEntryStruct
		// bin0(size), bin1(size), bin2(size)...
	};

	struct BinHeaderStruct {
		uint64 size;
		uint32 blockCount;
		uint32 blockSize;
		uint32 blocksInUse;
		uint64 usage;
		uint32 bitFieldSize; // number of bytes in the bitfield
		// Bitfield(bitFieldSize)
		// blockCount * blockSize data
	};

	//struct BinSpaceFit {
	//	uint32 bin;
	//	uint32 blocksNeeded;
	//	uint32 spaceWaisted;
	//};
	//bool BinSpaceFitSort(const BinSpaceFit& a, const BinSpaceFit& b) {
	//	return (a.blocksNeeded < b.blocksNeeded);
	//}

#define GETMSGNODE(id) (uint16)((id & 0xffff000000000000L)>>48)
#define GETMSGSERIAL(id) (uint16)((id & 0x0000ffff00000000L)>>32)
#define GETMSGSLOT(id) (uint32)(id & 0x00000000ffffffffL)
#define SETMSGID(id,node,serial,slot) id = (((uint64)node) << 48) | (((uint64)serial) << 32) | ((uint64)slot)

#define CHECKTEMPORALMEMORYSERIAL 	if (serial != master->getDynamicShmemSerial()) {if (!open()) {mutex->leave();return 0;}}

	class MasterMemory;
	class TemporalMemory {
	public:
		TemporalMemory(MemoryController* master);
		~TemporalMemory();

		bool getMemoryUsage(uint64& alloc, uint64& usage);

		bool open();
		bool create(uint32 slotCount, uint16 binCount, uint32 minBlockSize, uint32 maxBlockSize, uint64 initSize, uint64 maxSize, uint32 growSteps = 8);
		bool setNodeID(uint16 id);

		// Insert new block of memory and return full id
		bool insertMessage(DataMessage* msg, uint64& id);
		// Get copy of block of memory
		DataMessage* getCopyOfMessage(uint64 id);

		bool logUsage();

		bool maintenance();

		static bool UnitTest();

	private:

		std::vector<BinHeaderStruct> calcBinHeaders(uint32 binCount, uint32 minBlockSize, uint32 maxBlockSize, uint64 binDataSize);
		BinHeaderStruct* getBestBin(uint32 size, uint32& bin, uint32 &blocksNeeded);
		char* getMemoryBlock(uint64 id);
		bool checkSlots(uint64 now);

		bool resizeSlots();
		bool resizeBin(uint32 bin, BinHeaderStruct* binHeader, uint32 msgSize);
		std::vector<BinHeaderStruct> calcBinHeadersGrowth(uint32 bin = 0, uint32 msgSize = 0);
		bool resizeMemory(uint32 slotGrowth, std::vector<BinHeaderStruct>& binHeaderSizes);

		utils::Mutex* mutex;
		MemoryController* master;
		TemporalMemoryStruct* header;
		SlotEntryStruct* slotEntries;
		uint64 memorySize;
		uint16 port;
		uint16 node;
		uint32 serial;
	};


//	
//	
//struct TemporalMemoryStruct {
//	uint64 size;
//	uint32 cid;
//	uint64 usage;
//	uint64 growStep;
//	uint64 maxSize;
//	uint64 createdTime;		// time of creation
//	uint16 slotCount;
//	uint64 slotSize;
//	uint32 slotDuration;
//	uint16 startSlot;
//	uint64 startTime;
//	// slots * SlotHeaderStruct
//};
//
//struct SlotHeaderStruct {
//	uint64 size;
//	uint32 count;
//	uint64 usage;
//	uint64 eol;
//	uint64 offset;
//};
//
//#define GETMSGNODE(id) (uint16)((id & 0xffff000000000000L)>>48)
//#define GETMSGSLOT(id) (uint16)((id & 0x0000ffff00000000L)>>32)
//#define GETMSGOFFSET(id) (uint64)(id & 0x00000000ffffffffL)
//#define SETMSGID(id,node,slot,offset) id = (((uint64)node) << 48) | (((uint64)slot) << 32) | ((uint64)offset)
//
//#define CHECKTEMPORALMEMORYSERIAL 	if (serial != master->getDynamicShmemSerial()) {if (!open()) {mutex->leave();return 0;}}
//
//class MasterMemory;
//class TemporalMemory {
//public:
//	TemporalMemory(MemoryController* master);
//	~TemporalMemory();
//
//	bool getMemoryUsage(uint64& alloc, uint64& usage);
//
//	bool open();
//	bool create(uint16 slotCount, uint32 slotDuration, uint64 initSize, uint64 maxSize, uint32 growSteps = 8);
//	bool setNodeID(uint16 id);
//
//	// Insert new block of memory and return full id
//	bool insertMessage(DataMessage* msg, uint64& id);
//	// Get copy of block of memory
//	DataMessage* getCopyOfMessage(uint64 id);
//
//	bool logUsage();
//
//	bool maintenance();
//
//	static bool UnitTest();
//
//private:
//	bool getSlotFromEOL(uint64 eol, uint16& slot);
//	char* getMemoryBlock(uint64 id);
//	bool growSlotSize(uint64 growAmount);
//	bool checkSlots();
//	bool resetSlot(uint16 slot, uint64 eol);
//	uint64 calcResizeAmount(uint32 msgSize);
//
//	utils::Mutex* mutex;
//	MemoryController* master;
//	TemporalMemoryStruct* header;
//	SlotHeaderStruct* slotHeaders;
//	uint64 memorySize;
//	uint16 port;
//	uint16 node;
//	uint32 serial;
//};

} // namespace cmlabs

#endif //_TEMPORALMEMORY_H_

