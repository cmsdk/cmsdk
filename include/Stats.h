#if !defined(_STATS_H_)
#define _STATS_H_

#include "Utils.h"

namespace cmlabs {

class Stats {
public:
	static bool UnitTest();

	Stats(uint32 maxCount = 10000);
	~Stats();
	
	bool clear();
	bool add(double val);

	double getAverage();
	double getSum();
	uint32 getCount();
	double getVariance();
	double getStdDev();
	double getMedian();

protected:
	uint32 maxCount;
	utils::Mutex mutex;
	std::list<double> entries;
};


} // namespace cmlabs

#endif //_STATS_H_

