#if !defined(_MEMORYQUEUES_H_)
#define _MEMORYQUEUES_H_

#include "MemoryManager.h"
#include "DataMessage.h"

namespace cmlabs {

static THREAD_RET THREAD_FUNCTION_CALL QueueTest(THREAD_ARG arg);

struct MessageQueueHeader {
	uint32 size;
	uint32 count;
	uint32 startPos;
	uint32 endPos;
	uint32 padding;
};

class MessageQueue {
public:
	MessageQueue(char* data, uint32 size);
	~MessageQueue();

	changeSize(uint32 size);
	changeData(char* data);
	
	static bool CreateMessageQueue(uint32& qid, const char* name = NULL);
	static bool AddMessageToQueue(uint32 qid, DataMessage* msg);
	static DataMessage* WaitForMessageQueue(uint32 qid, uint32 ms);
	static uint32 GetMessageQueueSize(uint32 qid);
	static bool GetMessageQueueByName(uint32& qid, const char* name);
	static bool DestroyMessageQueue(uint32 qid);

	static bool AddRequest(DataMessage* msg, uint32& reqID);
	static bool AddReply(uint32 reqID, bool success, DataMessage* msg);
	static bool WaitForReply(uint32 reqID, uint32 ms, uint8& status, DataMessage** outMsg);
	static uint32 GetRequestCount();

	static bool UnitTest();
	static bool UnitTestQueues();
	friend THREAD_RET THREAD_FUNCTION_CALL QueueTest(THREAD_ARG arg);

protected:
	MessageQueueHeader* header;
};

bool TestMemoryCommander();

} // namespace cmlabs

#endif //_MEMORYQUEUES_H_

