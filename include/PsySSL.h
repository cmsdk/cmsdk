//#include <stdio.h>
//#include <string.h>
//#include <iostream>

#ifndef __SSL_SERVER_H__
#define __SSL_SERVER_H__

#include "Utils.h"

namespace	cmlabs{

int TestSSL(int port);

#ifdef _USE_SSL_
	#include <openssl/ssl.h>
	#include <openssl/err.h>

class PsySSLServer {
public:
	PsySSLServer();
	~PsySSLServer();

	bool init(char *cFile, char *kFile);

private:
	SSL_CTX *ctx;
	SSL *ssl;

	void CreateCTX(void);
	void LoadCerts(char *cFile, char *kFile);
};


using namespace std;

typedef void *(*P_ARG)(void *);

class SSLServer {
  // Private data
  private:
    SSL_CTX *ctx;
    int PORT;
    fd_set fdset;
    // FD for server
    int master;
    // Thread function
    P_ARG pthr_f;

  // Private Functions
  private:
    // Load algorithms and create context.
    void CreateCTX(void);
    // Load certification files.
    void LoadCerts(const char *cFile, const char *kFile);
    // Create and attach socket.
    void BindPort(void);

  public:
    // Set function for pthread to call.
    void SetPthread_F(P_ARG fp) { pthr_f = fp; }

    // Constructors
    SSLServer(const char *cFile, const char *kFile, int port);

    /* Check for new connections, block for wait_t amount of seconds,
       if no connection is made within wait_t function returns.  This must
       be called regularly from within main */
    void CheckClients(int wait_t = 0);
};

#endif

}

#endif // _USE_SSL_
