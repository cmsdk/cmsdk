#if !defined(_MESSAGEINDEX_H_)
#define _MESSAGEINDEX_H_

#include "DataMessage.h"
#include "PsyTime.h"
#include "Subscriptions.h"

namespace	cmlabs {


class MessageIndex {
public:
	static bool UnitTest();

	MessageIndex();
	~MessageIndex();

	bool setDefaultTTL(uint64 defaultTTL);

	bool addIndexKey(const char* key, uint8 type);
	bool removeIndexKey(const char* key, uint8 type);

	bool setFileStorage(const char* dir, uint32 buffertime, const char* indexfilename = "index.db");

	bool addMessage(DataMessage* msg);

	char* queryMessages(RetrieveSpec* spec, uint32 &size, uint32 &count);

	uint32 getCount();

	bool doMaintenance();

private:

	bool addMessageToTime(DataMessage* msg);
	bool addMessageToString(DataMessage* msg);
	bool addMessageToInteger(DataMessage* msg);
	bool addMessageToFloat(DataMessage* msg);

	template <typename T>
	bool deleteIndexKey(const char* key, std::map<std::string, std::multimap<T, DataMessage*> > &maps);

	template <typename T>
	bool removeMessage(DataMessage* msg, std::multimap<T, DataMessage*> &map, T &val);

	template <typename T>
	char* queryMessages(uint32 from, uint32 to, PsyType &type, uint32 maxcount, uint64 maxage, uint32 &size, uint32 &count, std::multimap<T, DataMessage*> &map);
	template <typename T>
	char* queryMessages(T start, uint32 from, uint32 to, PsyType &type, uint32 maxcount, uint64 maxage, uint32 &size, uint32 &count, std::multimap<T, DataMessage*> &map);
	template <typename T>
	char* queryMessages(T start, T end, uint32 from, uint32 to, PsyType &type, uint32 maxcount, uint64 maxage, uint32 &size, uint32 &count, std::multimap<T, DataMessage*> &map);

	typedef std::pair <uint64, DataMessage*> Time_Pair;
	typedef std::pair <std::string, DataMessage*> String_Pair;
	typedef std::pair <int64, DataMessage*> Int_Pair;
	typedef std::pair <float64, DataMessage*> Float_Pair;
	typedef std::pair <PsyType, DataMessage*> Type_Pair;

	std::map<std::string, std::multimap<uint64, DataMessage*> > timeMaps;
	std::map<std::string, std::multimap<std::string, DataMessage*> > stringMaps;
	std::map<std::string, std::multimap<int64, DataMessage*> > integerMaps;
	std::map<std::string, std::multimap<float64, DataMessage*> > floatMaps;

	std::multimap<PsyType, DataMessage*>* typeMap;
	std::multimap<uint64, DataMessage*>* timeMap;
	std::multimap<uint64, DataMessage*> eolMap;
	uint64 lastMaintenance;

	std::string dir;
	std::string indexfilename;
	int32 buffertime;
	int32 maintenanceInterval;

	uint64 defaultTTL;
};


} // namespace cmlabs

#endif // _MESSAGEINDEX_H_
