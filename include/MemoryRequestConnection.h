#if !defined(_MEMORYREQUESTCONNECTION_H_)
#define _MEMORYREQUESTCONNECTION_H_

#include "MemoryRequestQueues.h"

namespace cmlabs {

#define MEMORYREQUESTSERVER_INIT	1


struct RequestServerHeader {
	uint32 size;
	uint32 cid;
	uint64 createdTime;
	uint64 lastUpdateTime;
	char name[256];
	uint16 status;
	uint32 maxConnectionCount;
	uint32 eachConnectionSize;
	uint32 maxQueueSize;
	uint32 maxRequestCount;
};

struct RequestConnectionHeader {
	uint32 size;
	uint32 conid;
	uint64 createdTime;
	uint64 lastUpdateTime;
	uint16 status;
};

class MemoryRequestConnection {
public:
	MemoryRequestConnection();
	~MemoryRequestConnection();

	bool connect(const char* memoryName);
	bool shutdown();

	uint64 makeRequest(DataMessage* msg);
	uint16 getRequestStatus(uint64 id, uint64& createdTime, uint64& lastUpdateTime);
	DataMessage* waitForRequestReply(uint32 ms);

	DataMessage* waitForRequestReply(uint64 id, uint32 ms);

	DataMessage* waitForRequest(uint32 ms);
	bool setRequestStatus(uint64 id, uint16 status);
	bool replyToRequest(uint64 id, DataMessage* msg);

private:
	RequestServerHeader* serverHeader;
	RequestConnectionHeader* conHeader;
	char* queueHeader;
	char* requestHeader;

	std::map<uint64, DataMessage*> replyCache;

	uint32 conID;
	bool oneWaitingForQueue;
	
	utils::Mutex* mutex;
	utils::Event cacheEvent;

	char* getQueueHeader(uint32 otherConID);
	DataMessage* waitForQueue(uint32 ms);
};

} // namespace cmlabs

#endif //_MEMORYREQUESTCONNECTION_H_

