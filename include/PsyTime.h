#if !defined(_PSYTIME_H_)
#define _PSYTIME_H_

//#include "MemoryManager.h"
#include "Types.h"
#include "sys/timeb.h"

namespace cmlabs {

/** @defgroup PsyTime Functions dealing with time
*  This is a list of the CMSDK time functions
*  @{
*/

#define USEC_YEAR_0_TO_1970 62167305600000000L
#define SECS_PER_YEAR 31536000
#define SECS_PER_MONTH 2629744
#define SECS_PER_WEEK 604800
#define SECS_PER_DAY 86400
#define SECS_PER_HOUR 3600
#define SECS_PER_MIN 60
#define TIME_YEAR_1970 62167305600000000L

#define SECOND_MS	1000
#define MINUTE_MS	SECS_PER_MIN * 1000
#define HOUR_MS		SECS_PER_HOUR * 1000
#define DAY_MS		SECS_PER_DAY * 1000
#define WEEK_MS		SECS_PER_WEEK * 1000
#define MONTH_MS	(uint64)SECS_PER_MONTH * 1000
#define YEAR_MS		(uint64)SECS_PER_YEAR * 1000

#define SECOND_US	(uint64)1000000
#define MINUTE_US	(uint64)SECS_PER_MIN * 1000000
#define HOUR_US		(uint64)SECS_PER_HOUR * 1000000
#define DAY_US		(uint64)SECS_PER_DAY * 1000000
#define WEEK_US		(uint64)SECS_PER_WEEK * 1000000
#define MONTH_US	(uint64)SECS_PER_MONTH * 1000000
#define YEAR_US		(uint64)SECS_PER_YEAR * 1000000

#define PSYSECOND	SECOND_US
#define PSYMINUTE	MINUTE_US
#define PSYHOUR		HOUR_US
#define PSYDAY		DAY_US
#define PSYWEEK		WEEK_US
#define PSYMONTH	MONTH_US
#define PSYYEAR		YEAR_US

static char PsyDays[][5] = {"None", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
static char PsyDaysFull[][10] = {"None", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
static char PsyMonths[][5] = {"None", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
static char PsyMonthsFull[][10] = {"None", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

struct PsyDateAndTime {
	uint32 usec;      /* microseconds after the minute - [0,999999] */
	uint32 msec;      /* milliseconds after the minute - [0,999] */
	uint16 sec;       /* seconds after the minute - [0,59] */
	uint16 min;       /* minutes after the hour - [0,59] */
	uint16 hour;      /* hours since midnight - [0,23] */
	uint16 day;      /* day of the month - [1,31] */
	uint16 mon;       /* months since January - [1,12] */
	uint16 year;      /* years since 0000 */
	uint16 wday;      /* day of the week - [1,7] */
	uint16 yday;      /* day of the year - [1,366] */
	bool negative;    /* is the time value negative */
	bool dst;         /* daylight savings in effect */
	int32 dstsec;     /* daylight savings in minutes when in effect */
	int32 tzsec;	  /* timezone in sec */
	char zonename[64];  /* the name of the current time zone */
};

// Return current time according to the TMC
uint64 GetTimeNow();

// Calculate the current Time Mapping Constant
uint64 SyncToHardwareClock();
bool SetCurrentNetSyncDif(int64 netTimeDif);
bool SetCurrentTimeSyncData(uint64 tmc, int64 netTimeAdjust);
bool GetCurrentTimeSyncData(uint64& tmc, int64& netTimeAdjust);
//bool SetLocalSyncAdjustment(int64 adjust);
//bool ChangeLocalSyncAdjustment(int64 adjust);

#define TMC_MASTER 0
#define TMC_SLAVE 1
#define TMC_SYNC_INTERVAL 60000000	// 1 minute

static uint64 CurrentTMC = 0;
static uint8 CurrentTMCMode = TMC_MASTER;
static uint64 LastTMCSync = 0;
static int64 NetTimeAdjust = 0;

// Estimate the next TMC wrap-around
uint64 EstNextTMCWrap();

// Return age of a timestamp compared to NOW
int64 GetTimeAge(uint64 t);
int32 GetTimeAgeMS(uint64 t);
uint64 GetTimeFromString(const char* str);
uint32 GetHTTPTime(uint64 time, char* buffer, uint32 size);
uint64 GetTimeFromPsyDateAndTime(struct PsyDateAndTime &tad);

uint32 GetTimeOffsetGMT();

struct PsyDateAndTime GetDateAndTime(uint64 t, bool local = true);
struct PsyDateAndTime GetDateAndTimeUTC(uint64 t);

struct PsyDateAndTime GetTimeDifference(uint64 t1, uint64 t2);
struct PsyDateAndTime GetTimeDifference(int64 dif);

char* PrintTime(uint64 t, bool local = true, bool us = true, bool ms = true);
char* PrintTimeOnly(uint64 t, bool local = true, bool us = true, bool ms = true);
char* PrintTimeDif(uint64 t, bool us = true, bool ms = true);
char* PrintDate(uint64 t, bool local = true);
char* PrintDateSortable(uint64 t, bool local = true);
char* PrintDateSortableDelimiter(uint64 t, const char* del, bool local = true);

char* PrintTimeSortable(uint64 t, bool local);
char* PrintTimeSortableMillisec(uint64 t, bool local);
char* PrintTimeSortableMicrosec(uint64 t, bool local);

std::string PrintTimeNowString(bool local = true, bool us = true, bool ms = true);
std::string PrintTimeString(uint64 t, bool local = true, bool us = true, bool ms = true);
std::string PrintTimeOnlyString(uint64 t, bool local = true, bool us = true, bool ms = true);
std::string PrintTimeDifString(uint64 t, bool us = true, bool ms = true);
std::string PrintDateString(uint64 t, bool local = true);
std::string PrintDateStringSortable(uint64 t, bool local = true);
std::string PrintDateStringSortableDelimiter(uint64 t, const char* del, bool local = true);

std::string PrintTimeSortableString(uint64 t, bool local = true);
std::string PrintTimeSortableMillisecString(uint64 t, bool local = true);
std::string PrintTimeSortableMicrosecString(uint64 t, bool local = true);

uint64 FTime2PsyTime(uint64 t);

bool PsyTime_UnitTest();

/** @} */ // end of time

} // namespace cmlabs

#endif //_PSYTIME_H_

