#include "Utils.h"

namespace cmlabs {

template <typename T>
std::list<GenericObservation<T>*> GenericObservation<T>::ReadAllFromMessage(DataMessage* msg) {
	uint32 n = 1;
	std::list<GenericObservation<T>*> list;
	GenericObservation<T>* obs;
	uint32 size;
	const char* data;
	while ((data = msg->getData(n, "OBS", size)) && size && (size == sizeof(ObsHeader) + sizeof(T))) {
		obs = new GenericObservation<T>();
		if (obs->readFromData(data, size))
			list.push_back(obs);
		else
			delete obs;
		n++;
	}
	return list;
}

template <typename T>
bool GenericObservation<T>::WriteAllToMessage(DataMessage* msg, std::list<GenericObservation<T>*>& list) {
	// #################
	return false;
}

template <typename T>
GenericObservation<T>::GenericObservation() {
	memset(&header, 0, sizeof(ObsHeader));
	header.cid = OBSERVATIONID;
	header.size = sizeof(ObsHeader);
	header.timestamp = GetTimeNow();
	header.type = data.getType();
	data.init();
}

template <typename T>
GenericObservation<T>::GenericObservation(const char* cdata, uint32 size, uint64 id) {
	if (!readFromData(cdata, size, id)) {
		memset(&header, 0, sizeof(ObsHeader));
		header.cid = OBSERVATIONID;
		header.size = sizeof(ObsHeader);
		header.timestamp = GetTimeNow();
		header.type = data.getType();
		data.init();
	}
}


template <typename T>
GenericObservation<T>::~GenericObservation() {}

//template <typename T>
//bool GenericObservation<T>::init() {}

template <typename T>
bool GenericObservation<T>::writeToMessage(DataMessage* msg, uint64 id) {
	if (!msg || (!header.id && !id)) return false;
	uint32 s;
	if (!id)
		id = header.id;
	if (msg->getData(id, "OBS", s))
		return false;
	// calculate size needed
	header.size = sizeof(ObsHeader) + sizeof(T);
	// create temp memory buffer
	char* buffer = new char[header.size];
	memcpy(buffer, &header, sizeof(ObsHeader));
	memcpy(buffer + sizeof(ObsHeader), &data, sizeof(T));
	bool res = msg->setData(id, "OBS", buffer, header.size);
	delete[] buffer;
	return res;
}

template <typename T>
bool GenericObservation<T>::readFromData(const char* cdata, uint32 size, uint64 id) {
	if (!cdata || (size != (sizeof(ObsHeader) + sizeof(T))))
		return false;
	if (((ObsHeader*)cdata)->type != header.type)
		return false;
	memcpy(&header, cdata, sizeof(ObsHeader));
	memcpy(&data, cdata + sizeof(ObsHeader), sizeof(T));
	return true;
}

template <typename T>
bool GenericObservation<T>::readFromMessage(DataMessage* msg, uint64 id) {
	if (!msg || !id) return false;
	uint32 s;
	const char* cdata = msg->getData(id, "OBS", s);
	return readFromData(cdata, s, id);
}

//template <typename T>
//bool GenericObservation<T>::buildFromQuery(std::string& query) {
//	return data.buildFromQuery(query.c_str());
//}

template <typename T>
bool GenericObservation<T>::isBetween(GenericObservation<T>* a, GenericObservation<T>* b) {
	return data.isBetween(a->data, b->data);
}

template <typename T>
bool GenericObservation<T>::extractDataFromMessage(DataMessage* msg, std::map<std::string, std::string>& mappings) {
	if (!data.extractDataFromMessage(msg, mappings))
		return false;
	// Extract header data
	//header.creatorID = (uint64)msg->getInt("SystemID");
	header.creatorID = msg->getSystemID();
	header.objectID = (uint64)msg->getInt("ObjectID");
	header.confidence = msg->getFloat("Confidence");
	header.consensus = msg->getFloat("Consensus");
	header.timestamp = msg->getCreatedTime();
	return true;
}

template <typename T>
std::string GenericObservation<T>::print() {
	return data.print();
}

template <typename T>
std::string GenericObservation<T>::toJSON() {
	std::string json = data.toJSON();
	if (json.length())
		return utils::StringFormat("{ \"datatype\": \"%s\", \"name\": \"%s\", \"id\": %llu, \"timestamp\": %llu, \"timetext\": \"%s\", \"objectid\": %llu, \"systemid\": %llu, \"topicid\": %llu, \"confidence\": %.3f, \"consensus\": %.3f, %s}\n",
			data.getTypeName().c_str(), header.name, header.id, header.timestamp, PrintTimeString(header.timestamp).c_str(), header.objectID, header.creatorID, header.topicID, header.confidence, header.consensus, json.c_str());
	else
		return utils::StringFormat("{ \"datatype\": \"%s\", \"name\": \"%s\", \"id\": %llu, \"timestamp\": %llu, \"timetext\": \"%s\", \"objectid\": %llu, \"systemid\": %llu, \"topicid\": %llu, \"confidence\": %.3f, \"consensus\": %.3f, \"datatype\": \"unknown\"}\n",
			data.getTypeName().c_str(), header.name, header.id, header.timestamp, PrintTimeString(header.timestamp).c_str(), header.objectID, header.creatorID, header.topicID, header.confidence, header.consensus);
}

} // namespace cmlabs
