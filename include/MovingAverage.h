#if !defined(_MOVINGAVERAGE_H_)
#define _MOVINGAVERAGE_H_

#include "Stats.h"
#include "PsyTime.h"

namespace cmlabs {

struct StatEntry {
	uint64 count;
	double value;
};

class MovingAverage {
public:
	static bool UnitTest();

	MovingAverage(uint32 binTimeMS = 100, uint16 binCount = 600);
	~MovingAverage();
	
	bool add(double val, uint32 count = 1, uint64 time = GetTimeNow());

	bool getThroughput(uint32 ms, double& valPerSec, double& countPerSec, uint64 now = GetTimeNow());
	bool getThroughputMulti(
		uint32 ms1, uint32 ms2, uint32 ms3,
		double& valPerSec1, double& countPerSec1,
		double& valPerSec2, double& countPerSec2,
		double& valPerSec3, double& countPerSec3, uint64 now = GetTimeNow() );

	bool getAverage(uint32 ms, double& val, uint64& count, uint64 now = GetTimeNow());
	bool getAverageMulti(
		uint32 ms1, uint32 ms2, uint32 ms3,
		double& avg1, uint64& count1,
		double& avg2, uint64& count2,
		double& avg3, uint64& count3, uint64 now = GetTimeNow());

	bool getSum(uint32 ms, double& val, uint64& count, uint64 now = GetTimeNow());
	bool getSumMulti(
		uint32 ms1, uint32 ms2, uint32 ms3,
		double& sum1, uint64& count1,
		double& sum2, uint64& count2,
		double& sum3, uint64& count3, uint64 now = GetTimeNow());
	
	bool getTotal(double& val, uint64& count);

	std::string getPerfXML(uint32 binMS = 1000, uint32 binNum = 60);
	std::string getPerfJSON(uint32 binMS = 1000, uint32 binNum = 60);

protected:
	StatEntry* entries;
	utils::Mutex mutex;

	uint32 binTime;
	uint32 binCount;
	uint32 currentBin;
	uint64 currentBinStart;
	double totalValue;
	uint64 totalCount;
	// uint64 totalTime;

	bool shiftBins(uint64 now);
};


} // namespace cmlabs

#endif //_MOVINGAVERAGE_H_

