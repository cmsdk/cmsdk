#if !defined(_OBJECTIDS_H_)
#define _OBJECTIDS_H_

#include "Types.h"

namespace cmlabs {

#define GenObjID(n)			(((n) << 16) | n)
#define VerObjID(id)		( (id != 0) && ((((id) & 0xFF00) >> 16) == ((id) & 0x00FF)) )
#define GetObjID(data)		*(((uint32*)(data))+1)
#define VerObj(data)		VerObjID(GetObjID(data))

// Built-in object types

#define UNUSEDID			GenObjID(	1	)
#define CONSTCHARID			GenObjID(	2	)
#define TIMEID				GenObjID(	3	)
#define CHARDATAID			GenObjID(	4	)
#define INTID				GenObjID(	5	)
#define DOUBLEID			GenObjID(	6	)
#define CONSTCHARINFOID		GenObjID(	7	)
#define CHARDATAINFOID		GenObjID(	8	)

#define PSYCLONEINDEXID		GenObjID(	10	)
#define PSYCLONEMASTERID	GenObjID(	11	)
#define TEMPORALMEMORYID	GenObjID(	12	)
#define PROCESSMEMORYID		GenObjID(	13	)
#define COMPONENTMEMORYID	GenObjID(	14	)
#define DATAMAPSMEMORYID	GenObjID(	15	)

#define DATAMESSAGEOLDID	GenObjID(	20	)
#define CONTROLMESSAGEID	GenObjID(	21	)
#define LOGENTRYID			GenObjID(	22	)
#define TIMESYNCDATA		GenObjID(	23	)
#define TRIGGERDATA			GenObjID(	24	)
#define RETRIEVEDATA		GenObjID(	25	)
#define POSTDATA			GenObjID(	26	)
#define COMPONENTSETUPDATA	GenObjID(	27	)
#define IDRESERVATION		GenObjID(	28	)
#define TYPELEVELID			GenObjID(	29	)
#define CONTEXTLEVELID		GenObjID(	30	)
#define CRANKID				GenObjID(	31	)
#define COMPONENTID			GenObjID(	32	)
#define TAGID				GenObjID(	33	)
#define NODEINFOPACKID		GenObjID(	34	)
#define NODEMAPID			GenObjID(	35	)
#define DATAMESSAGEINFOID	GenObjID(	36	)
#define DATAMESSAGEDRAFTID	GenObjID(	37	)

#define MEMORYREQUESTSERVERID	GenObjID(	38	)
#define DYNAMICMEMORYID		GenObjID(	39	)
#define OBSERVATIONID		GenObjID(	40	)
#define DATAMESSAGEID		GenObjID(	41	)

// Built-in message types
static struct PsyType CTRL_TEST =			{ { 1,10001,0,0,0,0,0,0,0,0,0,0,0,0,0,0 } };
static struct PsyType CTRL_LOGPRINT =		{ { 1,10002,0,0,0,0,0,0,0,0,0,0,0,0,0,0 } };
static struct PsyType CTRL_PING =			{ { 1,10003,0,0,0,0,0,0,0,0,0,0,0,0,0,0 } };
static struct PsyType CTRL_PING_REPLY =	{ { 1,10004,0,0,0,0,0,0,0,0,0,0,0,0,0,0 } };

} // namespace cmlabs

#endif //_OBJECTIDS_H_

