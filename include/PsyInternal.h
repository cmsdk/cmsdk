#if !defined(_PSYINTERNAL_H_)
#define _PSYINTERNAL_H_

#include "Utils.h"
#include "PsyAPI.h"
#include "Bitmap.h"

namespace cmlabs {

struct TrackPlayerMessageEntry {
	enum EntryType { TEXT, INT, FLOAT } entryType;
	std::string key;
	std::string value;
};

struct TrackPlayerMessage {
	enum PostType { MESSAGE, SIGNAL } postType;
	enum TriggerType { TRIGGER, TIME, NONE } triggerType;
	std::string postName;
	uint32 delayMS;
	uint64 nextTime;
	uint32 timeMS;
	uint32 intervalMS;
	std::map<std::string, TrackPlayerMessageEntry> entryMap;
	bool addEntry(const char* key, const char* value, const char* keytype = NULL);
};


int8 Internal_Simple(PsyAPI* api);
int8 Internal_Ping(PsyAPI* api);
int8 Internal_Pong(PsyAPI* api);
int8 Internal_Shutdown(PsyAPI* api);
int8 Internal_Print(PsyAPI* api);
int8 Internal_Time(PsyAPI* api);
int8 Internal_SignalPing(PsyAPI* api);
int8 Internal_SignalPong(PsyAPI* api);
int8 Internal_RetrieveTest(PsyAPI* api);
int8 Internal_MessageScript(PsyAPI* api);
int8 Internal_QueryTest(PsyAPI* api);
int8 Internal_StatsLog(PsyAPI* api);
int8 Internal_BitmapPoster(PsyAPI* api);
int8 Internal_MessageToggler(PsyAPI* api);
int8 Internal_MessageTypeConverter(PsyAPI* api);

} // namespace cmlabs

#endif //_PSYINTERNAL_H_

