#if !defined(_NETWORKPROTOCOLS_H_)
#define _NETWORKPROTOCOLS_H_

#pragma once

#include "DataMessage.h"
#include "NetworkConnections.h"
#include "jsmn.h"
#include "hash/sha1.h"

#define PROTOCOL_HTTP_SERVER	1
#define PROTOCOL_HTTP_CLIENT	2
#define PROTOCOL_MESSAGE		4
#define PROTOCOL_TELNET			8
#define PROTOCOL_3GCAM			16
#define ENCRYPTION_SSL			1
#define ENCRYPTION_AES			2

namespace	cmlabs{

/////////////////////////////////////////////////////////////
// Data Types
/////////////////////////////////////////////////////////////

#define HTTP_GET		1
#define HTTP_PUT		2
#define HTTP_POST		3
#define HTTP_HEAD		4
#define HTTP_DELETE		5
#define HTTP_OPTIONS	6

static char HTTP_Type[][8] =
{
"",
"GET",
"PUT",
"POST",
"HEAD",
"DELETE",
"OPTIONS"
};

// 
//GET /path/file.html HTTP/1.0
//From: someuser@jmarshall.com
//User-Agent: HTTPTool/1.0
//if-modified-since: Sat, 29 Oct 1994 19:43:31 GMT
//[blank line here]
//
//POST /path/script.cgi HTTP/1.0
//From: frog@jmarshall.com
//User-Agent: HTTPTool/1.0
//Content-Type: application/x-www-form-urlencoded
//Content-Length: 32
//
//home=Cosby&favorite+flavor=flies
//

class HTTPPostEntry {
public:
	HTTPPostEntry() { content = NULL; contentSize = 0; }
	virtual ~HTTPPostEntry() { delete [] content; }

	bool isValid() {
		return ( name.length() && content && contentSize );
	}
	bool setContent(const char* content, uint32 size) {
		if (!content) return false;
		this->contentSize = size;
		this->content = new char[size+1];
		memcpy(this->content, content, size);
		this->content[size] = 0;
		return true;
	}

	std::string name;
	std::string filename;
	std::string type;
	uint32 contentSize;
	char* content;
};

class HTTPRequest {
public:

	static HTTPRequest* CreateWebsocketRequest(const char* uri, const char* host, const char* protocolName, const char* origin);

//	HTTPRequest(uint64 startRecTime = 0);
	HTTPRequest(uint64 source = 0, uint64 startRecTime = 0);
	HTTPRequest(HTTPRequest* req);
	virtual ~HTTPRequest();

	// Reading it in from a socket
	bool processHeader(const char* buffer, uint32 size, bool &isInvalid);
	bool processContent(const char* buffer, uint32 size);
	const char* getHeaderEntry(const char* entry);
	const char* getRequest();
	const char* getURI();
	const char* getProtocol();
	std::string decodeBasicAuthorization();
	const char* getBasicAuthorization();
	std::string getBasicAuthorizationUser();
	std::string getBasicAuthorizationPassword();
	bool setBasicAuthorization(const char* authB64);
	bool setBasicAuthorization(const char* user, const char* password);
	const char* getParameter(const char* entry);
	const char* getPostData(const char* entry, uint32& size, const char** type);
	const char* getPostData(const char* entry, uint32& size);
	const char* getPostDataType(const char* entry);
	bool parseURIParameters(const char* text);
	bool parseContentParameters(const char* content, uint32 size);
	bool parseContentChunk(const char* chunk, uint32 size);

	DataMessage* convertToMessage();

	bool isWebsocketUpgrade();

	const char* getContent(uint32& size);
	const char* getRawContent(uint32& size);

	bool createRequest(uint8 type, const char* host, const char* uri, const char* content, uint32 contentSize, bool keepAlive, uint64 ifModifiedSince);
	bool createRequest(uint8 type, const char* host, const char* uri, std::map<std::string, std::string>& headerEntries, bool keepAlive, uint64 ifModifiedSince);
	bool createRequest(uint8 type, const char* host, const char* uri, std::map<std::string, std::string>& headerEntries, const char* content, const char* contentType, uint32 contentSize, bool keepAlive, uint64 ifModifiedSince);
	bool createRequest(uint8 type, const char* host, const char* uri, std::map<std::string, std::string>& headerEntries, HTTPPostEntry* bodyEntry, bool keepAlive, uint64 ifModifiedSince);
	bool createMultipartRequest(uint8 type, const char* host, const char* uri, std::map<std::string, std::string>& headerEntries, std::map<std::string, HTTPPostEntry*>& bodyEntries, bool keepAlive, uint64 ifModifiedSince);
	bool createWebsocketRequest(const char* uri, const char* host, const char* protocolName, const char* origin);

	uint32 getSize();
	
	uint8 type;
	uint64 time;
	uint64 endReceiveTime;
	uint64 source;
	uint32 headerLength;
	uint32 contentLength;
	uint64 ifModifiedSince;
	bool keepAlive;
	char* data;

	std::map<std::string, std::string> entries;
	std::map<std::string, std::string> params;
	std::map<std::string, HTTPPostEntry*> postEntries;
	std::string postBoundary;
};

struct JSONMEntry {
	uint32 chunk;
	uint64 size;
	uint64 offset;
	std::string name;
	std::string type;
	std::string getInfoString() {
		return utils::StringFormat("[%u] %s {%s} (%s / %llu)\n",
			chunk, name.c_str(), type.c_str(), utils::BytifySize((uint32)size).c_str(), offset);
	}
	std::string toJSON() {
		return utils::StringFormat("{\"name\": \"%s\", \"bytesize\": %llu, \"mimetype\": \"%s\"}",
			name.c_str(), size, type.c_str());
	}
};

class JSONM {
public:
	JSONM();
	JSONM(const char* data, uint64 size);
	JSONM(const char* json);
	~JSONM();

	uint64 getSize();
	uint32 getCount();

	bool isValid();
	std::string getJSON();
	const char* getData(const char* name, uint64& size);
	const char* getData(uint32 chunk, uint64& size);
	std::string getDataType(const char* name);
	std::string getDataType(uint32 chunk);
	std::string getDataName(uint32 chunk);

	bool setJSON(const char* json);
	uint32 addData(const char* name, const char* data, uint64 size, const char* type);

	std::string getInfoString();
	DataMessage* convertToMessage();

	char* jmData;
	uint64 jmSize;
	uint64 mOffset;
	std::map<uint32, JSONMEntry> entries;

private:
	bool setRawJSON(const char* json);
	bool extractMultipartInfo();
	bool updateMultipartJSON();

	utils::Mutex mutex;
};


class WebsocketData {
public:
	enum DataType { NONE = 0, TEXT = 1, BINARY = 2, CLOSE = 8, PING = 9, PONG = 10 } dataType;
	enum WSSTATUS { IDLE = 0, INCOMPLETE = 1, COMPLETE = 2 } status;

	// masks for fields in the basic header
	static uint8 const BHB0_OPCODE = 0x0F;
	static uint8 const BHB0_RSV3 = 0x10;
	static uint8 const BHB0_RSV2 = 0x20;
	static uint8 const BHB0_RSV1 = 0x40;
	static uint8 const BHB0_FIN = 0x80;
	static uint8 const BHB1_PAYLOAD = 0x7F;
	static uint8 const BHB1_MASK = 0x80;
	static uint8 const payload_size_code_16bit = 0x7E; // 126
	static uint8 const payload_size_code_64bit = 0x7F; // 127

	static uint8 const basic_header_length = 2;
	/// Maximum length of a WebSocket header
	static uint8 const max_header_length = 14;
	/// Maximum length of the variable portion of the WebSocket header
	static uint8 const max_extended_header_length = 12;
	/// Maximum size of a basic WebSocket payload
	static uint8 const payload_size_basic = 125;
	/// Maximum size of an extended WebSocket payload (basic payload = 126)
	static uint16 const payload_size_extended = 0xFFFF; // 2^16, 65535
												  /// Maximum size of a jumbo WebSocket payload (basic payload = 127)
	static uint64 const payload_size_jumbo = 0x7FFFFFFFFFFFFFFFLL;//2^63

	//	HTTPRequest(uint64 startRecTime = 0);
	WebsocketData(uint64 source = 0, uint64 startRecTime = 0);
	WebsocketData(WebsocketData* req);
	virtual ~WebsocketData();

	static WebsocketData* CreateTerminationConfirmation();
	static WebsocketData* CreatePong();
	static WebsocketData* CreatePing();

	bool isTerminationRequest();
	bool isTerminationConfirmation();
	bool isComplete();
	bool isPing();
	bool isPong();

	bool setData(DataType dataType, bool maskData, const char* data = NULL, uint64 size = 0);
	const char* getRawData(uint64& size);

	// Reading it in from a socket
	bool processHeader(const char* buffer, uint64 size, bool &isInvalid);
	bool processContent(const char* buffer, uint64 size);
	//const char* getHeaderEntry(const char* entry);
	//const char* getRequest();
	//const char* getURI();
	//const char* getProtocol();
	//std::string decodeBasicAuthorization();
	//const char* getBasicAuthorization();
	//std::string getBasicAuthorizationUser();
	//std::string getBasicAuthorizationPassword();
	//bool setBasicAuthorization(const char* authB64);
	//bool setBasicAuthorization(const char* user, const char* password);
	//const char* getParameter(const char* entry);
	//const char* getPostData(const char* entry, uint32& size, const char** type);
	//const char* getPostData(const char* entry, uint32& size);
	//const char* getPostDataType(const char* entry);
	//bool parseURIParameters(const char* text);
	//bool parseContentParameters(const char* content, uint32 size);
	//bool parseContentChunk(const char* chunk, uint32 size);

	//DataMessage* convertToMessage();

	const char* getContent(uint64& size);

	//bool createRequest(uint8 type, const char* host, const char* uri, const char* content, uint32 contentSize, bool keepAlive, uint64 ifModifiedSince);
	uint64 getContentSize();
	uint64 getPayloadSize();

	bool isFinal;
	uint64 time;
	uint64 endReceiveTime;
	uint64 source;
	//uint64 ifModifiedSince;
	//bool keepAlive;
	uint32 headerLength;
	uint64 contentSize;
	uint64 payloadSize;
	uint8 opcode;
	uint32 maskingKey;
	uint32 packages;

	char* data;
	char* rawData;

	//std::map<std::string, std::string> entries;
	//std::map<std::string, std::string> params;
	//std::map<std::string, HTTPPostEntry*> postEntries;
	//std::string postBoundary;
};


#define TELNET_WINDOWS	1
#define TELNET_UNIX		2

class TelnetLine {
public:
	TelnetLine(uint64 source);
	virtual ~TelnetLine();

	bool setLine(const char* buffer, bool addCR);
	bool setLine(const char* buffer, uint32 len, bool addCR = false);
	bool giveData(char* buffer, uint32 len);
	uint32 getSize();
	bool setCR(uint8 type);

	char* data;
	char cr[3];
	uint64 time;
	uint64 source;
	uint32 size;
	uint32 user;
};

// HTTP/1.0 200 OK
// HTTP/1.0 500 Internal Server Error  <H1>Error while creating internal page</H1>
// HTTP/1.0 200 OK
// Cache-Control: No-Cache
// HTTP/1.0 404 Object Not Found   <H1>File not found</H1>
// HTTP/1.0 403 Access Forbidden   <H1>Access Denied</H1>

#define HTTP_OK						0
#define HTTP_MOVED_PERMANENTLY		1
#define HTTP_USE_LOCAL_COPY			2
#define HTTP_MALFORMED_URL			3
#define HTTP_UNAUTHORIZED			4
#define HTTP_ACCESS_DENIED			5
#define HTTP_FILE_NOT_FOUND			6
#define HTTP_INTERNAL_ERROR			7
#define HTTP_NOT_IMPLEMENTED		8
#define HTTP_SERVICE_NOT_AVAILABLE	9
#define HTTP_GATEWAY_TIMEOUT		10
#define HTTP_SERVER_UNAVAILABLE		11
#define HTTP_SERVER_NOREPLY			12
#define HTTP_SERVER_MALFORMED_REPLY	13
#define HTTP_SWITCH_PROTOCOL		14

static char HTTP_Status[][128] =
{
"HTTP/1.1 200 OK",
"HTTP/1.1 301 Moved Permanently",
"HTTP/1.1 304 Use Local Copy",
"HTTP/1.1 400 Bad Request <H1>Malformed URL</H1>",
"HTTP/1.1 401 Unauthorized",
"HTTP/1.1 403 Access Denied",
"HTTP/1.1 404 Object Not Found",
"HTTP/1.1 500 Internal Server Error  <H1>Error while creating page</H1>",
"HTTP/1.1 501 Not Implemented  <H1>Not implemented</H1>",
"HTTP/1.1 503 Service Unavailable  <H1>Service Unavailable</H1>",
"HTTP/1.1 504 Gateway Timeout  <H1>Gateway Timeout</H1>",
"HTTP/1.1 500 Internal Server Error  <H1>Server unavailable</H1>",
"HTTP/1.1 500 Internal Server Error  <H1>No reply from server</H1>",
"HTTP/1.1 500 Internal Server Error  <H1>Malformed server reply</H1>",
"HTTP/1.1 101 Switching Protocols"
};

class HTTPReply {
public:
	static HTTPReply* CreateErrorReply(uint8 type);
	static HTTPReply* CreateAuthorizationReply(const char* realm);
	static HTTPReply* CreateWebsocketHTTPReply(const char* key, const char* version);

	HTTPReply(uint64 source = 0);
	HTTPReply(HTTPReply* reply);
	virtual ~HTTPReply();
	
	bool isWebsocketUpgrade();

	// Reading it in from a socket
	bool processHeader(const char* buffer, uint32 size, bool &isInvalid);
	bool processContent(const char* buffer, uint32 size);
	const char* getHeaderEntry(const char* entry);
	const char* getProtocol();

	const char* getContent(uint32& size);
	const char* getRawContent(uint32& size);

	// Generating the data
	bool createPage(uint8 status, uint64 time, const char* serverName,
		uint64 lastMod, bool keepAlive, bool cache, const char* contentType,
		const char* content, uint32 contentSize = 0, const char* additionalHeaderEntries = NULL);

	bool createFromFile(uint64 time, const char* serverName,
		uint64 ifLastMod, bool keepAlive, bool cache,
		const char* filename);

	bool createOptionsResponse(uint8 status, uint64 time, const char* serverName, bool keepAlive, const char* origin, const char* operations);
	bool createErrorPage(uint8 status, uint64 time, const char* serverName, bool keepAlive);
	bool createAuthorizationReply(const char* realm);
	bool createWebsocketHTTPReply(const char* key, const char* version);

	uint32 getSize();

	uint8  type;
	uint64 time;
	uint64 source;
	uint32 headerLength;
	uint32 contentLength;
	bool keepAlive;
	char* data;

	std::map<std::string, std::string> entries;
	std::map<std::string, std::string> params;
};


/////////////////////////////////////////////////////////////
// Auto Detect Protocols
/////////////////////////////////////////////////////////////

class NetworkProtocol {
public:
	uint8 protocol;
	NetworkProtocol() {protocol = 0;}
	static bool CheckBufferForCompatibility(const char* buffer, uint32 length) {return false;};
	static bool InitialiseConversation(NetworkConnection* con) {return false;};
};

class HTTPProtocol : public NetworkProtocol
{
public:
	HTTPProtocol() {protocol = PROTOCOL_HTTP_SERVER;}
	static bool CheckBufferForCompatibility(const char* buffer, uint32 length);
	static bool InitialiseConversation(NetworkConnection* con);
	static bool SendHTTPReply(NetworkConnection* con, HTTPReply* reply);
	static HTTPReply* ReceiveHTTPReply(NetworkConnection* con, uint32 timeout);
	static bool SendHTTPRequest(NetworkConnection* con, HTTPRequest* req);
	static HTTPRequest* ReceiveHTTPRequest(NetworkConnection* con, uint32 timeout);
	static WebsocketData* ReceiveWebsocketData(NetworkConnection* con, uint32 timeout);
	static bool SendWebsocketData(NetworkConnection* con, WebsocketData* wsData);
};

class HTTPClientProtocol : public NetworkProtocol
{
public:
	HTTPClientProtocol() {protocol = PROTOCOL_HTTP_CLIENT;}
	static bool CheckBufferForCompatibility(const char* buffer, uint32 length);
	static bool InitialiseConversation(NetworkConnection* con);
	static bool SendHTTPReply(NetworkConnection* con, HTTPReply* reply);
	static HTTPReply* ReceiveHTTPReply(NetworkConnection* con, uint32 timeout);
	static bool SendHTTPRequest(NetworkConnection* con, HTTPRequest* req);
	static HTTPRequest* ReceiveHTTPRequest(NetworkConnection* con, uint32 timeout);
};

class MessageProtocol : public NetworkProtocol
{
public:
	MessageProtocol() {protocol = PROTOCOL_MESSAGE;}
	static bool CheckBufferForCompatibility(const char* buffer, uint32 length);
	static bool InitialiseConversation(NetworkConnection* con);
	static bool SendMessage(NetworkConnection* con, DataMessage* msg, uint64 receiver = 0);
	static DataMessage* ReceiveMessage(NetworkConnection* con, uint32 timeout);
};

class TelnetProtocol : public NetworkProtocol
{
public:
	TelnetProtocol() {protocol = PROTOCOL_TELNET;}
	static bool CheckBufferForCompatibility(const char* buffer, uint32 length);
	static bool InitialiseConversation(NetworkConnection* con);
	static bool SendTelnetLine(NetworkConnection* con, TelnetLine* line);
	static TelnetLine* ReceiveTelnetLine(NetworkConnection* con, uint32 timeout);
	static TelnetLine* ReceiveTelnetLine(NetworkConnection* con, uint32 timeout, uint32 size);
};

}

#endif // _NETWORKPROTOCOLS_H_
