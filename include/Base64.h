#include <string>

std::string base64_encode(std::string string_to_encode);
std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(const char* encoded_string);
std::string base64_decode(std::string const& s);
