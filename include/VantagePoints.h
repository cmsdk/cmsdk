#if !defined(_VANTAGEPOINTS_H_)
#define _VANTAGEPOINTS_H_

#include "MathClasses.h"
#include "Utils.h"
#include "xml_parser.h"

namespace cmlabs {

enum PerchState { FREE = 0, RESERVED = 1, OCCUPIED = 2, LEAVING = 3 };
enum VantageState { IDLE = 0, INMOTION = 1, SETTLED = 2 };

struct PerchPoint {
	uint32 id;
	PerchState state;
	uint32 entityID;
	uint64 lastVisit;
	std::string label;
	std::string data;
	std::vector<double> overlaps;
};

class VantagePoints {
public:
	VantagePoints();
	~VantagePoints();

	bool init(const char* xml);
	bool init(XMLNode node);
	uint32 reserveNextPoint(uint32 entityID);
	bool occupyPoint(uint32 id, uint32 entityID);
	bool freePoint(uint32 id, uint32 entityID);

	const char* getPointData(uint32 id);
	const char* getPointLabel(uint32 id);
	uint32 count;
	VantageState state;

private:
	std::map<uint32, PerchPoint> points;
	uint32* inUseMap;
	uint64 intrinsicTimescaleMS;
	int32 timeoutMS;
	uint64 motionStartedTime;
};

} // namespace cmlabs

#endif //_VANTAGEPOINTS_H_

