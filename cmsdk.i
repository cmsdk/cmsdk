%module cmsdk

%include "std_string.i"
%include "std_map.i"
%include "std_vector.i"
/* %include "std_list.i" */

#if defined(SWIGWORDSIZE64)
typedef				bool		bit;
typedef	signed		char		int8;
typedef	unsigned	char		uint8;
typedef				short		int16;
typedef	unsigned	short		uint16;
typedef				int			int32;
typedef	unsigned	int			uint32;
typedef				long long	int64;
typedef	unsigned	long long	uint64;
typedef				float		float32;
typedef				double		float64;
#else
typedef				bool		bit;
typedef	signed		char		int8;
typedef	unsigned	char		uint8;
typedef				short		int16;
typedef	unsigned	short		uint16;
typedef				int			int32;
typedef	unsigned	int			uint32;
typedef				long long	int64;
typedef	unsigned	long long	uint64;
typedef				float		float32;
typedef				double		float64;
#endif

#define THREAD_FUNCTION_CALL

%{
#include "DataMessage.h"
#include "Subscriptions.h"
#include "PsySpace.h"
#include "PsyTime.h"
#include "PsyAPI.h"
#include "Bitmap.h"
#include "MessagePlayer.h"
using namespace cmlabs; 
%}

%apply SWIGTYPE *DISOWN {(cmlabs::DataMessage *msg)};

%include "include/DataMessage.h"
%include "include/Subscriptions.h"
%include "include/PsyAPI.h"
%include "include/PsySpace.h"
%include "include/PsyTime.h"
%include "include/Bitmap.h"
%include "include/MessagePlayer.h"

namespace std {
	/* %template(MessageList) list<DataMessage>; */
	%template(StringVector) vector<string>;
	%template(IntVector) vector<int64>;
	%template(FloatVector) vector<float64>;
}
