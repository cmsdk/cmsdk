######## Identify OS ########

UNAME := $(shell uname)
ifeq ($(UNAME),Darwin)
	OSNAME = MACOSX
else
  ifeq (CYGWIN, $(findstring CYGWIN, $(UNAME)))
	OSNAME = CYGWIN
  else
	OSNAME = LINUX
  endif
endif



######## System Commands ########

AR=ar
ARFLAGS=rc
CC = g++
LN=ln
LD=ld
MV=mv
RM=rm
MAKE=make
SH=sh

ifeq ($(OSNAME),MACOSX)
	RANLIB=ranlib -s
	CPALL = cp -pR
else
  ifeq ($(OSNAME),CYGWIN)
	RANLIB=ranlib
	CPALL = cp -a
  else
	RANLIB=ranlib
	CPALL = cp -a
  endif
endif

######## Compile and Link Args ########

NOTLINK = -c

ifeq ($(OSNAME),MACOSX)
	SYSTEM=Darwin
	STANDARD=-O2 -D_GNU_SOURCE -D_REENTRANT -D_THREAD_SAFE -D $(SYSTEM) -D PSYPRO
	OTHERLIBS=
	LIBPATHS=
else
  ifeq ($(OSNAME),CYGWIN)
	SYSTEM=WIN32 -D CYGWIN -D_WIN32_WINNT=0x0500
	STANDARD= -O2 -D_GNU_SOURCE -D_REENTRANT -D_THREAD_SAFE -D $(SYSTEM) -D PSYPRO
	OTHERLIBS=-lws2_32 -lrpcrt4 -lrpcns4 -lpsapi -liphlpapi -lGdi32
	LIBPATHS=-Wl,--rpath -Wl,/usr/local/lib
	LIBOPT=-Wl,--out-implib,$(IMPORT) -shared
  else
	SYSTEM=Linux
	STANDARD=-O2 -D_GNU_SOURCE -D_REENTRANT -D_THREAD_SAFE -D $(SYSTEM) -D PSYPRO -std=c++11
#	OTHERLIBS=-pthread -ldl -lrt -lcurses
	OTHERLIBS=-pthread -ldl -lrt
	LIBPATHS=-Wl,--rpath -Wl,/usr/local/lib
	LIBOPT=-shared -rdynamic -Wl,-soname,$(SONAME)
	PYTHON2INCLUDE=-I/usr/include/python2.7
	PYTHON3INCLUDE=-I/usr/include/python3.4m
	PYTHON2LIB=-lpython2.7
	PYTHON3LIB=-lpython3.4m
  endif
endif

######## Check for 64-bit arch, use of -fPIC ########

MACHINE := $(shell uname -m)
ifeq ($(MACHINE),x86_64)
  ifeq ($(OSNAME),CYGWIN)
	BITARCH=32
  else
	STANDARD += -fPIC
	BITARCH=64
  endif
else
  ifeq ($(MACHINE),i686)
	ifeq (64, $(findstring 64, $(UNAME)))
		BITARCH=64
	WINBITARCH=64
	else
	BITARCH=32
	WINBITARCH=32
	endif
  else
	BITARCH=32
  endif
endif

######## Setup system names ########

ifeq ($(OSNAME),MACOSX)
  ARCH = macosx$(BITARCH)
  BINDESTDIR = $(BINDIR)/$(ARCH)
  LIBDESTDIR = $(LIBDIR)/$(ARCH)
else
  ifeq ($(OSNAME),CYGWIN)
	ARCH = cygwin32
	BINDESTDIR = $(BINDIR)/$(ARCH)
	LIBDESTDIR = $(LIBDIR)/$(ARCH)
	WINARCH = win32
	WINBINDESTDIR = $(BINDIR)/$(WINARCH)
	WINLIBDESTDIR = $(LIBDIR)/$(WINARCH)
  else
	ARCH = linux$(BITARCH)
	BINDESTDIR = $(BINDIR)/$(ARCH)
	LIBDESTDIR = $(LIBDIR)/$(ARCH)
  endif
endif

######## Check for debug and profile commands ########

export EXT=
export EXTLOW=

ifeq (debug, $(findstring debug, $(MAKECMDGOALS)))
	export DEBUG=-g -rdynamic -D _DEBUG
	export EXT=Debug
	export EXTLOW=debug
endif

ifeq (ssl, $(findstring ssl, $(MAKECMDGOALS)))
	export DEBUG=-D _USE_SSL_
	export OTHERLIBS=-pthread -ldl -lrt -lssl -lcrypto
endif

ifeq (ssldebug, $(findstring ssldebug, $(MAKECMDGOALS)))
	export DEBUG=-g -rdynamic -D _DEBUG -D _USE_SSL_
	export OTHERLIBS=-pthread -ldl -lrt -lssl -lcrypto
	export EXT=Debug
	export EXTLOW=debug
endif

ifeq (profile, $(findstring profile, $(MAKECMDGOALS)))
	export DEBUG=-pg -rdynamic -D _DEBUG
endif

######## Setup various variables ########

COMMAND = $(MAKECMDGOALS)
CURRMONTHSHORT := $(shell date +%b)
CURRYEAR := $(shell date +%Y)
DATE := $(shell date +%y%m%d)
BUILDDATE := $(shell date +%c)
BUILDSYSTEM := $(shell uname -pmns)
TOP_DIR := $(shell pwd)

VC90PATHX86 := C:/Program*Files*x86*/Microsoft*Visual*Studio*9.0/VC/vcpackages
VC90PATH := C:/Program*Files/Microsoft*Visual*Studio*9.0/VC/vcpackages
VCBUILDDIR := $(shell if `test -d $(VC90PATH)` ; then echo  '$(VC90PATH)' ; else if `test -d $(VC90PATHX86)`; then echo '$(X86VC90PATH)' ; else echo VC_90_NOT_FOUND; fi ; fi )
