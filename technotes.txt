The use of Global Shared Memory requires the user executing the application to be allowed
the priviledge of SeCreateGlobalPrivilege. If the software is executed as a service or by
an administrator this is set by default, but for regular users and some other cases this
priviledge needs to be added manually.

This can be done by adding the user in question to
Administrative Tools / Local Security Policy - Local Policies / User Rights Assignment / Create global objects
This is described in more detail on: http://www.tenouk.com/ModuleI2.html

or

1) Type secpol.msc in the Start Menu and press Enter.
2) Double click on Local Policies then double click on Security Options.
3) Scroll to the bottom to this entry -
User Account Control: Run all administrators in Admin approval mode.
Double click that line.
4) Set it to disabled then press OK.
5) Reboot.

Disable SELinux/firewall on Linux:
Turn off SELinux
mcedit /etc/selinux/config
reboot

Turn off firewall
service iptables save
service iptables stop
chkconfig iptables off

On Fedora 18+:
systemctl status firewalld.service
systemctl stop firewalld.service
systemctl disable firewalld.service
